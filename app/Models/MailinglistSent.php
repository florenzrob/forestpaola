<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailinglistSent extends Model
{

    //protected $fillable = [];
    //protected $table = 'mailinglist_sents';

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

     //Many-to-One
     public function mailinglist()
     {
         return $this->belongsTo('App\Models\Mailinglist');
     }


}
