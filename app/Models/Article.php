<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    use Sluggable;

    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    //One-to-Many
    public function children()
    {
        return $this->hasMany('App\Models\Article', 'parent_id', 'id');
    }

    //Many-to-One
    public function parent()
    {
        return $this->hasOne('App\Models\Article', 'id', 'parent_id');
    }

    //Many-to-One
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    //Many-to-Many
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag')->withPivot('article_id', 'tag_id');
    }

    public function positions()
    {
        return $this->belongsToMany('App\Models\Position')->withPivot('article_id', 'position_id');
    }

}
