<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mediacollection extends Model
{
    public $timestamps = false;
    
    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

    //One-to-Many
    public function mediaitems()
    {
        return $this->hasMany('App\Models\Mediaitem');
    }

    //Many-to-One
    public function mediatype()
    {
        return $this->belongsTo('App\Models\Mediatype');
    }

    public function mediaposition()
    {
        return $this->belongsTo('App\Models\Mediaposition');
    }

    public function mediarelatedmodel()
    {
        return $this->belongsTo('App\Models\Mediarelatedmodel');
    }

}
