<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use Sluggable;

    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    //One-to-Many
    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    //Many-to-One
    public function parent()
    {
        return $this->hasOne('App\Models\Category', 'id', 'parent_id');
    }

    //One-to-Many
    public function articles()
    {
        return $this->hasMany('App\Models\Article');
    }

    //Many-to-Many
    public function pages()
    {
        return $this->belongsToMany('App\Models\Page')->withPivot('page_id', 'category_id');
    }

}
