<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mediarelatedmodel extends Model
{

    public $timestamps = false;
    
    protected $fillable = ['modelname', 'description'];

    //One-to-Many
    public function mediacollections()
    {
        return $this->hasMany('App\Models\Mediacollection');
    }
}
