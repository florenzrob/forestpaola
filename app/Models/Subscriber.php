<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{

    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];



    //Many-to-Many
    public function groups()
    {
        return $this->belongsToMany('App\Models\Mlgroup')->withPivot('mlgroup_id', 'subscriber_id');
    }


}
