<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mediaitem extends Model
{
    public $timestamps = false;

    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];


    //Many-to-One
    public function mediacollection()
    {
        return $this->belongsTo('App\Models\Mediacollection');
    }


}
