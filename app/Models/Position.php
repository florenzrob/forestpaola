<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{


    protected $fillable = ['name', 'position', 'description'];


    //Many-to-Many
    public function articles()
    {
        return $this->belongsToMany('App\Models\Article')->withPivot('article_id', 'position_id');
    }
}
