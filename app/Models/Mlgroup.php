<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mlgroup extends Model
{

    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

    //Many-to-Many
    public function subscribers()
    {
        return $this->belongsToMany('App\Models\Subscriber')->withPivot('mlgroup_id', 'subscriber_id');
    }

    //Many-to-Many
    public function mailings()
    {
        return $this->belongsToMany('App\Models\Mailinglist')->withPivot('mailinglist_id', 'mlgroup_id');
    }

}
