<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mailinglist extends Model
{

    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

     //One-to-Many
     public function sent()
     {
         return $this->hasMany('App\Models\MailinglistSent');
     }

    //Many-to-Many
    public function groups()
    {
        return $this->belongsToMany('App\Models\Mlgroup')->withPivot('mailinglist_id', 'mlgroup_id');
    }

}
