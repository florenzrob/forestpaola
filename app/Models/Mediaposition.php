<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mediaposition extends Model
{

    public $timestamps = false;
    
    protected $fillable = ['name', 'description'];

    //One-to-Many
    public function mediacollections()
    {
        return $this->hasMany('App\Models\Mediacollection');
    }
}
