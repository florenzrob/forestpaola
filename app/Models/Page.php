<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    use Sluggable;

    //protected $fillable = ['title', 'slug', 'subtitle', 'seo_title', 'seo_content', 'abstract', 'content', 'icon', 'image_cover', 'is_private', 'sort', 'visible'];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    //One-to-Many
    public function children()
    {
        return $this->hasMany('App\Models\Page', 'parent_id', 'id');
    }

    //Many-to-One
    public function parent()
    {
        return $this->hasOne('App\Models\Page', 'id', 'parent_id');
    }

    //Many-to-One
    public function template()
    {
        return $this->belongsTo('App\Models\Template');
    }

    //Many-to-Many
    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu')->withPivot('visible');
    }

     //Many-to-Many
     public function categories()
     {
         return $this->belongsToMany('App\Models\Category')->withPivot('page_id', 'category_id');
     }

}
