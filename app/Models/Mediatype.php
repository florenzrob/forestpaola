<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mediatype extends Model
{

    protected $fillable = ['type'];

    //One-to-Many
    public function mediacollections()
    {
        return $this->hasMany('App\Models\Mediacollection');
    }
}
