<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    //protected $fillable = [];

    //Campi che non possono essere aggiornati
    //Se $fillable non è definito, e guarded è vuoto, tutti i campi possono essere aggiornati
    protected $guarded = [];

    //One-to-Many
    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id', 'id');
    }

    //Many-to-One
    public function parent()
    {
        return $this->hasOne('App\Models\Menu', 'id', 'parent_id');
    }

    //Many-to-Many
    public function pages()
    {
        return $this->belongsToMany('App\Models\Page')->withPivot('visible');
    }

}
