<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        //Attivare cron su server: * * * * * cd /home/lape/lape2020 && php artisan schedule:run >> /dev/null 2>&1
        //Cron su cpanel
        ///usr/local/bin/php /home/rmwaqtfe/forestpaola/artisan schedule:run >> /dev/null 2>&1

        //Test cron
        //$schedule->call('App\Http\Controllers\MailinglistController@testCron')->everyTenMinutes();


        //Limite invio su keliweb: 180 mail/h
        //Invio mailinglist
        $schedule->call('App\Http\Controllers\MailinglistController@queueMailinglist')->everyThirtyMinutes();    
        //$schedule->call('App\Http\Controllers\MailinglistController@testCron')->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
