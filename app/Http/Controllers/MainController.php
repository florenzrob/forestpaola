<?php

namespace App\Http\Controllers;

use App\Models\Mediacollection;
use App\Models\Mediarelatedmodel;
use App\Models\Page;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $cl = null;
        $page = Page::where('is_home', true)->first();

        //Controllo se ci sono gallery associate
        if (!empty($page)):
            $media = Mediarelatedmodel::where('modelname', 'Page')->first();
            if (!empty($media)):
                $cl = Mediacollection::with('mediatype')->with('mediaitems')->where('mediarelatedmodel_id', $media->id)->where('record_id', $page->id)->get();
            endif;
        endif;

        return view('layouts.home', [
            'page' => $page,
            'cl' => $cl,
        ]);
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function wip()
    {
        //$this->middleware('auth');
        return view('layouts.wip');
    }

    public function phpVersion()
    {
        return view('admin.test.phpinfo');
    }

    public function privacy()
    {

        return view('layouts.privacy', [
        ]);
    }

    //Unsubscribe mailinglist
    public function unsubscribe()
    {

        return view('layouts.unsubscribe', [

        ]);
    }
}
