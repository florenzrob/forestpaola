<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\Subscriber;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function send(Request $request)
    {
        if ($request->ajax()) {

            $fields = null;
            $fromAddress = env('MAIL_FROM_ADDRESS');
            $fromName = env('MAIL_FROM_NAME');

            $data = $request['formValues'];
            foreach ($request['formValues'] as $field):
                $fields[$field['name']] = $field['value'];
            endforeach;

            //var_dump($fields);
            //exit;
            //Iscrivo a ml se box cecheked
            if(isset($fields['newsletter'])):
                $subscriber = [];

                $user = Subscriber::where('email',  $fields['email'])->first();
                if(empty($user)):
                    $subscriber['name'] = $fields['name'];
                    $subscriber['surname'] = $fields['surname'];
                    $subscriber['email'] = $fields['email'];
                    $subscriber['active'] = 1;

                    $subscriber = Subscriber::create($subscriber);
                endif;

            endif;


            $this->sendMail($fields, $fromAddress, $fromName);

            //Save in db
            //Contact::create($fields);

            return response()->json(['responseText' => trans('messages.mail_success')], 200);
        }
    }

    public function sendMail($fields, $fromAddress, $fromName)
    {

        $settings = Setting::first();

        $fields['settings'] = $settings;

        //Save in db
        //Contact::create($fields);

        //Send email to customer
        $subject = $settings->company_business_name . ' - ' . trans('messages.form_subject_customer');

        Mail::send('emails.risposta_customer',
            array(
                'fields' => $fields,
            ), function ($message) use ($fromAddress, $fromName, $subject, $fields) {

                $fullname = $fields['name'] . ' ' . $fields['surname'];

                $message->from($fromAddress, $fromName);
                $message->to($fields['email'], $fullname)->subject($subject);
            });

        //Send email to Admin
        $subject = $settings->company_business_name . ' - ' . trans('messages.form_subject_admin') . ' ' . date('d/m/Y') . ' alle ore ' . date('H:i');

        Mail::send('emails.risposta',
            array(
                'fields' => $fields,
            ), function ($message) use ($fromAddress, $fromName, $subject, $fields) {

                $fullname = $fields['name'] . ' ' . $fields['surname'];

                $message->from($fields['email'], $fullname);
                $message->to($fromAddress, $fromName)->subject($subject);
                $message->cc(env('MAIL_CC'), $fromName)->subject($subject);

            });

        return true;

    }

}
