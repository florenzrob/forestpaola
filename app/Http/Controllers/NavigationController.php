<?php

namespace App\Http\Controllers;

use App;
use App\Helpers\MyHelper;
use App\Http\Controllers\Controller;
use App\Models\Menu;

class NavigationController extends Controller
{

    public function getNavigation($menuId)
    {

        //1: top menu - 2: footer

        $menu = Menu::findOrFail($menuId);

        $pages = $menu->pages()
        //->orderby('is_home', 'DESC')
        ->orderby('sort', 'ASC')
        ->where('parent_id', null)
        ->where('pages.visible', true)
        ->where('pages.is_private', false)
        ->where('pages.is_home', false) //La home è una pagina speciale, la mostro solo nella home
        ->wherePivot('visible', true)->get();


        $items = null;

        foreach ($pages as $page) {
            // echo 'id: ' . $page->id . ' - parent:' . $page->parent_id. ' - title:' . $page->title . '<br>';
            $items[$page->id] = [
                'item' => $page,
                'route' => null,
                'children' => MyHelper::getChildren($items, $page->slug, $page->id),
            ];
        };

        //dd($items);

        //Converto array a oggetto
        return json_decode(json_encode($items));

    }

    public function getAdminNavigation()
    {

        $navigation = [
            //Settings
            'settings' => [
                'visible' => true,
                'title' => trans('admin.navigations.settings'),
                'icon' => 'fas fa-cogs',
                'items' => [
                    [
                        //Settings
                        'visible' => true,
                        'route' => route('settings.edit'),
                        'routename' => 'templates.edit',
                        'icon' => 'fas fa-sliders-h',
                        'label' => trans('admin.settings.title'),
                    ],
                ],
            ],

            //Layout
            'sections' => [
                'visible' => true,
                'title' => trans('admin.navigations.layouts'),
                'icon' => 'fas fa-store-alt',
                'items' => [
                    [
                        //Templates
                        'visible' => true,
                        'route' => route('templates.list'),
                        'routename' => 'templates.list',
                        'icon' => 'fas fa-sitemap',
                        'label' => trans('admin.templates.title'),
                    ],
                    [
                        //Positions
                        'visible' => true,
                        'route' => route('positions.list'),
                        'routename' => 'positions.list',
                        'icon' => 'fas fa-columns',
                        'label' => trans('admin.positions.title'),
                    ],
                    [
                        //Menus
                        'visible' => true,
                        'route' => route('menus.list'),
                        'routename' => 'menus.list',
                        'icon' => 'fas fa-bars',
                        'label' => trans('admin.menus.title'),
                    ],
                ],
            ],

            //Media
            [
                'visible' => true,
                'title' => trans('admin.navigations.medias'),
                'icon' => 'fas fa-images',
                'items' => [
                    [
                        //Gestione immagini
                        'visible' => true,
                        'route' => route('images.list'),
                        'routename' => 'images.list',
                        'icon' => 'fas fa-images',
                        'label' => trans('admin.medias.manage_images'),
                    ],
                    [
                        //Gestione gallerie
                        'visible' => true,
                        'route' => route('galleries.list'),
                        'routename' => 'galleries.list',
                        'icon' => 'fas fa-images',
                        'label' => trans('admin.medias.manage_galleries'),
                    ],

                ],
            ],

            //Contenuti
            [
                'visible' => true,
                'title' => trans('admin.navigations.contents'),
                'icon' => 'fas fa-file',
                'items' => [
                    [
                        //Pages
                        'visible' => true,
                        'route' => route('pages.list'),
                        'routename' => 'pages.list',
                        'icon' => 'fas fa-file',
                        'label' => trans('admin.pages.title'),
                    ],
                    [
                        //Categories
                        'visible' => true,
                        'route' => route('categories.list'),
                        'routename' => 'categories.list',
                        'icon' => 'fas fa-file',
                        'label' => trans('admin.categories.title'),
                    ],
                    [
                        //Articles
                        'visible' => true,
                        'route' => route('articles.list'),
                        'routename' => 'articles.list',
                        'icon' => 'fas fa-file',
                        'label' => trans('admin.articles.title'),
                    ],
                    [
                        //Tags
                        'visible' => true,
                        'route' => route('tags.list'),
                        'routename' => 'tags.list',
                        'icon' => 'fas fa-file',
                        'label' => trans('admin.tags.title'),
                    ],
                ],
            ],

            //Mailing list
            [
                'visible' => true,
                'title' => trans('admin.navigations.mailinglist'),
                'icon' => 'fas fa-mail-bulk',
                'items' => [
                    [
                        //Iscritti
                        'visible' => true,
                        'route' => route('subscribers.list'),
                        'routename' => 'subscribers.list',
                        'icon' => 'fas fa-users',
                        'label' => trans('admin.subscribers.title'),
                    ],
                    [
                        //Gruppi Mailing list
                        'visible' => true,
                        'route' => route('mlgroups.list'),
                        'routename' => 'mlgroups.list',
                        'icon' => 'fas fa-layer-group',
                        'label' => trans('admin.mlgroups.title'),
                    ],
                    [
                        //Mailing list
                        'visible' => true,
                        'route' => route('mailinglists.list'),
                        'routename' => 'mailinglists.list',
                        'icon' => 'fas fa-file',
                        'label' => trans('admin.mailinglists.title'),
                    ],

                    [
                        //Mailing list sent
                        'visible' => true,
                        'route' => route('mailinglists.sent.list'),
                        'routename' => 'mailinglists.sent.list',
                        'icon' => 'fas fa-chart-line',
                        'label' => trans('admin.mailinglists.sent_report'),
                    ],

                    [
                        //Anteprima email
                        'visible' => true,
                        'route' => route('mailinglist.test.showmail'),
                        'routename' => 'ailinglist.test.showmail',
                        'icon' => 'fas fa-search',
                        'label' => 'Anteprima email',
                    ],

                    [
                        //Test cron
                        'visible' => true,
                        //'route' => route('test.cron.sendingmailinglist'),
                        'route' => route('test.cron'),
                        'routename' => 'test.cron',
                        'icon' => 'fas fa-radiation-alt',
                        'label' => 'TEST CRON INVIO MAIL',
                    ],

                ],
            ],

            //Users
            [
                'visible' => true,
                'title' => trans('admin.navigations.profile'),
                'icon' => 'fas fa-users',
                'items' => [
                    [
                        //Profile
                        'visible' => true,
                        'route' => route('users.list'),
                        'routename' => 'users.list',
                        'icon' => 'fas fa-users',
                        'label' => trans('admin.users.title'),
                    ],
                ],
            ],

        ];

        //Converto a oggetto per comodità
        $json = json_encode($navigation);
        $nav = json_decode($json);

        return $nav;

    }

}
