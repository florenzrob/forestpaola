<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{

    private $routeList = 'positions.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::orderBy('position', 'ASC')->get();

        return view('admin.positions.index', [
            'positions' => $positions,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $position = new Position;

        return view('admin.positions.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'position.store', //Salvataggio form
            'position' => $position,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        /*
        $active = isset($fields['active']) ? 1 : 0;
        $fields['active'] = $active;
         */
        $response = Position::create($fields);

        $message = trans('admin.positions.created', ['name' => $fields['position']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Position::findOrFail($id);

        return view('admin.positions.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'position.update', //Salvataggio form
            'position' => $position,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        //dd($fields);

        /*
        $active = isset($fields['active']) ? 1 : 0;
        $fields['active'] = $active;
         */
        $position = Position::findOrFail($id);
        $position->update($fields);

        $message = trans('admin.positions.updated', ['name' => $fields['position']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $position = Position::destroy($id);

            $positions = Position::orderBy('position', 'ASC')->get();

            $message = trans('admin.positions.deleted');

            $view = view('admin.positions.list')->with([
                'positions' => $positions,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    //Mostro i contenuti per la posizione scelta
    public function getPositionContent($positionName)
    {

        $position = Position::where('position', $positionName)->first();

        $articles = Article::where('is_private', false)
            ->where('visible', true)
            ->whereHas('positions', function ($q) use ($position) {
                $q->where('position_id', $position->id);
            })
            ->orderBy('sort', 'asc')
            ->get();

        return $articles;

    }

}
