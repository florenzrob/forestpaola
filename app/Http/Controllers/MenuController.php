<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    private $routeList = 'menus.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::with('parent')
            ->with('pages')
            ->orderBy('parent_id', 'ASC')
            ->orderBy('sort', 'ASC')->get();

        return view('admin.menus.index', [
            'menus' => $menus,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = new Menu;
        $menus = Menu::get();
        $pages = Page::get();

        return view('admin.menus.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'menu.store', //Salvataggio form
            'menu' => $menu,
            'menus' => $menus,
            'pages' => $pages,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        //Add/Update positions
        $pages = isset($fields['pages']) ? $fields['pages'] : null;

        //Tolgo positions dall'array dell'update
        if (isset($fields['pages'])):
            unset($fields['pages']);
        endif;

        //dd($pages);

        $visible = isset($fields['visible']) ? 1 : 0;

        $fields['visible'] = $visible;

        $menu = Menu::create($fields);

        //Last insert id
        $id = $menu->id;

        //Aggiorno associazioni
        if (!empty($pages)):
            foreach ($pages as $page):
                $menu->pages()->attach($id, [
                    'page_id' => $page,
                ]);
            endforeach;
        endif;

        $message = trans('admin.menus.created', ['name' => $fields['name']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::with('pages')->findOrFail($id);
        $menus = Menu::where('id', '<>', $id)->orderBy('name', 'ASC')->get();

        //dd($menus);

        $pages = Page::orderBy('is_home', 'DESC')->orderBy('title', 'ASC')->get();

        return view('admin.menus.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'menu.update', //Salvataggio form
            'menu' => $menu,
            'menus' => $menus,
            'pages' => $pages,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        //Add/Update pages
        $pages = isset($fields['pages']) ? $fields['pages'] : null;

        //Tolgo pages dall'array dell'update
        if (isset($fields['pages'])):
            unset($fields['pages']);
        endif;

        //dd($fields);

        $visible = isset($fields['visible']) ? 1 : 0;

        $fields['visible'] = $visible;

        $menu = Menu::findOrFail($id);
        $menu->update($fields);

        //Rimuovo tutte associazioni menu-pages
        $menu->pages()->where('id', $id)->wherePivot('menu_id', $id)->detach();

        //Aggiorno associazioni
        if (!empty($pages)):
            foreach ($pages as $page):
                $menu->pages()->attach($id, [
                    'page_id' => $page,
                ]);
            endforeach;
        endif;

        $message = trans('admin.menus.updated', ['name' => $fields['name']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Imposto il parent di tutte le pagine figlie a null
            $menus = Menu::with('parent')
                ->with('pages')
                ->orderBy('parent_id', 'ASC')
                ->orderBy('sort', 'ASC')
                ->where('parent_id', $id)->get();

            if (!empty($menus)):
                foreach ($menus as $menu):
                    $m = Menu::findOrFail($menu->id);
                    $m->parent_id = null;
                    $m->update();
                endforeach;
            endif;

            //Rimuovo tutte associazioni menu-page
            $menu = Menu::findOrFail($id);
            $menu->pages()->where('id', $id)->wherePivot('menu_id', $id)->detach();

            //Elimino menu
            $menu = Menu::destroy($id);

            $menus = Menu::with('parent')
                ->with('pages')
                ->orderBy('parent_id', 'ASC')
                ->orderBy('sort', 'ASC')
                ->get();

            $message = trans('admin.menus.deleted');

            $view = view('admin.menus.list')->with([
                'menus' => $menus,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update visible status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateVisible(Request $request, $id)
    {

        if ($request->ajax()) {

            $visible = $request->is_visible;
            $fields['visible'] = $visible;

            $menu = Menu::findOrFail($id);
            $menu->update($fields);

            $menus = Menu::with('pages')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.menus.list')->with([
                'menus' => $menus,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

}
