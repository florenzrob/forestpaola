<?php

namespace App\Http\Controllers;

use App\Helpers\MyHelper;
use App\Models\Category;
use App\Models\Mediacollection;
use App\Models\Mediarelatedmodel;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{

    private $routeList = 'pages.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

        return view('admin.pages.index', [
            'pages' => $pages,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = new Page;
        $pages = Page::get();
        $categories = Category::orderBy('title', 'ASC')->get();

        return view('admin.pages.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'page.store', //Salvataggio form
            'page' => $page,
            'pages' => $pages,
            'categories' => $categories,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        //Se presente cover, prendo solo nome del file
        if (!empty($fields['image_cover'])):
            //dd(basename($fields['company_logo']));
            $fields['image_cover'] = basename($fields['image_cover']);
        endif;

        //Add/Update categories
        $categories = isset($fields['categories']) ? $fields['categories'] : null;

        //Tolgo categories dall'array dell'update
        if (isset($fields['categories'])):
            unset($fields['categories']);
        endif;

        $ishome = isset($fields['is_home']) ? 1 : 0;
        $onhome = isset($fields['on_home']) ? 1 : 0;
        $visible = isset($fields['visible']) ? 1 : 0;
        $isprivate = isset($fields['is_private']) ? 1 : 0;
        $showsidebardx = isset($fields['show_sidebar_dx']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['title'];
        $slug = $fields['title'];

        //Cerco il max value del sort
        $maxValue = Page::max('sort');
        $maxValue = $maxValue < 1 ? 1 : $maxValue+1;

        $sort = isset($fields['sort']) ? $fields['sort'] : $maxValue;


        $fields['is_home'] = $ishome;
        $fields['on_home'] = $onhome;
        $fields['visible'] = $visible;
        $fields['is_private'] = $isprivate;
        $fields['show_sidebar_dx'] = $showsidebardx;
        $fields['slug'] = str_slug($slug, '-');
        $fields['sort'] = $sort;

        /*
        //Image cover
        if ($request->hasFile('image_cover')) {

        if ($request->file('image_cover')->isValid()) {
        $allegato = $request->file('image_cover');

        $upload = MyHelper::fileUpload($allegato);
        $fields['image_cover'] = $upload['filename'];
        }
        }
         */

        if ($ishome):
            $this->resetHome();
        endif;

        $page = Page::create($fields);

        //Last insert id
        $id = $page->id;

        //Aggiorno associazioni
        if (!empty($categories)):
            foreach ($categories as $category):
                $page->categories()->attach($id, [
                    'category_id' => $category,
                ]);
            endforeach;
        endif;

        $message = trans('admin.pages.created', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        $pages = Page::where('id', '<>', $id)->orderBy('title', 'ASC')->get();
        $categories = Category::orderBy('title', 'ASC')->get();

        return view('admin.pages.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'page.update', //Salvataggio form
            'page' => $page,
            'pages' => $pages,
            'categories' => $categories,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        //Se presente cover, prendo solo nome del file
        if (!empty($fields['image_cover'])):
            //dd(basename($fields['company_logo']));
            $fields['image_cover'] = basename($fields['image_cover']);
        endif;

        //Add/Update categories
        $categories = isset($fields['categories']) ? $fields['categories'] : null;

        //Tolgo categories dall'array dell'update
        if (isset($fields['categories'])):
            unset($fields['categories']);
        endif;

        $ishome = isset($fields['is_home']) ? 1 : 0;
        $onhome = isset($fields['on_home']) ? 1 : 0;
        $visible = isset($fields['visible']) ? 1 : 0;
        $isprivate = isset($fields['is_private']) ? 1 : 0;
        $showsidebardx = isset($fields['show_sidebar_dx']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['title'];
        $slug = $fields['title'];

        $fields['is_home'] = $ishome;
        $fields['on_home'] = $onhome;
        $fields['visible'] = $visible;
        $fields['is_private'] = $isprivate;
        $fields['show_sidebar_dx'] = $showsidebardx;
        $fields['slug'] = str_slug($slug, '-');

        //$pressSlugName = $request->slug;
        $urls = null;
        $path = null;

        /*
        //Image cover
        if ($request->hasFile('image_cover')) {

        if ($request->file('image_cover')->isValid()) {
        $allegato = $request->file('image_cover');

        $upload = MyHelper::fileUpload($allegato);
        $fields['image_cover'] = $upload['filename'];
        }
        }
         */

        if ($ishome):
            $this->resetHome();
        endif;

        $page = Page::findOrFail($id);
        $page->update($fields);

        //Rimuovo tutte associazioni category-page
        $page->categories()->where('id', $id)->wherePivot('page_id', $id)->detach();

        //Aggiorno associazioni
        if (!empty($categories)):
            foreach ($categories as $category):
                $page->categories()->attach($id, [
                    'category_id' => $category,
                ]);
            endforeach;
        endif;

        $message = trans('admin.pages.updated', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Imposto il parent di tutte le pagine figlie a null
            $pages = Page::with('parent')->where('parent_id', $id)->get();

            if (!empty($pages)):
                foreach ($pages as $page):
                    $p = Page::findOrFail($page->id);
                    $p->parent_id = null;
                    $p->update();
                endforeach;
            endif;

            //Rimuovo tutte associazioni menu-page
            $page = Page::findOrFail($id);
            $page->menus()->where('id', $id)->wherePivot('menu_id', $id)->detach();

            //Elimino fisicamente l'eventuale cover
            if (!$page->image_cover == null):
                $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $page->image_cover;

                if (file_exists($path)) {
                    unlink($path);
                }
            endif;

            //Elimino pagina
            $page = Page::destroy($id);


            //Rigenero sort
            $pages = Page::with('parent')->orderBy('sort', 'ASC')->orderBy('parent_id', 'ASC')->get();

            if(!empty($pages)):
                $loop = 1;
                foreach($pages as $page):
                    $page->update(['sort' => $loop]);
                    $loop++;
                endforeach;
            endif;

            $message = trans('admin.pages.deleted');

            $view = view('admin.pages.list')->with([
                'pages' => $pages,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update visible status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateVisible(Request $request, $id)
    {

        if ($request->ajax()) {

            $visible = $request->is_visible;
            $fields['visible'] = $visible;

            $page = Page::findOrFail($id);
            $page->update($fields);

            $pages = Page::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.pages.list')->with([
                'pages' => $pages,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update is_home status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateIsHome(Request $request, $id)
    {

        if ($request->ajax()) {

            $ishome = $request->is_home;
            $fields['is_home'] = $ishome;
            $fields['visible'] = 1;
            $fields['on_home'] = 0;

            $this->resetHome();

            $page = Page::findOrFail($id);
            $page->update($fields);

            $pages = Page::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.pages.list')->with([
                'pages' => $pages,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update on_home status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOnHome(Request $request, $id)
    {

        if ($request->ajax()) {

            $onhome = $request->on_home;
            $fields['on_home'] = $onhome;

            $page = Page::findOrFail($id);

            //Se la pagina è la home, on_home rimane false
            if ($page->is_home):
                $fields['on_home'] = 0;
            endif;

            $page->update($fields);

            $pages = Page::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.pages.list')->with([
                'pages' => $pages,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update is_private status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateIsPrivate(Request $request, $id)
    {

        if ($request->ajax()) {

            $isprivate = $request->is_private;
            $fields['is_private'] = $isprivate;

            $page = Page::findOrFail($id);
            $page->update($fields);

            $pages = Page::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.pages.list')->with([
                'pages' => $pages,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update show_sidebar_dx status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateShowSidebarDx(Request $request, $id)
    {

        if ($request->ajax()) {

            $showsidebar = $request->show_sidebar_dx;
            $fields['show_sidebar_dx'] = $showsidebar;

            $page = Page::findOrFail($id);
            $page->update($fields);

            $pages = Page::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.pages.list')->with([
                'pages' => $pages,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    public function getPage($query)
    {

        //Creo breadcrumb e trovo pagina corrente
        $bc = MyHelper::createBreadcrumb($query);
        $cl = null;

        $breadcrumb = $bc['breadcrumb'];
        $currentPage = $bc['currentPage'];

        //Controllo se ci sono gallery associate
        $media = Mediarelatedmodel::where('modelname', 'Page')->first();
        if (!empty($media)):
            $cl = Mediacollection::with('mediaitems')->where('mediarelatedmodel_id', $media->id)->where('record_id', $currentPage->id)->get();
        endif;

        return view('layouts.page', [
            'page' => $currentPage,
            'breadcrumb' => $breadcrumb,
            'query' => $query,
            'cl' => $cl,

        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteCover(Request $request)
    {
        if ($request->ajax()) {

            $id = $request->id;
            $filename = $request->filename;
            $containerDiv = $request->containerDiv;
            $inputId = $request->inputId;

            /*
            //Elimino fisicamente il file
            $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $filename;

            if (file_exists($path)) {
            unlink($path);
            }

            $dir = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
             */

            $page = Page::findOrFail($id);
            $page->image_cover = null;
            $page->update();

            $replaceHtml = view('admin.medias.image_holder', [
                'btnId' => 'image_holder_btn',
                'imgId' => 'image_holder',
            ])
                ->render();

            $values = [
                'divtoreload' => $containerDiv,
                'inputId' => $inputId,
                'replaceHtml' => $replaceHtml,
            ];

            return $values;

        };
    }

     /**
     * @return \Illuminate\Http\Response
     */
    public function reorder(Request $request)
    {
        if ($request->ajax()) {

            $items = $request->items;
            $values = null;

            if(!empty($items)):
                foreach ($items as $item):
                    $values[$item['itemid']] = str_replace('#', '', $item['newPosition']);
                endforeach;
            endif;

            $pages = Page::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            if(!empty($pages)):
                foreach($pages as $page):
                    foreach($values as $key => $value):
                        if($page->id == $key):
                            $page->update(['sort' => $value]);
                        endif;
                    endforeach;
                endforeach;
            endif;

            $view = view('admin.pages.list')->with([
                'pages' => $pages,
            ])->render();

            $values = [
                'view' => $view,
                'values' => $values,

            ];


            return $values;

        };
    }


    //Recupero le pagine in evidenza
    public function getHiglights()
    {
        $pages = Page::with('parent')
            ->where('on_home', true)
            ->where('visible', true)
            ->where(function ($query) {
                $query->whereNull('is_home')
                    ->orWhere('is_home', 0);
            })
            ->where('is_private', false)
            ->orderBy('parent_id', 'ASC')
            ->orderBy('sort', 'ASC')
            ->get();

        //dd($pages);
        return $pages;

    }

    //Tolgo l'eventuale precedente pagina home (per sicurezza cerco su più pagine)
    public function resetHome()
    {
        $homePages = Page::where('is_home', true)->get();
        if (!empty($homePages)):
            foreach ($homePages as $hp):
                $page = Page::find($hp->id);
                $page->update(['is_home' => false]);
            endforeach;
        endif;
    }
}
