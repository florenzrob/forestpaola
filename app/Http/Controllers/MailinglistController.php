<?php

namespace App\Http\Controllers;

use App;
use App\Helpers\MyHelper;
use App\Http\Controllers\Controller;
use App\Models\Mailinglist;
use App\Models\MailinglistSent;
use App\Models\Mlgroup;
use App\Models\Setting;
use App\Models\Subscriber;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class MailinglistController extends Controller
{
    private $routeList = 'mailinglists.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $mailinglists = Mailinglist::with('groups')->orderBy('created_at', 'ASC')->get();

        return view('admin.mailinglists.index', [
            'mailinglists' => $mailinglists,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mailinglist = new Mailinglist;
        $groups = Mlgroup::orderBy('name', 'ASC')->get();

        return view('admin.mailinglists.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'mailinglist.store', //Salvataggio form
            'mailinglist' => $mailinglist,
            'groups' => $groups,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        //dd($fields);

        $attach = null;
        $folder = 'images/uploads/files';

        $att = isset($fields['attachment']) ? $fields['attachment'] : '';

        if (!empty($att)):
            $attach = MyHelper::fileUpload($att, $folder, false);
        endif;

        //Add/Update groups
        $groups = isset($fields['groups']) ? $fields['groups'] : null;

        //Tolgo file dall'array dell'update
        if (isset($fields['attachment'])):
            unset($fields['attachment']);
        endif;

        //Tolgo hid_attachment dall'array dell'update
        unset($fields['hid_attachment']);

        //Salvo il nome del file
        $fields['attachment'] = !empty($attach) ? $attach['filename'] : '';

        //Tolgo positions dall'array dell'update
        if (isset($fields['groups'])):
            unset($fields['groups']);
        endif;

        $mailinglist = Mailinglist::create($fields);

        //Last insert id
        $id = $mailinglist->id;

        //Aggiorno associazioni
        if (!empty($groups)):
            foreach ($groups as $group):
                $mailinglist->groups()->attach($id, [
                    'mlgroup_id' => $group,
                ]);
            endforeach;
        endif;

        $message = trans('admin.mailinglists.created');

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $query
     * * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mailinglist = Mailinglist::with('groups')->findOrFail($id);
        $groups = Mlgroup::orderBy('name', 'ASC')->get();

        return view('admin.mailinglists.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'mailinglist.update', //Salvataggio form
            'mailinglist' => $mailinglist,
            'groups' => $groups,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();
        $attach = null;
        $folder = 'images/uploads/files';
        $path = null;
        $att = isset($fields['attachment']) ? $fields['attachment'] : '';

        if (!empty($att)):
            $attach = MyHelper::fileUpload($att, $folder, false);
        endif;

        //$path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . 'files/' . $attach['filename'];

        //dd($fields);

        //Salvo il nome del file se caricato, altrimenti tengo quello corrente
        if (empty($attach)):
            $fields['attachment'] = $fields['hid_attachment'];
        else:
            $fields['attachment'] = $attach['filename'];
        endif;

        //Add/Update groups
        $groups = isset($fields['groups']) ? $fields['groups'] : null;

        //Tolgo hid_attachment dall'array dell'update
        unset($fields['hid_attachment']);

        //Tolgo positions dall'array dell'update
        if (isset($fields['groups'])):
            unset($fields['groups']);
        endif;

        //dd($fields);

        $mailinglist = Mailinglist::findOrFail($id);
        $mailinglist->update($fields);

        //Rimuovo tutte associazioni group-subscriber
        $mailinglist->groups()->where('id', $id)->wherePivot('mailinglist_id', $id)->detach();

        //Aggiorno associazioni
        if (!empty($groups)):
            foreach ($groups as $group):
                $mailinglist->groups()->attach($id, [
                    'mlgroup_id' => $group,
                ]);
            endforeach;
        endif;

        $message = trans('admin.mailinglists.updated');

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Rimuovo tutte associazioni groups-subscriber
            $mailinglist = Mailinglist::findOrFail($id);
            $mailinglist->groups()->where('id', $id)->wherePivot('mailinglist_id', $id)->detach();

            //Elimino mailinglist
            $mailinglist = mailinglist::destroy($id);

            $mailinglists = Mailinglist::with('groups')->orderBy('created_at', 'ASC')->get();

            $message = trans('admin.mailinglists.deleted');

            $view = view('admin.mailinglists.list')->with([
                'mailinglists' => $mailinglists,
                'message' => $message,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update Send mailing
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function send(Request $request)
    {

        if ($request->ajax()) {
            $recipient = null;
            $id = $request->id;

            $mailinglist = Mailinglist::with(['groups' => function ($query) {
                $query->with('subscribers');
            }])->find($id);

            //dd($mailinglist);
            if (!empty($mailinglist->groups)):
                foreach ($mailinglist->groups as $group):
                    if (!empty($group->subscribers)):
                        foreach ($group->subscribers as $subscriber):
                            $recipient[$subscriber->email] = [
                                'fullname' => $subscriber->name . ' ' . $subscriber->surname,
                                'email' => $subscriber->email,
                            ];
                        endforeach;
                    endif;
                endforeach;
            endif;

            if (!empty($recipient)):

                $res = $this->sendMailinglist($recipient, $mailinglist);

                $sent = $res['sent'];
                $recipients = $res['recipients'];
                $errors = $res['errors'];

                $success = $sent == true ? 'success' : 'error';

                //Aggiorno dati mailinglist
                $fields['sent_date'] = Carbon::today()->format('Y-m-d');
                $fields['sent'] = true;
                $mailinglist->update($fields);

                $mailinglists = Mailinglist::with('groups')->orderBy('created_at', 'ASC')->get();

                $message = $sent == true ? trans('messages.mail_success') : trans('messages.mail_failure');

                $view = view('admin.mailinglists.list')->with([
                    'mailinglists' => $mailinglists,
                    'res' => $res,
                    'errors' => $errors,
                    'recipients' => $recipients,
                    'message' => $message,
                    'success' => $success,
                ])
                    ->with($success, $message)
                    ->with('errors', $errors)
                    ->render();

                $values = [
                    'view' => $view,
                    'res' => $res,
                ];

                return $values;

                //return response()->json(['responseText' => trans('messages.mail_success')], 200);
            else:
                return response()->json(['responseText' => trans('messages.mail_failure')], 200);
            endif;

        };
    }

    public function sendMailinglist($recipient, $mailinglist)
    {

        //dd($recipient);

        $settings = Setting::first();

        $fields['settings'] = $settings;
        $fields['mailinglist'] = $mailinglist;

        $fromAddress = env('MAIL_FROM_ADDRESS');
        $fromName = env('MAIL_FROM_NAME');

        //Send email to customer
        $subject = $mailinglist->subject;

        $err = [];
        /*
        $r = [
        'fullname' => 'rob',
        'email' => 'roberto_mantovani@yahoo.it',
        ];

        for ($x = 0; $x < 70; $x++):
        $emails[] = $r['email'];
        endfor;
         */

        foreach ($recipient as $recipe):
            $emails[] = $recipe['email'];
        endforeach;

        if (empty($err)):

            //Divido le mail in chunks e memorizzo nella tabella mailinglist_sent.
            //Ogni chunk avrà una data di invio diversa
            //Un cron orario leggerà questa tabella e spedirà le mail
            $dataSent = null;

            foreach (array_chunk($emails, 100) as $e):
                $strEmails = null;
                if (!empty($e)):
                    foreach ($e as $x):
                        $strEmails = !empty($strEmails) ? $strEmails . ',' . $x : $x;
                    endforeach;
                endif;

                $dataSent = empty($dataSent) ? Carbon::now()->addMinutes(2)->format('Y-m-d H.i:s') : Carbon::parse($dataSent)->addMinutes(60)->format('Y-m-d H.i:s');
                $data = null;

                $data['mailinglist_id'] = $mailinglist->id;
                $data['recipients'] = $strEmails;
                $data['data_sent'] = $dataSent;

                $ms = MailinglistSent::create($data);

            endforeach;
            return [
                'sent' => true,
                'recipients' => $emails,
                'errors' => null,
            ];
        else:

            return [
                'sent' => false,
                'recipients' => $emails,
                'errors' => $err,
            ];
        endif;

    }

    /**
     * Remove user form mailinglist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unsubscribe(Request $request)
    {
        if ($request->ajax()) {

            $data = $request['formValues'];
            foreach ($request['formValues'] as $field):
                $fields[$field['name']] = $field['value'];
            endforeach;

            $email = $fields['email'];

            //Rimuovo tutte associazioni groups-subscriber
            $subscriber = Subscriber::where('email', $email)->first();

            if (!empty($subscriber->id)):
                $id = $subscriber->id;
                $subscriber->groups()->where('id', $id)->wherePivot('subscriber_id', $id)->detach();

                //Elimino subscriber
                $subscriber = Subscriber::destroy($id);

                $state = 'success';
                $message = trans('admin.subscribers.deleted');
            else:
                $state = 'danger';
                $message = trans('admin.subscribers.mail_not_found');

            endif;

            return response()->json(['responseText' => $message, 'class' => $state, 'formid' => 'unsubscribeForm'], 200);
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CRON
     *
     ***********************************************************************************************************************************
     */

    //Scansiona ogni ora la tabella mailinglist_sents e invia l'eventuale mailinglist
    public function testCron()
    {

        $settings = Setting::first();

        $fields['settings'] = $settings;

        $fromAddress = env('MAIL_FROM_ADDRESS');
        $fromName = env('MAIL_FROM_NAME');

        $emails[] = 'p.barducci@libero.it';

        $lists = MailinglistSent::where('sent', false)->orderBy('data_sent', 'ASC')->get();

        Mail::send('emails.test',
            array(
                'fields' => $fields,
            ), function ($message) use ($fromAddress, $fromName, $emails, $lists) {

                $message->from($fromAddress, $fromName);
                $message->to($emails);
                $message->subject(count($lists) . ' ' . env('TEST_MAIL_SUBJECT') . ' ' . Carbon::now()->format('Y-m-d H:i:s'));

            });

        return count($lists);

    }

    //Scansiona ogni ora la tabella mailinglist_sents e invia l'eventuale mailinglist
    public function queueMailinglist()
    {

        $settings = Setting::first();

        $fields['settings'] = $settings;

        $fromAddress = env('MAIL_FROM_ADDRESS');
        $fromName = env('MAIL_FROM_NAME');

        //Controllo le mailinglist da inviare
        $lists = MailinglistSent::where('sent', false)->orderBy('data_sent', 'ASC')->get();

        if (!empty($lists)):

            //dd($lists);
            foreach ($lists as $list):

                $data = [];
                $mailinglist = Mailinglist::find($list->mailinglist_id);

                //Spedisco solo quelle con data precedente all'attuale
                if (Carbon::parse($list->data_sent)->format('Y-m-d H:i:s') <= Carbon::now()->format('Y-m-d H:i:s')):
                    /*
                    $emails[] = 'roberto_mantovani@yahoo.it';

                    Mail::send('emails.test',
                    array(
                    'fields' => $fields,
                    ), function ($message) use ($fromAddress, $fromName, $emails) {

                    $message->from($fromAddress, $fromName);
                    $message->to($emails);
                    $message->subject('1 ' . env('TEST_MAIL_SUBJECT') . ' ' . Carbon::now()->format('Y-m-d H:i:s'));

                    });
                     */
                    if (isset($mailinglist->id)):

                        $path = null;

                        if (!empty($mailinglist->attachment)):
                            //$path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . 'files/' . $mailinglist->attachment;
                            $path = 'images/uploads/files/' . $mailinglist->attachment;
                        endif;

                        $fields['mailinglist'] = $mailinglist;
                        $fields['path'] = $path;

                        $recs = null;
                        $recipients = explode(',', $list->recipients);

                        //dd($recipients);

                        if (!empty($recipients)):
                            foreach ($recipients as $recipient):
                                $recs[] = str_replace(' ', '', $recipient);
                            endforeach;

                            //dd($recs);
                            $subject = $mailinglist->subject;

                            if (!empty($recs)):

                                $count = count($recs);
                                $loop = 0;

                                foreach ($recs as $rec):

                                    //Limite invio su keliweb: 180 mail/h
                                    //dd($rec);
                                    Mail::send('emails.newsletter',
                                        array(
                                            'fields' => $fields,
                                        ), function ($message) use ($fromAddress, $fromName, $subject, $rec, $mailinglist, $path) {

                                            $message
                                                ->from($fromAddress, $fromName)
                                                ->bcc($rec)
                                                ->subject($subject);

                                            if (!empty($path)):
                                                $message->attach(public_path($path));
                                            endif;

                                        });

                                    $loop++;
                                endforeach;

                                //Setto come spedito
                                if ($loop >= $count):
                                    $data = [];
                                    $sent = MailinglistSent::find($list->id);
                                    $data['sent'] = 1;
                                    $sent->update($data);
                                endif;
                            endif;
                        endif;
                    endif;
                endif;

            endforeach;
        endif;

        return 1;

    }

    public function previewEmail()
    {
        $settings = Setting::first();
        $mailinglist = Mailinglist::orderBy('id', 'DESC')->where('sent', true)->first();

        $fields['settings'] = $settings;
        $fields['mailinglist'] = $mailinglist;
        $fields['path'] = !empty($mailinglist->attachment) ? $mailinglist->attachment : null;

        return view('emails.newsletter', [
            'fields' => $fields,
        ]);
    }

}
