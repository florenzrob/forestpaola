<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\Mlgroup;
use Illuminate\Http\Request;

class MlgroupController extends Controller
{
    private $routeList = 'mlgroups.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mlgroups = Mlgroup::orderBy('name', 'ASC')->get();

        //dd($mlgroup);

        return view('admin.mailing_groups.index', [
            'mlgroups' => $mlgroups,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mlgroup = new Mlgroup;

        return view('admin.mailing_groups.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'mlgroup.store', //Salvataggio form
            'mlgroup' => $mlgroup,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        $visible = isset($fields['visible']) ? 1 : 0;

        $fields['visible'] = $visible;

        $mlgroup = Mlgroup::create($fields);

        //Last insert id
        $id = $mlgroup->id;

        $message = trans('admin.mlgroups.created', ['name' => $fields['name']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $query
     * * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mlgroup = Mlgroup::findOrFail($id);

        return view('admin.mailing_groups.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'mlgroup.update', //Salvataggio form
            'mlgroup' => $mlgroup,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        $visible = isset($fields['visible']) ? 1 : 0;

        $fields['visible'] = $visible;

        $mlgroup = Mlgroup::findOrFail($id);
        $mlgroup->update($fields);

        $message = trans('admin.mlgroups.updated', ['name' => $fields['name']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Rimuovo tutte associazioni groups-subscriber
            $mlgroup = Mlgroup::findOrFail($id);
            $mlgroup->subscribers()->where('id', $id)->wherePivot('mlgroup_id', $id)->detach();

            //Elimino gruppo
            $mlgroup = Mlgroup::destroy($id);

            $mlgroups = Mlgroup::orderBy('name', 'ASC')->get();

            $message = trans('admin.mlgroups.deleted');

            $view = view('admin.mailing_groups.list')->with([
                'mlgroups' => $mlgroups,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update visible status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateVisible(Request $request, $id)
    {

        if ($request->ajax()) {

            $visible = $request->is_visible;
            $fields['visible'] = $visible;

            $mlgroup = Mlgroup::findOrFail($id);
            $mlgroup->update($fields);

            $mlgroup = Mlgroup::orderBy('name', 'ASC')->get();

            $view = view('admin.mailing_groups.list')->with([
                'mlgroup' => $mlgroup,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

}
