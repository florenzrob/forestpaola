<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{

    //private $routeList = 'pages.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.medias.dropzone', [

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $image = $request->file('file');

        $imageName = $image->getClientOriginalName();

        $upload_success = $image->move(public_path('images/uploads'), $imageName);

        if ($upload_success) {
            return response()->json(
                [
                    'success' => $upload_success,
                    'status' => 200,
                    'filename' => $imageName,
                ]
            );
        }
        // Else, return error 400
        else {
            return response()->json('error', 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {

            //$slug = $request->slug;
            $filename = $request->filename;
            $urls = null;

            //Elimino fisicamente il file
            $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $filename;

            if (file_exists($path)) {
                unlink($path);
            }

            $dir = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
            $files = File::files($dir);

            if (!empty($files)) {
                foreach ($files as $path) {
                    $urls[] = [
                        'filename' => basename($path),
                        'extension' => pathinfo($path, PATHINFO_EXTENSION),
                    ];
                }
            }

            $view = view('admin.medias.uploader_list')->with([
                'urls' => $urls,
            ])->render();

            $divToReload = 'uploader-list';

            $values = [
                'view' => $view,
                'divtoreload' => $divToReload,
                'urls' => $urls,
            ];

            return $values;

        };
    }

    /*
 ***********************************************************************************************************************************
 *
 * CUSTOM
 *
 ***********************************************************************************************************************************
 */

}
