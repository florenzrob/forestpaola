<?php

namespace App\Http\Controllers;

use App;
use App\Helpers\MyHelper;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Mediacollection;
use App\Models\Mediarelatedmodel;
use App\Models\Position;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    private $routeList = 'articles.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('parent')
            ->with('category')
            ->with('positions')
            ->with('tags')
            ->orderBy('sort', 'ASC')
            ->orderBy('parent_id', 'ASC')
            ->orderBy('category_id', 'ASC')
            ->get();

        //dd($articles);

        return view('admin.articles.index', [
            'articles' => $articles,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new Article;
        $articles = Article::get();
        $categories = Category::orderBy('title', 'ASC')->get();
        $positions = Position::orderBy('position', 'ASC')->get();
        $tags = Tag::orderBy('tag', 'ASC')->get();

        return view('admin.articles.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'article.store', //Salvataggio form
            'article' => $article,
            'articles' => $articles,
            'categories' => $categories,
            'positions' => $positions,
            'tags' => $tags,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        //Se presente cover, prendo solo nome del file
        if (!empty($fields['image_cover'])):
            //dd(basename($fields['company_logo']));
            $fields['image_cover'] = basename($fields['image_cover']);
        endif;

        //Add/Update positions
        $positions = isset($fields['positions']) ? $fields['positions'] : null;

        //Tolgo positions dall'array dell'update
        if (isset($fields['positions'])):
            unset($fields['positions']);
        endif;

        //Add/Update tags
        $tags = isset($fields['tags']) ? $fields['tags'] : null;

        //Tolgo tags dall'array dell'update
        if (isset($fields['tags'])):
            unset($fields['tags']);
        endif;

        $visible = isset($fields['visible']) ? 1 : 0;
        $isprivate = isset($fields['is_private']) ? 1 : 0;
        $showsidebardx = isset($fields['show_sidebar_dx']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['title'];
        $slug = $fields['title'];

        //Cerco il max value del sort
        $maxValue = Article::max('sort');
        $maxValue = $maxValue < 1 ? 1 : $maxValue+1;

        $sort = isset($fields['sort']) ? $fields['sort'] : $maxValue;

        $fields['visible'] = $visible;
        $fields['is_private'] = $isprivate;
        $fields['show_sidebar_dx'] = $showsidebardx;
        $fields['slug'] = str_slug($slug, '-');
        $fields['sort'] = $sort;

        //Cambio formato data
        if (isset($fields['publish_from']) && !empty($fields['publish_from'])) {
            $dateFrom = MyHelper::formatDate($fields['publish_from']);
            $fields['publish_from'] = $dateFrom;
        }

        if (isset($fields['publish_to']) && !empty($fields['publish_to'])) {
            $dateTo = MyHelper::formatDate($fields['publish_to']);
            $fields['publish_to'] = $dateTo;
        }

        /*
        //Image cover
        if ($request->hasFile('image_cover')) {

        if ($request->file('image_cover')->isValid()) {
        $allegato = $request->file('image_cover');

        $upload = MyHelper::fileUpload($allegato);
        $fields['image_cover'] = $upload['filename'];
        }
        }
         */

        $article = Article::create($fields);

        //Last insert id
        $id = $article->id;

        //Aggiorno associazioni
        if (!empty($positions)):
            foreach ($positions as $position):
                $article->positions()->attach($id, [
                    'position_id' => $position,
                ]);
            endforeach;
        endif;

        if (!empty($tags)):
            foreach ($tags as $tag):
                $article->tags()->attach($id, [
                    'tag_id' => $tag,
                ]);
            endforeach;
        endif;

        $message = trans('admin.articles.created', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $query
     * * @param  string  $articleSlug
     * @return \Illuminate\Http\Response
     */
    public function show( /*$query, */$articleSlug)
    {

        //dd($query);

        $page = null;
        $cl = null;

        $article = Article::with(['category' => function ($q) {
            $q->with('pages');
        }])
            ->where('slug', $articleSlug)->first();

        //Cerco la pagina associata che stavo guardando. Nella modalità url con slug anche della pagina
        /*
        if (!empty($query)):
        foreach ($article->category->pages as $p):
        if (strpos($query, $p->slug) !== false):
        $page = $p;
        endif;
        endforeach;

        //dd($page);
        endif;
         */

        //Creo breadcrumb e trovo pagina corrente
        $bc = MyHelper::createBreadcrumb($articleSlug);

        $breadcrumb = $bc['breadcrumb'];
        $currentPage = $bc['currentPage'];

        //Controllo se ci sono gallery associate
        $media = Mediarelatedmodel::where('modelname', 'Article')->first();

        //dd($currentPage);
        if (!empty($media)):
            $cl = Mediacollection::with('mediaitems')->where('mediarelatedmodel_id', $media->id)->where('record_id', $article->id)->get();
        endif;

        /*
        TODO
        $items = explode('/', $query);
        foreach ($items as $item):
        $page = Page::with(['categories' => function ($q) {
        $q->where('visible', true)
        ->with(['articles' => function ($q1) {
        $q1->where('visible', true)
        ->whereDate('publish_from', '<=', Carbon::today()->format('Y-m-d'))
        ->whereDate('publish_to', '>=', Carbon::today()->format('Y-m-d'))
        ->orWhere('publish_to', null)
        ->orderBy('parent_id', 'ASC')
        ->orderBy('category_id', 'ASC')
        ->orderBy('sort', 'ASC');
        }]);
        }])

        ->where('visible', true)->where('slug', $item)->first();

        $breadcrumb[] = (object) [
        'id' => $page->id,
        'slug' => $page->slug,
        'title' => $page->title,
        ];

        $currentPage = $page;

        endforeach;
         */
        //dd($articles);

        return view('layouts.article', [
            'article' => $article,
            'page' => $currentPage,
            'breadcrumb' => $breadcrumb,
            'cl' => $cl,
            //'query' => $query,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::with('positions')->findOrFail($id);
        $articles = Article::where('id', '<>', $id)->orderBy('title', 'ASC')->get();
        $categories = Category::orderBy('title', 'ASC')->get();
        $positions = Position::orderBy('position', 'ASC')->get();
        $tags = Tag::orderBy('tag', 'ASC')->get();

        return view('admin.articles.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'article.update', //Salvataggio form
            'article' => $article,
            'articles' => $articles,
            'categories' => $categories,
            'positions' => $positions,
            'tags' => $tags,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        //Se presente cover, prendo solo nome del file
        if (!empty($fields['image_cover'])):
            //dd(basename($fields['company_logo']));
            $fields['image_cover'] = basename($fields['image_cover']);
        endif;

        //Add/Update positions
        $positions = isset($fields['positions']) ? $fields['positions'] : null;

        //Tolgo positions dall'array dell'update
        if (isset($fields['positions'])):
            unset($fields['positions']);
        endif;

        //Add/Update tags
        $tags = isset($fields['tags']) ? $fields['tags'] : null;

        //Tolgo positions dall'array dell'update
        if (isset($fields['tags'])):
            unset($fields['tags']);
        endif;

        $visible = isset($fields['visible']) ? 1 : 0;
        $isprivate = isset($fields['is_private']) ? 1 : 0;
        $showsidebardx = isset($fields['show_sidebar_dx']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['title'];
        $slug = $fields['title'];

        $fields['visible'] = $visible;
        $fields['is_private'] = $isprivate;
        $fields['show_sidebar_dx'] = $showsidebardx;
        $fields['slug'] = str_slug($slug, '-');

        //Cambio formato data
        if (isset($fields['publish_from']) && !empty($fields['publish_from'])) {
            $dateFrom = MyHelper::formatDate($fields['publish_from']);
            $fields['publish_from'] = $dateFrom;
        }

        if (isset($fields['publish_to']) && !empty($fields['publish_to'])) {
            $dateTo = MyHelper::formatDate($fields['publish_to']);
            $fields['publish_to'] = $dateTo;
        }

        /*
        //Image cover
        if ($request->hasFile('image_cover')) {

        if ($request->file('image_cover')->isValid()) {
        $allegato = $request->file('image_cover');

        $upload = MyHelper::fileUpload($allegato);
        $fields['image_cover'] = $upload['filename'];
        }
        }
         */

        $article = Article::findOrFail($id);
        $article->update($fields);

        //Rimuovo tutte associazioni article-position
        $article->positions()->where('id', $id)->wherePivot('article_id', $id)->detach();

        //Rimuovo tutte associazioni article-tag
        $article->tags()->where('id', $id)->wherePivot('article_id', $id)->detach();

        //Aggiorno associazioni
        if (!empty($positions)):
            foreach ($positions as $position):
                $article->positions()->attach($id, [
                    'position_id' => $position,
                ]);
            endforeach;
        endif;

        if (!empty($tags)):
            foreach ($tags as $tag):
                $article->tags()->attach($id, [
                    'tag_id' => $tag,
                ]);
            endforeach;
        endif;

        $message = trans('admin.articles.updated', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Imposto il parent di tutte le pagine figlie a null
            $articles = Article::with('parent')
                ->with('category')
                ->orderBy('parent_id', 'ASC')
                ->orderBy('category_id', 'ASC')
                ->orderBy('sort', 'ASC')
                ->where('parent_id', $id)->get();

            if (!empty($articles)):
                foreach ($articles as $article):
                    $a = Article::findOrFail($article->id);
                    $a->parent_id = null;
                    $a->update();
                endforeach;
            endif;

            //Rimuovo tutte associazioni article-position
            $article = Article::findOrFail($id);
            $article->positions()->where('id', $id)->wherePivot('article_id', $id)->detach();

            //Rimuovo tutte associazioni article-tag
            $article->tags()->where('id', $id)->wherePivot('article_id', $id)->detach();

            //Elimino fisicamente l'eventuale cover
            if (!$article->image_cover == null):
                $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $article->image_cover;

                if (file_exists($path)) {
                    unlink($path);
                }
            endif;

            //Elimino articolo
            $article = Article::destroy($id);


            //Rigenero sort
            $articles = Article::with('parent')
                ->with('category')
                ->orderBy('sort', 'ASC')
                ->orderBy('parent_id', 'ASC')
                ->orderBy('category_id', 'ASC')
                ->get();

                if(!empty($articles)):
                    $loop = 1;
                    foreach($articles as $article):
                        $article->update(['sort' => $loop]);
                        $loop++;
                    endforeach;
                endif;


            $message = trans('admin.articles.deleted');

            $view = view('admin.articles.list')->with([
                'articles' => $articles,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update visible status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateVisible(Request $request, $id)
    {

        if ($request->ajax()) {

            $visible = $request->is_visible;
            $fields['visible'] = $visible;

            $article = Article::findOrFail($id);
            $article->update($fields);

            $articles = Article::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.articles.list')->with([
                'articles' => $articles,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update is_private status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateIsPrivate(Request $request, $id)
    {

        if ($request->ajax()) {

            $isprivate = $request->is_private;
            $fields['is_private'] = $isprivate;

            $article = Article::findOrFail($id);
            $article->update($fields);

            $articles = Article::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.articles.list')->with([
                'articles' => $articles,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update show_sidebar_dx status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateShowSidebarDx(Request $request, $id)
    {

        if ($request->ajax()) {

            $showsidebar = $request->show_sidebar_dx;
            $fields['show_sidebar_dx'] = $showsidebar;

            $article = Article::findOrFail($id);
            $article->update($fields);

            $articles = Article::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.articles.list')->with([
                'articles' => $articles,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteCover(Request $request)
    {
        if ($request->ajax()) {

            $id = $request->id;
            $filename = $request->filename;
            $containerDiv = $request->containerDiv;
            $inputId = $request->inputId;

            /*
            //Elimino fisicamente il file
            $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $filename;

            if (file_exists($path)) {
            unlink($path);
            }

            $dir = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
             */

            $article = Article::findOrFail($id);
            $article->image_cover = null;
            $article->update();

            $replaceHtml = view('admin.medias.image_holder', [
                'btnId' => 'image_holder_btn',
                'imgId' => 'image_holder',
            ])
                ->render();

            $values = [
                'divtoreload' => $containerDiv,
                'inputId' => $inputId,
                'replaceHtml' => $replaceHtml,
            ];

            return $values;

        };
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function reorder(Request $request)
    {
        if ($request->ajax()) {

            $items = $request->items;
            $values = null;

            if (!empty($items)):
                foreach ($items as $item):
                    $values[$item['itemid']] = str_replace('#', '', $item['newPosition']);
                endforeach;
            endif;

            $articles = Article::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            if (!empty($articles)):
                foreach ($articles as $article):
                    foreach ($values as $key => $value):
                        if ($article->id == $key):
                            $article->update(['sort' => $value]);
                        endif;
                    endforeach;
                endforeach;
            endif;

            $view = view('admin.articles.list')->with([
                'articles' => $articles,
            ])->render();

            $values = [
                'view' => $view,
                'values' => $values,

            ];

            return $values;

        };
    }

    //Recupero gli articoli in evidenza
    public function getHiglights()
    {
        $tag = Tag::where('slug', 'highlights')->first();

        $articles = Article::where('is_private', false)
            ->where('visible', true)
            ->whereHas('tags', function ($q) use ($tag) {
                $q->where('tag_id', $tag->id);
            })
            ->where(function ($q) {
                $q->whereDate('publish_from', '<=', Carbon::today()->format('Y-m-d'))
                    ->whereDate('publish_to', '>=', Carbon::today()->format('Y-m-d'))
                    ->orWhere('publish_to', null);
            })
            ->orderBy('category_id', 'asc')
            ->orderBy('sort', 'asc')
            ->get();

        //dd($articles);
        return $articles;

    }

}
