<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    private $routeList = 'tags.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::orderBy('tag', 'ASC')->get();

        //dd($tags);

        return view('admin.tags.index', [
            'tags' => $tags,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tag = new Tag;

        return view('admin.tags.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'tag.store', //Salvataggio form
            'tag' => $tag,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        $system = isset($fields['system']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['tag'];
        $slug = $fields['tag'];

        $fields['system'] = $system;
        $fields['slug'] = str_slug($slug, '-');

        $tag = Tag::create($fields);

        $message = trans('admin.tags.created', ['name' => $fields['tag']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);

        return view('admin.tags.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'tag.update', //Salvataggio form
            'tag' => $tag,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        $system = isset($fields['system']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['tag'];
        $slug = $fields['tag'];

        $fields['system'] = $system;
        $fields['slug'] = str_slug($slug, '-');

        $tag = Tag::findOrFail($id);
        $tag->update($fields);

        $message = trans('admin.tags.updated', ['name' => $fields['tag']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Rimuovo tutte associazioni article-tag
            $tag = Tag::findOrFail($id);
            $tag->articles()->where('id', $id)->wherePivot('tag_id', $id)->detach();

            //Elimino tag
            $tag = Tag::destroy($id);

            $tags = Tag::orderBy('tag', 'ASC')->get();

            $message = trans('admin.tags.deleted');

            $view = view('admin.tags.list')->with([
                'tags' => $tags,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update system status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSystem(Request $request, $id)
    {

        if ($request->ajax()) {

            $system = $request->is_system;
            $fields['system'] = $system;

            $tag = Tag::findOrFail($id);
            $tag->update($fields);

            $tags = Tag::orderBy('tag', 'ASC')->get();

            $view = view('admin.tags.list')->with([
                'tags' => $tags,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

}
