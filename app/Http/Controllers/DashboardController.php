<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Chiamate ai vari moduli nella dashboard
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.dashboard', [

        ]);

    }

}
