<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Models\Mlgroup;
use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    private $routeList = 'subscribers.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::with('groups')->orderBy('surname', 'ASC')->orderBy('name', 'ASC')->get();

        //dd($subscribers);

        return view('admin.subscribers.index', [
            'subscribers' => $subscribers,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subscriber = new Subscriber;
        $groups = Mlgroup::orderBy('name', 'ASC')->get();

        return view('admin.subscribers.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'subscriber.store', //Salvataggio form
            'subscriber' => $subscriber,
            'groups' => $groups,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        //Add/Update groups
        $groups = isset($fields['groups']) ? $fields['groups'] : null;

        //Tolgo positions dall'array dell'update
        if (isset($fields['groups'])):
            unset($fields['groups']);
        endif;

        $active = isset($fields['active']) ? 1 : 0;

        $fields['active'] = $active;

        $subscriber = Subscriber::create($fields);

        //Last insert id
        $id = $subscriber->id;

        //Aggiorno associazioni
        if (!empty($groups)):
            foreach ($groups as $group):
                $subscriber->groups()->attach($id, [
                    'mlgroup_id' => $group,
                ]);
            endforeach;
        endif;

        $message = trans('admin.subscribers.created', ['name' => $fields['name'] . ' ' . $fields['surname']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $query
     * * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscriber = Subscriber::with('groups')->findOrFail($id);
        $groups = Mlgroup::orderBy('name', 'ASC')->get();

        return view('admin.subscribers.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'subscriber.update', //Salvataggio form
            'subscriber' => $subscriber,
            'groups' => $groups,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        //Add/Update groups
        $groups = isset($fields['groups']) ? $fields['groups'] : null;

        //Tolgo positions dall'array dell'update
        if (isset($fields['groups'])):
            unset($fields['groups']);
        endif;

        $active = isset($fields['active']) ? 1 : 0;

        $fields['active'] = $active;

        $subscriber = Subscriber::findOrFail($id);
        $subscriber->update($fields);

        //Rimuovo tutte associazioni group-subscriber
        $subscriber->groups()->where('id', $id)->wherePivot('subscriber_id', $id)->detach();

        //Aggiorno associazioni
        if (!empty($groups)):
            foreach ($groups as $group):
                $subscriber->groups()->attach($id, [
                    'mlgroup_id' => $group,
                ]);
            endforeach;
        endif;

        $message = trans('admin.subscribers.updated', ['name' => $fields['name'] . ' ' . $fields['surname']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Rimuovo tutte associazioni groups-subscriber
            $subscriber = Subscriber::findOrFail($id);
            $subscriber->groups()->where('id', $id)->wherePivot('subscriber_id', $id)->detach();

            //Elimino subscriber
            $subscriber = Subscriber::destroy($id);

            $subscribers = Subscriber::with('groups')->orderBy('surname', 'ASC')->orderBy('name', 'ASC')->get();

            $message = trans('admin.subscribers.deleted');

            $view = view('admin.subscribers.list')->with([
                'subscribers' => $subscribers,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update active status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateActive(Request $request, $id)
    {

        if ($request->ajax()) {

            $active = $request->is_active;
            $fields['active'] = $active;

            $subscriber = Subscriber::findOrFail($id);
            $subscriber->update($fields);

            $subscribers = Subscriber::with('groups')->orderBy('surname', 'ASC')->orderBy('name', 'ASC')->get();

            $view = view('admin.subscribers.list')->with([
                'subscribers' => $subscribers,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }


     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subscribe(Request $request)
    {
        if ($request->ajax()) {

            $data = $request['formValues'];
            foreach ($request['formValues'] as $field):
                $fields[$field['name']] = $field['value'];
            endforeach;

            $user = Subscriber::where('email',  $fields['email'])->first();
            if(empty($user)):
                $subscriber['name'] = $fields['name'];
                $subscriber['surname'] = $fields['surname'];
                $subscriber['email'] = $fields['email'];
                $subscriber['active'] = 1;

                $subscriber = Subscriber::create($subscriber);
            else:
                return response()->json(['responseText' => trans('messages.nl_subscribe_error'), 'class' => 'danger', 'formid' => 'newsletterForm'], 200);

            endif;


            return response()->json(['responseText' => trans('messages.nl_subscribe_success'), 'class' => 'success', 'formid' => 'newsletterForm'], 200);

        };
    }

}
