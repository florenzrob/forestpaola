<?php

namespace App\Http\Controllers;

use App\Helpers\MyHelper;
use App\Models\Article;
use App\Models\Category;
use App\Models\Mediacollection;
use App\Models\Mediaitem;
use App\Models\Mediaposition;
use App\Models\Mediarelatedmodel;
use App\Models\Mediatype;
use App\Models\Page;
use App\Models\Setting;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{

    private $routeList = 'galleries.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $galleries = $this->getRelatedRecords();

        return view('admin.medias.galleries.index', [
            'galleries' => $galleries,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cl = new Mediacollection;
        $types = Mediatype::orderBy('type', 'ASC')->get();
        $positions = Mediaposition::orderBy('name', 'ASC')->get();
        $relateds = Mediarelatedmodel::orderBy('modelname', 'ASC')->get();

        $items = $this->imageList();

        return view('admin.medias.galleries.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'gallery.store', //Salvataggio form
            'cl' => $cl,
            'types' => $types,
            'positions' => $positions,
            'relateds' => $relateds,
            'medias' => $items['medias'],
            'folders' => $items['folders'],

        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        //dd($fields);

        $images = null;

        //Rimuovo le immagini nulle
        if(!empty($fields['images'])):
            foreach ($fields['images'] as $key => $img):
                if (!empty($img)):
                    $images[] = [
                        'path' => $img,
                    ];
                endif;

            endforeach;
        endif;

        unset($fields['images']);

        //dd($fields);
        $gallery = Mediacollection::create($fields);

        if (!empty($images)):
            foreach ($images as $image):
                $image['mediacollection_id'] = $gallery->id;
                //dd($image);
                $items = Mediaitem::create($image);
            endforeach;
        endif;

        $message = trans('admin.medias.gallery_created', ['name' => $fields['name']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cl = Mediacollection::findOrFail($id);
        $types = Mediatype::orderBy('type', 'ASC')->get();
        $positions = Mediaposition::orderBy('name', 'ASC')->get();
        $relateds = Mediarelatedmodel::orderBy('modelname', 'ASC')->get();
        $enumsLinktype = MyHelper::getEnumValues('mediaitems', 'link_type');
        $enumsLinktarget = MyHelper::getEnumValues('mediaitems', 'target');

        $items = $this->imageList();

        //Recupero il titolo della pagina, categoria o articolo collegato
        $nameSpace = '\\App\Models\\';
        $record = null;

        if (!empty($cl->mediarelatedmodel_id)):
            $model = app($nameSpace . ucfirst($cl->mediarelatedmodel->modelname));
            $records = $model::get();
        endif;

        //dd($cl->mediaitems);

        return view('admin.medias.galleries.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'gallery.update', //Salvataggio form
            'cl' => $cl,
            'records' => $records,
            'types' => $types,
            'positions' => $positions,
            'relateds' => $relateds,
            'medias' => $items['medias'],
            'folders' => $items['folders'],
            'enumsLinktype' => $enumsLinktype,
            'enumsLinktarget' => $enumsLinktarget,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $fields = $request->all();

        //dd($fields);

        $records = null;
        $ids = $fields['ids'];

        foreach ($ids as $row):
            $records[$row] = [
                'path' => $fields['path_' . $row],
                'link_type' => $fields['link_type_' . $row],
                'link' => $fields['link_' . $row],
                'target' => $fields['target_' . $row],
                'title' => $fields['title_' . $row],
                'subtitle' => $fields['subtitle_' . $row],
                'content' => $fields['content_' . $row],
            ];

        endforeach;

        $galleryFields = [
            'mediatype_id' => $fields['mediatype_id'],
            'mediaposition_id' => $fields['mediaposition_id'],
            'mediarelatedmodel_id' => $fields['mediarelatedmodel_id'],
            'record_id' => $fields['record_id'],
            'name' => $fields['name'],
            'description' => $fields['description'],
        ];

        $gallery = Mediacollection::findOrFail($id);
        $gallery->update($galleryFields);

        if (!empty($records)):
            foreach ($records as $key => $record):
                $record['mediacollection_id'] = $gallery->id;
                $item = Mediaitem::find($key);
                $item->update($record);
            endforeach;
        endif;

        $message = trans('admin.medias.gallery_updated', ['name' => $fields['name']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {

        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /*
     ***********************************************************************************************************************************
     * IMAGES
     ***********************************************************************************************************************************
     */

    //Mostra elenco file della cartella images
    public function getImages()
    {

        $items = $this->imageList();

        //dd($items);
        return view('admin.medias.images.index', [
            'medias' => $items['medias'],
            'folders' => $items['folders'],

        ]);

    }

    //Elimina immagine fisicamente
    public function destroyImage(Request $request)
    {
        if ($request->ajax()) {

            $filename = $request->filename;
            $containerDiv = $request->containerDiv;
            $urls = null;
            $alert = null;

            //Controllo se l'immagine viene usata da qualche parte
            $p = Page::where('image_cover', $filename)->get();
            $a = Article::where('image_cover', $filename)->get();
            $c = Category::where('image_cover', $filename)->get();
            $s = Setting::where('company_logo', $filename)->get();

            if (count($p) > 0 || count($a) > 0 || count($c) > 0 || count($s) > 0):
                //Immagine usata
                $alert = trans('admin.medias.alert_image');
            else:

                //Elimino fisicamente il file
                $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $filename;

                if (file_exists($path)):
                    unlink($path);
                endif;
            endif;

            $items = $this->imageList();

            $viewParams = [
                'delete' => true,
                'medias' => $items['medias'],
                'folders' => $items['folders'],
                'cl' => null,
            ];

            if (!empty($alert)):
                $viewParams['error'] = $alert;
            endif;

            $view = view('admin.medias.images.grid')->with($viewParams)->render();

            $values = [
                'view' => $view,
                'containerDiv' => $containerDiv,
            ];

            return $values;

        };
    }

    //Elimina gallery
    public function destroyGallery(Request $request)
    {
        if ($request->ajax()) {

            $galleryId = $request->id;

            //Elimino immagini e gallery
            Mediaitem::where('mediacollection_id', $galleryId)->delete();
            Mediacollection::destroy($galleryId);


            $galleries = $this->getRelatedRecords();

            $message = trans('admin.medias.gallery_deleted');

            $view = view('admin.medias.galleries.list')->with([
                'galleries' => $galleries,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;

        };
    }

    //Elimina una foto dalla gallery (non elimina fisicamente il file)
    public function destroyGalleryItem(Request $request, $id)
    {
        if ($request->ajax()) {

            // $itemId = $request->itemId;
            $galleryId = $request->galleryId;

            Mediaitem::destroy($id);

            $cl = Mediacollection::findOrFail($galleryId);

            $view = view('admin.medias.galleries.table_items')->with([
                'cl' => $cl,
            ])
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;

        };
    }

    //Seleziona immagine per gallery
    public function selectImage(Request $request)
    {
        if ($request->ajax()) {

            $filename = $request->filename;
            $containerDiv = $request->containerDiv;
            $urls = null;
            $alert = null;

            $items = $this->imageList();

            $view = view('admin.medias.images.list')->with([
                'medias' => $items['medias'],
                'folders' => $items['folders'],
                'alert' => $alert,
            ])->render();

            $values = [
                'view' => $view,
                'containerDiv' => $containerDiv,
            ];

            return $values;

        };
    }

    /*
     ***********************************************************************************************************************************
     * DROPZONE
     ***********************************************************************************************************************************
     */

    //Salvo nuovo file con dropzone
    public function storeFiles(Request $request)
    {

        //$pressSlugName = $request->slug;
        $urls = null;
        $path = null;
        //$loggedUser = Auth::user();

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $allegato = $request->file('file');

                //$filename = $allegato->getClientOriginalName();
                MyHelper::fileUpload($allegato);

            }
        }

        $items = $this->imageList();

        $view = view('admin.medias.images.grid')->with([
            'delete' => true,
            'medias' => $items['medias'],
            'folders' => $items['folders'],
            'cl' => null,

        ])
            ->render();

        $values = [
            'view' => $view,
        ];

        return $values;

    }

    /*
     ***********************************************************************************************************************************
     * UTILITIES
     ***********************************************************************************************************************************
     */

    //Elenca lista file
    public function imageList()
    {

        $medias = null;
        $folders = null;

        $dir = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
        $files = File::files($dir);
        $dirs = File::directories($dir);

        if (!empty($files)):
            foreach ($files as $path):
                $medias[] = [
                    'dir' => basename($dir),
                    'filename' => basename($path),
                    'extension' => pathinfo($path, PATHINFO_EXTENSION),
                ];
            endforeach;
        endif;

        if (!empty($dirs)):
            foreach ($dirs as $dr):
                //dd(str_replace('../public/', '', $dr));
                $folders[] = str_replace('../public/', '', $dr);
                //$folders[] = $dr;
            endforeach;
        endif;

        return $values = [
            'medias' => $medias,
            'folders' => $folders,
        ];

    }

    //Recupera tutti i record di una specifica tabella da mostrare nella select di creazione/modifica gallery
    public function getRecords(Request $request)
    {

        if ($request->ajax()) {

            $records = null;
            $modelId = $request->modelId;
            //$modelName = $request->modelName;

            $related = Mediarelatedmodel::find($modelId);

            $modelName = $related->modelname;

            switch ($modelName):
        case 'Article':
            $records = Article::orderBy('title', 'ASC')->where('visible', true)->get();
            break;
        case 'Category':
            $records = Category::orderBy('title', 'ASC')->where('visible', true)->get();
            break;
        case 'Page':
            $records = Page::orderBy('title', 'ASC')->where('visible', true)->get();
            break;

            endswitch;

            $view = view('admin.medias.galleries.related_options')->with([
                'records' => $records,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;

        }
    }

    //Recupera il titolo di una pagina, articolo o categoria collegata ad una gallery
    public function getRelatedRecords()
    {

            $collections = Mediacollection::get();
            $nameSpace = '\\App\Models\\';
            $galleries = null;

            if (!empty($collections)):
                foreach ($collections as $cl):
                    if (!empty($cl->mediarelatedmodel_id)):
                        $model = app($nameSpace . ucfirst($cl->mediarelatedmodel->modelname));
                        $record = $model::find($cl->record_id);
                    endif;
                    $galleries[] = [
                        'gallery' => $cl,
                        'record' => $record,
                    ];
                endforeach;
            endif;

            return $galleries;
    }

    //Apro modale per inserimento immagini
    public function openImagesModal(Request $request)
    {

        if ($request->ajax()) {
            $galleryId = $request->galleryId;
            $items = $this->imageList();

            if (!empty($galleryId)):
                //Gallery già esistente
                $cl = Mediacollection::findOrFail($galleryId);
            else:
                //Nuova gallery
                $cl = null;
            endif;

            $view = view('admin.medias.images.grid')->with([
                'select' => true,
                'medias' => $items['medias'],
                'folders' => $items['folders'],
                'cl' => $cl,

            ])
                ->render();

            $values = [
                'content' => $view,
            ];

            return $values;
        };
    }


    //Aggiorno contenuto modale quando si cambia folder
    public function navigateFoler(Request $request)
    {



        //if ($request->ajax()) {
            $folder = $request->folder;
            //$items = $this->imageList();

            //dd(public_path($folder));

            //$dir = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
            //$dir = Storage::disk('public');//->files($folder);
            //$dir = Storage::disk($folder);
$dir = public_path($folder);
            dd(File::files($dir));
            //$files = File::files($dir);

            $dirs = File::directories($dir);

            if (!empty($files)):
                foreach ($files as $path):
                    $medias[] = [
                        'dir' => basename($dir),
                        'filename' => basename($path),
                        'extension' => pathinfo($path, PATHINFO_EXTENSION),
                    ];
                endforeach;
            endif;
/*
            if (!empty($dirs)):
                foreach ($dirs as $dr):
                    $folders[] = $dr;
                endforeach;
            endif;

            $view = view('admin.medias.images.grid')->with([
                'select' => true,
                'medias' => $items['medias'],
                'folders' => $items['folders'],
                'cl' => $cl,

            ])
                ->render();

            $values = [
                'content' => $view,
            ];

            return $values;
            */
        //};
    }


    //Salvo immagini selezionate nella modale e ricarico lista
    public function addImages(Request $request)
    {

        if ($request->ajax()) {
            $galleryId = $request->galleryId;
            $fields = $request->fields;

            //dd($fields);

            $fieldImage = null;
            $images = null;

            foreach ($fields as $key => $field):
                if ($field['name'] == 'images[]'):
                    $fieldImage[] = $field['value'];
                endif;
            endforeach;

            //Rimuovo le immagini nulle
            foreach ($fieldImage as $img):
                if (!empty($img)):
                    $images[] = [
                        'path' => $img,
                    ];
                endif;

            endforeach;

            //unset($fields['images']);

            //dd($fields);

            if (!empty($images)):
                if(empty($galleryId)):
                    $gallery = Mediacollection::create($fields);
                else:
                    $gallery = Mediacollection::find($galleryId);
                endif;

                foreach ($images as $image):
                    $image['mediacollection_id'] = $gallery->id;
                    //dd($image);
                    $items = Mediaitem::create($image);
                endforeach;
            endif;

            $cl = Mediacollection::findOrFail($galleryId);
            $enumsLinktype = MyHelper::getEnumValues('mediaitems', 'link_type');
            $enumsLinktarget = MyHelper::getEnumValues('mediaitems', 'target');

            $view = view('admin.medias.galleries.table_items')->with([
                'cl' => $cl,
                'enumsLinktype' => $enumsLinktype,
                'enumsLinktarget' => $enumsLinktarget,
            ])
                ->render();

            $values = [
                'view' => $view,
                'images' => $images,
            ];

            return $values;
        }
    }

}
