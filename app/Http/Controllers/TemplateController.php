<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Template;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    private $routeList = 'templates.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = Template::orderBy('id', 'ASC')->get();

        return view('admin.templates.index', [
            'templates' => $templates,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $template = new Template;

        return view('admin.templates.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'template.store', //Salvataggio form
            'template' => $template,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        /*
        $active = isset($fields['active']) ? 1 : 0;
        $fields['active'] = $active;
        */
        $response = Template::create($fields);

        $message = trans('admin.templates.created', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Template::findOrFail($id);

        return view('admin.templates.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'template.update', //Salvataggio form
            'template' => $template,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();
        /*
        $active = isset($fields['active']) ? 1 : 0;
        $fields['active'] = $active;
        */
        $template = Template::findOrFail($id);
        $template->update($fields);

        $message = trans('admin.templates.updated', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $template = Template::destroy($id);

            $templates = Template::orderBy('id', 'ASC')->get();

            $message = trans('admin.templates.deleted');

            $view = view('admin.templates.list')->with([
                'templates' => $templates,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    

}
