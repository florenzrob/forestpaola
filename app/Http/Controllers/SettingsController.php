<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;





class SettingsController extends Controller
{
    private $routeList = 'settings.edit';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        /*
        $active = isset($fields['active']) ? 1 : 0;
        $fields['active'] = $active;
         */
        $response = Setting::create($fields);

        $message = trans('admin.settings.updated');

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settings = Setting::first();

        if (empty($settings)):
            $settings = new Setting;
        endif;

        return view('admin.settings.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'settings.update', //Salvataggio form
            'settings' => $settings,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $fields = $request->all();

        //Se presente logo, prendo solo nome del file
        if (!empty($fields['company_logo'])):
            //dd(basename($fields['company_logo']));
            $fields['company_logo'] = basename($fields['company_logo']);
        endif;

        //dd($fields);
        /*
        $active = isset($fields['active']) ? 1 : 0;
        $fields['active'] = $active;
         */
        $settings = Setting::first();

        if (empty($settings)):
            $settings = new Setting;
        endif;

        $settings->update($fields);

        $message = trans('admin.settings.updated');

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /* Per viewComposer */
    public function getSettings()
    {
        $settings = Setting::first();

        return $settings;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteLogo(Request $request)
    {
        if ($request->ajax()) {

            $id = $request->id;
            $filename = $request->filename;
            $containerDiv = $request->containerDiv;
            $inputId = $request->inputId;

            //Elimino fisicamente il file
            //$path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $filename;
            /*
            if (file_exists($path)) {
            unlink($path);
            }

            $dir = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
             */
            $settings = Setting::findOrFail($id);
            $settings->company_logo = null;
            $settings->update();

            $replaceHtml = view('admin.medias.image_holder', [
                'btnId' => 'logo_holder_btn',
                'imgId' => 'logo_holder',
            ])
                ->render();

            $values = [
                'divtoreload' => $containerDiv,
                'inputId' => $inputId,
                'replaceHtml' => $replaceHtml,
            ];

            return $values;

        };
    }
}
