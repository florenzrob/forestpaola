<?php

namespace App\Http\Controllers;

use App\Helpers\MyHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $routeList = 'users.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name', 'ASC')->orderBy('surname', 'ASC')->get();

        return view('admin.users.index', [
            'users' => $users,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;

        $enums = MyHelper::getEnumValues('users', 'gender');

        return view('admin.users.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'user.store', //Salvataggio form
            'user' => $user,
            'enums' => $enums,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        $isAdmin = isset($fields['is_admin']) ? 1 : 0;
        $active = isset($fields['active']) ? 1 : 0;

        $fields['is_admin'] = $isAdmin;
        $fields['active'] = $active;

        $pwd = $fields['password'];
        $pwd_confirm = $fields['password_confirmation'];

        if ($pwd_confirm = $pwd):
            unset($fields['password_confirmation']);

            $response = User::create($fields);

            $message = trans('admin.users.created', ['name' => $fields['name'] . ' ' . $fields['surname']]);
        endif;

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $enums = MyHelper::getEnumValues('users', 'gender');

        return view('admin.users.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'user.update', //Salvataggio form
            'user' => $user,
            'enums' => $enums,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        $isAdmin = isset($fields['is_admin']) ? 1 : 0;
        $active = isset($fields['active']) ? 1 : 0;

        $fields['is_admin'] = $isAdmin;
        $fields['active'] = $active;

        $user = User::findOrFail($id);
        $user->update($fields);

        $message = trans('admin.users.updated', ['name' => $fields['name'] . ' ' . $fields['surname']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $user = User::destroy($id);

            $users = User::orderBy('name', 'ASC')->orderBy('surname', 'ASC')->get();

            $message = trans('admin.users.deleted');

            $view = view('admin.users.list')->with([
                'users' => $users,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update is_admin status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateIsAdmin(Request $request, $id)
    {

        if ($request->ajax()) {

            $isAdmin = $request->is_admin;
            $fields['is_admin'] = $isAdmin;

            $user = User::findOrFail($id);
            $user->update($fields);

            $users = User::orderBy('name', 'ASC')->orderBy('surname', 'ASC')->get();

            $view = view('admin.users.list')->with([
                'users' => $users,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update active status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateActive(Request $request, $id)
    {

        if ($request->ajax()) {

            $active = $request->active;
            $fields['active'] = $active;

            $user = User::findOrFail($id);
            $user->update($fields);

            $users = User::orderBy('name', 'ASC')->orderBy('surname', 'ASC')->get();

            $view = view('admin.users.list')->with([
                'users' => $users,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function deletePicture(Request $request)
    {
        if ($request->ajax()) {

            $id = $request->id;
            $filename = $request->filename;
            $containerDiv = $request->containerDiv;

            //Elimino fisicamente il file
            $path = Storage::disk('userPictures')->getDriver()->getAdapter()->getPathPrefix() . $filename;

            if (file_exists($path)) {
                unlink($path);
            }

            $dir = Storage::disk('userPictures')->getDriver()->getAdapter()->getPathPrefix();

            $user = User::findOrFail($id);
            $user->picture = null;
            $user->update();

            /*
            $view = view('admin.medias.uploader_list')->with([
            'urls' => $urls,
            ])->render();
             */

            $values = [
                //'view' => $view,
                'divtoreload' => $containerDiv,
            ];

            return $values;

        };
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPassword($id)
    {
        $user = User::findOrFail($id);

        return view('auth.passwords.changepassword', [
            'breadcrumb_action' => 'action_edit_password', //Breadcrumb
            'icon_action' => 'action_edit_password', //Breadcrumb
            'routeName' => 'user.update.password', //Salvataggio form
            'user' => $user,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $id)
    {
        $fields = $request->all();

        //dd($fields);

        $pwd = $fields['password'];
        $pwd_confirm = $fields['password_confirmation'];

        if ($pwd_confirm == $pwd):

            $user = User::findOrFail($id);
            $user->password = $fields['password'];
            $user->update();

            $message = trans('admin.users.updated', ['name' => $user->name . ' ' . $user->surname]);
        else:
            $message = trans('admin.users.error_change_password');
        endif;
        return redirect()->route($this->routeList)->with('success', $message);

    }

}
