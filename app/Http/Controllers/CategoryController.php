<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{

    private $routeList = 'categories.list';

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

        return view('admin.categories.index', [
            'categories' => $categories,

        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;
        $categories = Category::get();
        $pages = Page::orderBy('title', 'ASC')->get();


        return view('admin.categories.edit', [
            'breadcrumb_action' => 'action_create', //Breadcrumb
            'routeName' => 'category.store', //Salvataggio form
            'category' => $category,
            'categories' => $categories,
            'pages' => $pages,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->all();

        //Se presente cover, prendo solo nome del file
        if (!empty($fields['image_cover'])):
            //dd(basename($fields['company_logo']));
            $fields['image_cover'] = basename($fields['image_cover']);
        endif;

        //Add/Update pages
        $pages = isset($fields['pages']) ? $fields['pages'] : null;

        //Tolgo pages dall'array dell'update
        if (isset($fields['pages'])):
            unset($fields['pages']);
        endif;

        $visible = isset($fields['visible']) ? 1 : 0;
        $isprivate = isset($fields['is_private']) ? 1 : 0;
        $showsidebardx = isset($fields['show_sidebar_dx']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['title'];
        $slug = $fields['title'];

        $fields['visible'] = $visible;
        $fields['is_private'] = $isprivate;
        $fields['show_sidebar_dx'] = $showsidebardx;
        $fields['slug'] = str_slug($slug, '-');

        /*
        //Image cover
        if ($request->hasFile('image_cover')) {

        if ($request->file('image_cover')->isValid()) {
        $allegato = $request->file('image_cover');

        $upload = MyHelper::fileUpload($allegato);
        $fields['image_cover'] = $upload['filename'];
        }
        }
         */

        $category = Category::create($fields);

        //Last insert id
        $id = $category->id;

        //Aggiorno associazioni
        if (!empty($pages)):
            foreach ($pages as $page):
                $category->pages()->attach($id, [
                    'page_id' => $page,
                ]);
            endforeach;
        endif;

        $message = trans('admin.categories.created', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $categories = Category::where('id', '<>', $id)->orderBy('title', 'ASC')->get();
        $pages = Page::orderBy('title', 'ASC')->get();

        return view('admin.categories.edit', [
            'breadcrumb_action' => 'action_edit', //Breadcrumb
            'routeName' => 'category.update', //Salvataggio form
            'category' => $category,
            'categories' => $categories,
            'pages' => $pages,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = $request->all();

        //Se presente cover, prendo solo nome del file
        if (!empty($fields['image_cover'])):
            //dd(basename($fields['company_logo']));
            $fields['image_cover'] = basename($fields['image_cover']);
        endif;

        //Add/Update pages
        $pages = isset($fields['pages']) ? $fields['pages'] : null;

        //Tolgo pages dall'array dell'update
        if (isset($fields['pages'])):
            unset($fields['pages']);
        endif;

        $visible = isset($fields['visible']) ? 1 : 0;
        $isprivate = isset($fields['is_private']) ? 1 : 0;
        $showsidebardx = isset($fields['show_sidebar_dx']) ? 1 : 0;
        //$slug = isset($fields['slug']) ? $fields['slug'] : $fields['title'];
        $slug = $fields['title'];

        //Cerco il max value del sort
        $maxValue = Article::max('sort');
        $maxValue = $maxValue < 1 ? 1 : $maxValue+1;

        $sort = isset($fields['sort']) ? $fields['sort'] : $maxValue;

        $fields['visible'] = $visible;
        $fields['is_private'] = $isprivate;
        $fields['show_sidebar_dx'] = $showsidebardx;
        $fields['slug'] = str_slug($slug, '-');
        $fields['sort'] = $sort;

        /*
        //Image cover
        if ($request->hasFile('image_cover')) {
        if ($request->file('image_cover')->isValid()) {
        $allegato = $request->file('image_cover');

        $upload = MyHelper::fileUpload($allegato);
        $fields['image_cover'] = $upload['filename'];
        }
        }
         */

        $category = Category::findOrFail($id);
        $category->update($fields);

        //Rimuovo tutte associazioni category-page
        $category->pages()->where('id', $id)->wherePivot('category_id', $id)->detach();

        //Aggiorno associazioni
        if (!empty($pages)):
            foreach ($pages as $page):
                $category->pages()->attach($id, [
                    'page_id' => $page,
                ]);
            endforeach;
        endif;

        $message = trans('admin.categories.updated', ['name' => $fields['title']]);

        return redirect()->route($this->routeList)->with('success', $message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            //Imposto il parent di tutte le categorie figlie a null
            $categories = Category::with('parent')->where('parent_id', $id)->get();

            if (!empty($categories)):
                foreach ($categories as $category):
                    $c = Category::findOrFail($category->id);
                    $c->parent_id = null;
                    $c->update();
                endforeach;
            endif;

            //Imposto il category_id di tutti gli articoli collegati a null
            $articles = Article::where('category_id', $id)->get();

            if (!empty($articles)):
                foreach ($articles as $article):
                    $a = Article::findOrFail($article->id);
                    $a->category_id = null;
                    $a->update();
                endforeach;
            endif;

            //Elimino fisicamente l'eventuale cover
            if (!$category->image_cover == null):
                $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $category->image_cover;

                if (file_exists($path)) {
                    unlink($path);
                }
            endif;

            //Elimino categoria
            $category = Category::destroy($id);

            //Rigenero sort
            $categories = Category::with('parent')->orderBy('sort', 'ASC')->orderBy('parent_id', 'ASC')->get();

            if(!empty($categories)):
                $loop = 1;
                foreach($categories as $category):
                    $category->update(['sort' => $loop]);
                    $loop++;
                endforeach;
            endif;


            $message = trans('admin.categories.deleted');

            $view = view('admin.categories.list')->with([
                'categories' => $categories,
            ])
                ->with('success', $message)
                ->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /*
     ***********************************************************************************************************************************
     *
     * CUSTOM
     *
     ***********************************************************************************************************************************
     */

    /**
     * Update visible status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateVisible(Request $request, $id)
    {

        if ($request->ajax()) {

            $visible = $request->is_visible;
            $fields['visible'] = $visible;

            $category = Category::findOrFail($id);
            $category->update($fields);

            $categories = Category::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.categories.list')->with([
                'categories' => $categories,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update is_private status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateIsPrivate(Request $request, $id)
    {

        if ($request->ajax()) {

            $isprivate = $request->is_private;
            $fields['is_private'] = $isprivate;

            $category = Category::findOrFail($id);
            $category->update($fields);

            $categories = Category::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.categories.list')->with([
                'categories' => $categories,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }

    /**
     * Update show_sidebar_dx status
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateShowSidebarDx(Request $request, $id)
    {

        if ($request->ajax()) {

            $showsidebar = $request->show_sidebar_dx;
            $fields['show_sidebar_dx'] = $showsidebar;

            $category = Category::findOrFail($id);
            $category->update($fields);

            $categories = Category::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            $view = view('admin.categories.list')->with([
                'categories' => $categories,
            ])->render();

            $values = [
                'view' => $view,
            ];

            return $values;
        };
    }


    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteCover(Request $request)
    {
        if ($request->ajax()) {

            $id = $request->id;
            $filename = $request->filename;
            $containerDiv = $request->containerDiv;
            $inputId = $request->inputId;

            /*
            //Elimino fisicamente il file
            $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . $filename;

            if (file_exists($path)) {
            unlink($path);
            }

            $dir = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix();
             */

            $category = Category::findOrFail($id);
            $category->image_cover = null;
            $category->update();

            $replaceHtml = view('admin.medias.image_holder', [
                'btnId' => 'image_holder_btn',
                'imgId' => 'image_holder',
            ])
                ->render();

            $values = [
                'divtoreload' => $containerDiv,
                'inputId' => $inputId,
                'replaceHtml' => $replaceHtml,
            ];

            return $values;

        };
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function reorder(Request $request)
    {
        if ($request->ajax()) {

            $items = $request->items;
            $values = null;

            if (!empty($items)):
                foreach ($items as $item):
                    $values[$item['itemid']] = str_replace('#', '', $item['newPosition']);
                endforeach;
            endif;

            $categories = Category::with('parent')->orderBy('parent_id', 'ASC')->orderBy('sort', 'ASC')->get();

            if (!empty($categories)):
                foreach ($categories as $category):
                    foreach ($values as $key => $value):
                        if ($category->id == $key):
                            $category->update(['sort' => $value]);
                        endif;
                    endforeach;
                endforeach;
            endif;

            $view = view('admin.categories.list')->with([
                'categories' => $categories,
            ])->render();

            $values = [
                'view' => $view,
                'values' => $values,

            ];

            return $values;

        };
    }

}
