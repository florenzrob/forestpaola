<?php
namespace App\Http\ViewComposers;

use App\Http\Controllers\NavigationController;
use Illuminate\View\View;

class NavigationComposer
{

    private $navigation;

    public function __construct(NavigationController $navigation)
    {
        // Dependencies automatically resolved by service container...
        $this->navigation = $navigation;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $pages = $this->navigation->getNavigation(1);
        $footerPages = $this->navigation->getNavigation(2);

        //Navigation
        $view->with([
            'pages' => $pages,
            'footerPages' => $footerPages,
        ]);

    }
}
