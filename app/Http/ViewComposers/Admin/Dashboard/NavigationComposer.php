<?php
namespace App\Http\ViewComposers\Admin\Dashboard;

use Illuminate\View\View;

use App\Http\Controllers\NavigationController;

class NavigationComposer
{

    private $navigation;


    public function __construct(NavigationController $navigation)
    {
        // Dependencies automatically resolved by service container...
        $this->navigation = $navigation;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $adminNavigation = $this->navigation->getAdminNavigation();

        $view->with([
            'adminNavigation' => $adminNavigation,
        ]);
    }
}