<?php
namespace App\Http\ViewComposers;

use App\Http\Controllers\PositionController;
use Illuminate\View\View;

class PositionComposer
{

    private $position;

    public function __construct(PositionController $position)
    {
        // Dependencies automatically resolved by service container...
        $this->position = $position;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //Recupero le categorie in evidenza
        $rightContent = $this->position->getPositionContent('sidebar_right');

        $view->with([
            'contents' => $rightContent,
            'positionName' => 'sidebar_right',
        ]);

    }
}
