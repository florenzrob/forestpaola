<?php
namespace App\Http\ViewComposers;

use App\Http\Controllers\ArticleController;
use Illuminate\View\View;

class ArticleComposer
{

    private $article;

    public function __construct(ArticleController $article)
    {
        // Dependencies automatically resolved by service container...
        $this->article = $article;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //Recupero le categorie in evidenza
        $highlights = $this->article->getHiglights();

        $view->with([
            'highlights' => $highlights,
        ]);

    }
}
