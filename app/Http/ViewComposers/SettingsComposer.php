<?php
namespace App\Http\ViewComposers;

use App\Http\Controllers\SettingsController;
use Illuminate\View\View;

class SettingsComposer
{

    private $settings;

    public function __construct(SettingsController $settings)
    {
        // Dependencies automatically resolved by service container...
        $this->settings = $settings;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $settings = $this->settings->getSettings();

        $view->with([
            'settings' => $settings,
        ]);

    }
}
