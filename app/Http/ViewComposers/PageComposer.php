<?php
namespace App\Http\ViewComposers;

use App\Http\Controllers\PageController;
use Illuminate\View\View;

class PageComposer
{

    private $page;

    public function __construct(PageController $page)
    {
        // Dependencies automatically resolved by service container...
        $this->page = $page;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //Recupero le categorie in evidenza
        $highlights = $this->page->getHiglights();

        $view->with([
            'homeHighlights' => $highlights,
        ]);

    }
}
