<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        /*
         ***********************************************
         * Settings
         ***********************************************
         */
        $settingsComposer = 'App\Http\ViewComposers\SettingsComposer';

        View::composer([
            /*
            'includes.preheader',
            'includes.footer',
            */
            '*',
        ], $settingsComposer);


        /*
         ***********************************************
         * Navigation
         ***********************************************
         */
        $navigationComposer = 'App\Http\ViewComposers\NavigationComposer';

        View::composer([
            'includes.navigation.menu',
            'includes.navigation.footer-menu',
        ], $navigationComposer);

        /*
         ******************************************************************************************************************************************************
         *
         * Admin Navigation
         *
         ******************************************************************************************************************************************************
         */

        $adminNavigationComposer = 'App\Http\ViewComposers\Admin\Dashboard\NavigationComposer';

        //Dashboard
        View::composer(
            [
                'admin.dashboard',
            ],
            $adminNavigationComposer);

        /*
         ***********************************************
         * Pages
         ***********************************************
         */
        $pageComposer = 'App\Http\ViewComposers\PageComposer';

        View::composer([
            'blocks.pages.highlights',
        ], $pageComposer);

        /*
         ***********************************************
         * Articles
         ***********************************************
         */
        $articleComposer = 'App\Http\ViewComposers\ArticleComposer';

        View::composer([
            'blocks.articles.highlights',
        ], $articleComposer);

        /*
         ***********************************************
         * Positions
         ***********************************************
         */
        $sidebarRight = 'App\Http\ViewComposers\PositionComposer';

        View::composer([
            'blocks.articles.sidebar_right',
        ], $sidebarRight);

    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
