<?php

namespace App\Helpers;

use App\Models\Page;
use Carbon\Carbon;
use DB;
use Session;

class MyHelper
{

    /*
     ***********************************************************************************************************************************
     *
     * UTILITIES
     *
     ***********************************************************************************************************************************
     */

    public static function createBreadcrumb($query)
    {
        $breadcrumb = null;
        $currentPage = null;

        $items = explode('/', $query);
        foreach ($items as $item):
            $page = Page::with(['categories' => function ($q) {
                $q->where('visible', true)
                    ->with(['articles' => function ($q1) {
                        $q1->where('visible', true)
                            ->whereDate('publish_from', '<=', Carbon::today()->format('Y-m-d'))
                            ->whereDate('publish_to', '>=', Carbon::today()->format('Y-m-d'))
                            ->orWhere('publish_to', null)
                            ->orderBy('parent_id', 'ASC')
                            ->orderBy('category_id', 'ASC')
                            ->orderBy('sort', 'ASC');
                    }]);
            }])

                ->where('visible', true)->where('slug', $item)->first();

            if (!empty($page)):
                $breadcrumb[] = (object) [
                    'id' => $page->id,
                    'slug' => $page->slug,
                    'title' => $page->title,
                ];

                $currentPage = $page;
            endif;

        endforeach;

        return $values[] = [
            'breadcrumb' => $breadcrumb,
            'currentPage' => $currentPage,
        ];
    }

    public static function getChildren($menu = null, $parentSlug = null, $parentID = null)
    {

        $submenu = null;
        $children = null;
        $route = null;
        $elements = Page::where('visible', true)->where('parent_id', $parentID)->orderby('sort', 'ASC')->get();

        if (count($elements) > 0):
            foreach ($elements as $element):
                //echo 'id: ' . $element->id . ' - parent:' . $element->parent_id. ' - title:' . $element->title . ' - babbo: ' . $parentTitle . '<br>';

                $route = $parentSlug != '' ? $parentSlug . '/' . $element->slug : $element->slug;

                if ($element->parent_id != null):
                    $children = self::getChildren($submenu, $route, $element->id);
                endif;

                $submenu[$element->id] = [
                    'item' => $element,
                    'route' => $route,
                    'children' => $children,
                ];
            endforeach;
        endif;

        return $submenu;
    }

    public static function getMesi()
    {
        $mesi = [
            1 => 'Gennaio',
            2 => 'Febbraio',
            3 => 'Marzo',
            4 => 'Aprile',
            5 => 'Maggio',
            6 => 'Giugno',
            7 => 'Luglio',
            8 => 'Agosto',
            9 => 'Settembre',
            10 => 'Ottobre',
            11 => 'Novembre',
            12 => 'Dicembre',
        ];

        return $mesi;

    }

    /**
    //Recupero le opzioni di un campo enum
    Params:
    - $table: nome tabella da controllare
    - $column: nome del campo enum
     */
    public static function getEnumValues($table, $column)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach (explode(',', $matches[1]) as $value) {
            $v = trim($value, "'");
            $enum = array_add($enum, $v, $v);
        }

        return $enum;
    }

/*
 ***********************************************************************************************************************************
 *
 * DATE/ORE
 *
 ***********************************************************************************************************************************
 */
    //Cambio formato di una data
    public static function formatDate($field, $format = 'dd/mm/yyyy')
    {
        switch ($format):
    case 'dd/mm/yyyy':
        $y = substr($field, 6, 4);
        $m = substr($field, 3, 2);
        $d = substr($field, 0, 2);
        $date = $y . '-' . $m . '-' . $d;
        break;

    case 'H:i':
        $y = substr($field, 6, 4);
        $m = substr($field, 3, 2);
        $d = substr($field, 0, 2);
        $date = $field . ':00';
        break;

        endswitch;

        return $date;
    }

    //Ritorna alla home se la session scade
    public static function backToHome()
    {
            Session::flush();
            //return Auth::logout();
    }

    /*
     ***********************************************************************************************************************************
     *
     * FILES
     *
     ***********************************************************************************************************************************
     */
    /**
     * Params:
     * $file: istanza del file da caricare
     * $folder: cartella specifica dove salvare il file
     * $rename: se true, aggiunge un prefisso al nome dell'immagine
     */
    public static function fileUpload($file, $folder = null, $rename = true)
    {

        $folder = $folder != null ? $folder : env('UPLOADS_URL'); //'images/uploads';

        //$imageName = $file->getClientOriginalName(); //Filename con estensione
        //$imageExtension = $file->getClientOriginalExtension(); //Lascia uno spazio prima del .

        $imageName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME); //Filename senza estensione
        $imageExtension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);


        

        $imageName = $rename == true ? time() . '_' . str_slug($imageName, '-') : str_slug($imageName, '-');

        $strFileName = $imageName . '.' . $imageExtension;

        //dd($imageExtension);
        if(env('APP_ENV') == 'local'):
            $upload_success = $file->move(public_path($folder), $strFileName);
        else:
            $upload_success = $file->move($folder, $strFileName);
        endif;

        //dd($upload_success);

        if ($upload_success) {
            $values = [
                'success' => $upload_success,
                'status' => 200,
                'filename' => $strFileName,
            ];

        } else {
            $values = [
                'success' => 'error',
                'status' => 400,
                'filename' => $strFileName,
            ];
        }

        return $values;
    }

}
