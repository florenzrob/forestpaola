<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SendNewsletter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $newsletterDetails;

    /**
     * @var Mailable
     */
    protected $mail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($newsletterDetails)
    {
        $this->newsletterDetails = $newsletterDetails;
        //$this->mail = $mail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Mail::send('emails.test',
        array(
            'msg' => 'ciao',
        ), function ($message) {

            $message->from('io@io.it', 'io');
            $message->bcc('roberto@proprionoi.it');
            $message->subject('parto');

        });

        //$email = new EmailForQueuing();

        //$newsletterDetails = $this->newsletterDetails;

        $fields = $this->newsletterDetails['fields'];
        $fromAddress = $this->newsletterDetails['fromAddress'];
        $fromName = $this->newsletterDetails['fromName'];
        $subject = $this->newsletterDetails['subject'];
        $recipients = $this->newsletterDetails['recipients'];
        $mailinglist = $this->newsletterDetails['mailinglist'];
        //$attachment = $this->newsletterDetails['attachment'];

        //dd($recipients);

        if (!empty($recipients)):

            //Single
            foreach ($recipients as $recipient):

                Mail::send('emails.newsletter',
                    array(
                        'fields' => $fields,
                    ), function ($message) use ($fromAddress, $fromName, $subject, $recipient, $mailinglist) {

                        $message
                            ->from($fromAddress, $fromName)
                            ->to($recipient)
                            ->subject($subject);

                        if (!empty($mailinglist->attachment)):

                            $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . 'files/' . $mailinglist->attachment;

                            $message->attach($path);

                        endif;
                    });
            endforeach;

        else:
            Mail::send('emails.test',
                array(
                    'msg' => 'nessun destinatario',
                ), function ($message) use ($fromAddress, $fromName) {

                    $message->from($fromAddress, $fromName);
                    $message->bcc('roberto@proprionoi.it');
                    $message->subject('errore job: ' . Carbon::now()->format('d/m/Y H:i:s'));

                });

            /*
        //BULK
        Mail::send('emails.newsletter',
        array(
        'fields' => $fields,
        ), function ($message) use ($fromAddress, $fromName, $subject, $recipients, $mailinglist) {

        $message
        ->from($fromAddress, $fromName)
        ->bcc($recipients)
        ->subject($subject);

        if (!empty($mailinglist->attachment)):

        $path = Storage::disk('uploads')->getDriver()->getAdapter()->getPathPrefix() . 'files/' . $mailinglist->attachment;

        //dd($path);

        $message->attach($path);

        endif;
        });
         */
        endif;
    }
}
