<?php

return [

    /*
    |--------------------------------------------------------------------------
    | GDPR Cookie
    |--------------------------------------------------------------------------
    */
    'title'         => 'This website is using cookies',
    'text'          => 'We use cookies to ensure that we give you the best experience on our website. If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on this website.',
    'continue'      => 'Continue',
    'details'       => 'Learn&nbsp;more',

];
