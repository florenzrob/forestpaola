<?php

return [

    /*
    |--------------------------------------------------------------------------
    | GDPR Cookie
    |--------------------------------------------------------------------------
    */
    'title'         => 'Questo sito fa uso di cookies',
    'text'          => 'Per offrirti una migliore esperienza di navigazione online questo sito web usa dei cookie. Continuando a navigare sul sito acconsenti all\'utilizzo dei cookie. Scopri di più sull\'uso dei cookie e sulla possibilità di modificarne le impostazioni o negare il consenso.',
    'continue'      => 'OK',
    'details'       => 'Dettagli',

];
