<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin
    |--------------------------------------------------------------------------
    */
    //Breadcrumb
    'dashboard'                 => 'Dashboard',
    'dashboard_title'           => 'Pannello di controllo',
    'name'                      => 'Nome',
    'action_create'             => 'Nuovo',
    'action_edit'               => 'Modifica',
    'action_edit_password'      => 'Modifica password',

    //Common
    'menu'                      => 'Menu',
    'edit'                      => 'Modifica',
    'delete'                    => 'Elimina',
    'show'                      => 'Mostra',
    'undo'                      => 'Cancella modifiche',
    'close'                     => 'Chiudi',
    'send'                      => 'Invia',
    'yes'                       => 'Si',
    'no'                        => 'No',
    'exit'                      => 'Esci',
    'save'                      => 'Salva',
    'date_from'                 => 'Dal',
    'date_to'                   => 'Al',
    'time_start'                => 'Dalle ore',
    'time_end'                  => 'Alle ore',
    'time_start_sm'             => 'Dalle',
    'time_end_sm'               => 'Alle',
    'before'                    => 'Prima',
    'after'                     => 'Dopo',
    'parent'                    => 'Parent',
    'title'                     => 'Titolo',
    'subtitle'                  => 'Sottotitolo',
    'abstract'                  => 'Abstract',
    'content'                   => 'Contenuto',
    'slug'                      => 'Slug',
    'all'                       => 'Tutti',
    'week_from'                 => 'Settimana dal',
    'date_input'                => 'gg/mm/aaaa',
    'time_placeholder'          => '00:00',
    'date_time_placeholder'     => 'gg/mm/aaaa - 00:00',
    'name'                      => 'Nome',
    'surname'                   => 'Cognome',
    'username'                  => 'Username',
    'address'                   => 'Indirizzo',
    'address_number'            => 'Civico',
    'postal_code'               => 'CAP',
    'city'                      => 'Città',
    'province'                  => 'Provincia',
    'nation'                    => 'Nazione',
    'nationality'               => 'Nazionalità',
    'cf'                        => 'Codice Fiscale',
    'vat'                       => 'P.IVA',
    'telephone1'                => 'Telefono',
    'telephone2'                => 'Altro telefono',
    'email1'                    => 'Email',
    'email2'                    => 'Altra email',
    'mobile1'                   => 'Cellulare',
    'mobile2'                   => 'Altro cellulare',
    'fax'                       => 'Fax',
    'approved'                  => 'Approvato',
    'default'                   => 'Predefinito',
    'color'                     => 'Colore',
    'id'                        => 'ID',
    'edit_pass'                 => 'Modifica Password',
    'edit_image'                => 'Modifica immagine del profilo',
    'state'                     => 'Stato',
    'text_copied'               => 'Testo copiato',
    'business_name'             => 'Ragione Sociale',
    'slogan'                    => 'Slogan',
    'logo'                      => 'Logo',
    'logo_height'               => 'Altezza logo',
    'data'                      => 'Data',
    'hour'                      => 'Ora',
    'timetable'                 => 'Orario',
    'choose'                    => 'Scegli',
    'sort'                      => 'Ordinamento',
    'link'                      => 'Link',
    'target_link'               => 'Target',
    'add'                       => 'Aggiungi',
    'cancel'                       => 'Annulla',

    //Form
    'active'                    => 'Attivo',
    'is_private'                => 'Privato',
    'is_home'                   => 'Homepage',
    'on_home'                   => 'Vedi in Home',
    'show_sidebar_dx'           => 'Mostra sidebar destra',
    'visible'                   => 'Visibile',
    'description'               => 'Descrizione',
    'icon'                      => 'Icona',
    'actions'                   => 'Azioni',
    'image_cover'               => 'Immagine di copertina',
    'cover_height'              => 'Altezza immagine',
    'clear_file'                => 'Rimuovi file selezionato',
    'select'                    => 'Seleziona',
    'image'                     => 'Immagine',
    'video_link'                => 'Link video',


    //Uploader
    'uploader' => [
        'delete_title'          => 'Elimina file',
        'delete_message'        => 'Vuoi davvero eliminare questo file?',
        'no_file_uploaded'      => 'Nessun file caricato',
        'dnd'                   => 'Trascina i files qui',
        'choose_file'           => 'Scegli un file',
        'choose_logo'           => 'Carica un logo',
        'click_to_add'          => 'Clicca per aggiungere i files',
    ],

    //User
    'users'  =>[
        'benvenuto'             => 'Benvenuto',
        'benvenuta'             => 'Benvenuta',
        'title'                 => 'Gestione Utenti',
        'list'                  => 'Elenco utenti',
        'name'                  => 'Nome',
        'surname'               => 'Cognome',
        'fullname'              => 'Nome e Cognome',
        'username'              => 'Username',
        'gender'                => 'Sesso',
        'address'               => 'Indirizzo',
        'cap'                   => 'CAP',
        'city'                  => 'Città',
        'province'              => 'Provincia',
        'nation'                => 'Nazione',
        'nationality'           => 'Nazionalità',
        'cf'                    => 'Codice Fiscale',
        'piva'                  => 'P.IVA',
        'phone'                 => 'Telefono',
        'phone2'                => 'Altro telefono',
        'email'                 => 'Email',
        'email2'                => 'Altra email',
        'mobile'                => 'Cellulare',
        'mobile2'               => 'Altro cellulare',
        'pec'                   => 'PEC',
        'note'                  => 'Note',
        'is_admin'              => 'Admin',
        'image_cover'           => 'Immagine di profilo',
        'edit_password'         => 'Cambia password',
        'created'               => 'Utente :name creato con successo',
        'updated'               => 'Utente :name aggiornato con successo',
        'deleted'               => 'Utente eliminato con successo',
        'delete_title'          => 'Elimina Utente',
        'delete_message'        => 'Vuoi davvero eliminare l\'utente :name?',

    ],

    //Navigations
    'navigations' => [
        'settings'              => 'Impostazioni',
        'layouts'               => 'Gestione layout',
        'contents'              => 'Gestione contenuti',
        'profile'               => 'Gestione Profilo',
        'medias'                => 'Gestione Media',
        'mailinglist'           => 'Gestione Mailing list',
    ],

    //Settings
    'settings' => [
        'title'                 => 'Settings',
        'updated'               => 'Impostazioni aggiornate con successo',

    ],

    //Templates
    'templates' => [
        'title'                 => 'Gestione Templates',
        'template'              => 'Template',
        'templates'             => 'Templates',
        'list'                  => 'Elenco templates',
        'name'                  => 'Nome del template',
        'created'               => 'Template :name creato con successo',
        'updated'               => 'Template :name aggiornato con successo',
        'deleted'               => 'Template eliminato con successo',
        'delete_title'          => 'Elimina template',
        'delete_message'        => 'Vuoi davvero eliminare il template :name?',
    ],

    //Positions
    'positions' => [
        'title'                 => 'Gestione Posizioni',
        'position'              => 'Posizione',
        'positions'             => 'Posizioni',
        'list'                  => 'Elenco posizioni',
        'created'               => 'Posizione :name creata con successo',
        'updated'               => 'Posizione :name aggiornata con successo',
        'deleted'               => 'Posizione eliminata con successo',
        'delete_title'          => 'Elimina posizione',
        'delete_message'        => 'Vuoi davvero eliminare la posizione :name?',
    ],

    //Menus
    'menus' => [
        'title'                 => 'Gestione Menu',
        'menu'                  => 'Menus',
        'menus'                 => 'Menus',
        'list'                  => 'Elenco menu',
        'name'                  => 'Nome del menu',
        'created'               => 'Menu :name creato con successo',
        'updated'               => 'Menu :name aggiornato con successo',
        'deleted'               => 'Menu eliminato con successo',
        'delete_title'          => 'Elimina menu',
        'delete_message'        => 'Vuoi davvero eliminare il menu :name?',
    ],


    //Pages
    'pages' => [
        'title'                 => 'Gestione pagine',
        'page'                  => 'Pagina',
        'pages_menu'            => 'Pagine associate al menu',
        'page_categories'       => 'Categorie associate alla pagina',
        'pages'                 => 'Pagine',
        'list'                  => 'Elenco pagine',
        'created'               => 'Pagina :name creata con successo',
        'updated'               => 'Pagina :name aggiornata con successo',
        'deleted'               => 'Pagina eliminata con successo',
        'delete_title'          => 'Elimina pagina',
        'delete_message'        => 'Vuoi davvero eliminare la pagina :name?<br>Tutte le pagine figlie verrano impostate a parent = null',
        'seo_title'             => 'Titolo SEO',
        'seo_content'           => 'Contenuto SEO',

    ],

    //Categories
    'categories' => [
        'title'                 => 'Gestione categorie articoli',
        'category'              => 'Categoria',
        'categories'            => 'Categorie',
        'page_categories'       => 'Pagine associate alla categoria',
        'list'                  => 'Elenco categorie articoli',
        'created'               => 'Categoria :name creata con successo',
        'updated'               => 'Categoria :name aggiornata con successo',
        'deleted'               => 'Categoria eliminata con successo',
        'delete_title'          => 'Elimina categoria',
        'delete_message'        => 'Vuoi davvero eliminare la categoria :name?<br>Tutte le categorie figlie verrano impostate a parent = null',

    ],

    //Articles
    'articles' => [
        'title'                 => 'Gestione articoli',
        'article'               => 'Articolo',
        'articles'              => 'Articoli',
        'list'                  => 'Elenco articoli',
        'published'             => 'Pubblicato',
        'created'               => 'Articolo :name creato con successo',
        'updated'               => 'Articolo :name aggiornato con successo',
        'deleted'               => 'Articolo eliminato con successo',
        'delete_title'          => 'Elimina articolo',
        'delete_message'        => 'Vuoi davvero eliminare l\'articolo :name?<br>Tutti gli articoli figli verrano impostati a parent = null',
        'related_pages'         => 'Trovi questo articolo in',
    ],

    //Tags
    'tags' => [
        'title'                 => 'Gestione tags',
        'tag'                   => 'Tag',
        'tags'                  => 'Tags',
        'list'                  => 'Elenco tags',
        'system'                => 'Tag di sistema',
        'created'               => 'Tag :name creato con successo',
        'updated'               => 'Tag :name aggiornato con successo',
        'deleted'               => 'Tag eliminato con successo',
        'delete_title'          => 'Elimina tag',
        'delete_message'        => 'Vuoi davvero eliminare il Tag :name?<br>Tutti i tag figli verrano impostati a parent = null',
    ],

    //Mailing list
    'subscribers' => [
        'title'                 => 'Gestione iscritti',
        'subscriber'            => 'Iscritto',
        'subscribers'           => 'Iscritti',
        'list'                  => 'Elenco iscritti',
        'mail_not_found'        => 'Questa mail non è presente tra gli iscritti alla mailing list',
        'newslettere_removed'   => 'Cancellazione dalla newsletter avvenuta',
        'created'               => 'Utente :name inserito con successo',
        'updated'               => 'Utente :name aggiornato con successo',
        'deleted'               => 'Utente eliminato con successo',
        'delete_title'          => 'Elimina iscritto',
        'delete_message'        => 'Vuoi davvero rimuovere :name dalla mailing list?',
    ],
    'mailinglists' => [
        'title'                 => 'Gestione mailing list',
        'mailings'              => 'Mailings',
        'list'                  => 'Elenco mailing list',
        'subject'               => 'Soggetto',
        'attachment'            => 'Allegato',
        'send'                  => 'Invia',
        'recipients'            => 'Destinatari',
        'mailing_send'          => 'Invia mailing list',
        'mailing_confirm_send'  => 'Vuoi inviare questa mailing list?',
        'mailing_confirm_send_alert'  => 'Attenzione!! Hai già inviato questa mail',
        'sent'                  => 'Spedita',
        'sent_date'             => 'Data invio',
        'sent_groups'           => 'Spedita a',
        'created'               => 'Mail creata con successo',
        'updated'               => 'Mail aggiornata con successo',
        'deleted'               => 'Mail eliminata con successo',
        'delete_title'          => 'Elimina mail',
        'delete_message'        => 'Vuoi davvero eliminare la mail :name?',
        'sent_report'           => 'Report invii mailinglists',
    ],
    'mlgroups' => [
        'title'                 => 'Gestione gruppi',
        'groups'                => 'Gruppi',
        'list'                  => 'Elenco gruppi',
        'created'               => 'Gruppo :name creato con successo',
        'updated'               => 'Gruppo :name aggiornato con successo',
        'deleted'               => 'Gruppo eliminato con successo',
        'delete_title'          => 'Elimina tag',
        'delete_message'        => 'Vuoi davvero eliminare il gruppo :name?',

    ],


    //Socials
    'socials' => [
        'fb'                    => 'Facebook',
        'in'                    => 'Instagram',
        'tw'                    => 'Twitter',
        'ln'                    => 'Linkedin',
        'gp'                    => 'Google page',

    ],


    //Medias
    'medias' => [
        'manage_images'         => 'Gestione Immagini',
        'alert_image'           => 'Attenzione, non è possibile eliminare questa immagine perchè è usata per qualche contenuto',
        'manage_galleries'      => 'Gestione Galleries',
        'galleries_list'        => 'Elenco galleries',
        'type'                  => 'Tipologia di gallery',
        'related_model'         => 'Collegato a',
        'related_model_empty'   => 'Nessun record presente in questa tabella',
        'related_model_choose'  => 'Scegli una tabella',
        'related_record'        => 'Contenuto',
        'choose_images'         => 'Aggiungi immagini alla gallery',
        'add_caption'           => 'Inserisci descrizione',
        'gallery_created'       => 'Gallery :name creata con successo',
        'gallery_updated'       => 'Gallery :name aggiornato con successo',
        'delete_gallery'        => 'Elimina gallery',
        'delete_gallery_message'=> 'Vuoi davvero eliminare la gallery?',
        'gallery_deleted'       => 'Gallery eliminata con successo',

        'link_type'             => 'Tipo link',
        'edit_content'          => 'Modifica contenuto',
        'add_images'            => 'Carica nuove immagini',

    ],



];
