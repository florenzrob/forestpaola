<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pages/Articles
    |--------------------------------------------------------------------------
    */
    'read_more' => 'Continua',

    /*
    |--------------------------------------------------------------------------
    | Forms
    |--------------------------------------------------------------------------
    */

    'name' => 'Nome',
    'surname' => 'Cognome',
    'phone' => 'Telefono',
    'email' => 'Email',
    'request' => 'Messaggio',
    'submit' => 'Invia',
    'privacy' => 'Autorizzo il trattamento dei miei dati personali ai sensi del D. Lgs 196/03.',
    'privacy_link' => 'Leggi l\'informativa sulla privacy',

    //Email
     'form_title' => 'Richiesta informazioni',
     'form_subject_customer' => 'Conferma invio richiesta',
     'form_subject_admin' => 'Richiesta informazioni spedita in data',
     'saluto' => 'Ciao',
     'messaggio_risposta' => 'questa è una risposta automatica.<br>Grazie per avermi scritto, ti ricontatterò quanto prima.</p>',
     'message' => 'Messaggio',
     'mail_success' => 'Email inviata correttamente',
     'mail_failure' => 'Attenzione, si è verificato un errore durante l\'invio della richiesta',
     'your_data' => 'I tuoi dati',
     'saluti' => 'Saluti',

     //Newsletter
     'nl_subscribe_success' => 'Iscrizione effettuata. Grazie!',
     'nl_subscribe_error' => 'Attenzione, l\'indirizzo email è già registrato',


    //Dropzone
    'choose_file'          => 'Scegli un file',
    'or'                   => 'o',
    'drop_file'            => 'trascinalo qui',

    'validations' => [
        'name_required'     => 'Inserisci il nome',
        'surname_required'  => 'Inserisci il cognome',
        'mail_required'     => 'Inserisci una email',
        'mail_format'       => 'Formato email non valido',
        'privacy_required'  => 'Devi accettare la privacy',
        'title_required'     => 'Inserisci il titolo',
        'content_required'   => 'Inserisci il contenuto',
        'positon_required'   => 'Inserisci il nome della posizione',
        'subject_required'   => 'Inserisci il soggetto',
        'username_required'  => 'Inserisci l\'username',
        'password_required'  => 'Inserisci la password',
        'password_confirm_required'  => 'Inserisci la password di conferma',


        'min_lenght'         => 'Inserisci almento :number caratteri',
    ],
];
