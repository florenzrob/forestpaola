<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La password deve contenere almeno 6 caratteri e corrispondere alla conferma.',
    'reset' => 'La tua password è stata resettata!',
    'sent' => 'Ti abbiamo spedito una mail con un link per resettare la tua password!',
    'token' => 'Questo token di reimpostazione della password non è valido.',
    'user' => "Nessun utente con questa email  trovato",
    'button' => "Invia il link per reimpostare la password",
    'card-header' => "Resetta la password",
    'email' => "Indirizzo Email"

];
