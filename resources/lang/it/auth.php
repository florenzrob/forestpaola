<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                => 'Credenziali non trovate tra i nostri records.',
    'throttle'              => 'Troppi tentativi di accesso. Prego ritentare di nuovo tra :seconds secondi.',

    'username'              => 'Username',
    'password'              => 'Password',
    'new_password'          => 'Nuova password',
    'confirm_password'      => 'Conferma password',
    'email'                 => 'Email',
    'remember'              => 'Ricordami',
    'login'                 => 'Login',
    'logout'                => 'Logout',
    'forgot_password'       => 'Password dimenticata',

];
