/*
 ***********************************************************************************************************************************
 *
 * Load all js dependencies
 *
 * Eseguire npm update
 *
 * Eseguire yarn add modulo per aggiungere nuovi moduli
 * Eseguire npm run dev ogni volta che si aggiorna questo file IN DEV
 * Eseguire npm run prod ogni volta che si aggiorna questo file IN PROD
 *
 ***********************************************************************************************************************************
 */

import I18n from './vendor/I18n.js';

import formUtilities from '../../public/js/admin/imports/forms.js'; //Utilities form
import utilitiesFunctions from '../../public/js/admin/imports/utilities.js';
import postSuccessFunctions from '../../public/js/admin/imports/postSuccessFunctions.js';
import autocompleteFunctions from '../../public/js/admin/imports/autocomplete.js';
import editorFunctions from '../../public/js/admin/imports/texteditor.js';


window.I18n = I18n;
window.translator = new I18n;



window.ff = new formUtilities();
window.uf = new utilitiesFunctions();
window.pf = new postSuccessFunctions();
window.ac = new autocompleteFunctions();
window.te = new editorFunctions();
