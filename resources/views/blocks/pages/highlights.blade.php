<div class="row mt-3">
    <div class="col site-section-title">
        <h2>Proposte</h2>
    </div>
</div>
<div class="row mb-5">
    @if(!empty($homeHighlights))
        @foreach ($homeHighlights as $page)
            {{--dd($homeHighlights)--}}
            @php($cover = isset($page->image_cover) ? 'uploads/' . $page->image_cover : 'placeholder.png')
            <div class="col-sm-12 col-md-6">
                <div class="mb-4 ">
                    <figure class="snip1477">
                        <img src="{{ asset('images/'. $cover) }}" class="crop img-fluid" alt="{{$page->title}}">
                        <div class="title">
                            <div>
                                <h4>{{$page->title}}</h4>
                            </div>
                        </div>
                        <figcaption>
                            <p>{!!$page->abstract!!}</p>
                        </figcaption>

                        @if($page->parent != null)
                            @include('blocks.pages.highlights_link', ['page' => $page, 'route' => $page->slug])
                        @else
                            <a class="nav-link" href="{{route('page', ['query' => $page->slug])}}"></a>
                        @endif

                    </figure>
                </div>
            </div>
        @endforeach
    @endif
</div>
