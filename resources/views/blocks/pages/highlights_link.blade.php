@if($page->parent->parent != null)
    @php($route = $page->parent->slug . '/' . $route)
    @include('blocks.pages.highlights_link', ['page' => $page->parent, 'route' => $route])
@else
    @php($route = $page->parent->slug . '/' . $route)
    <a class="nav-link" href="{{route('page', ['query' => $route ])}}"></a>
@endif
