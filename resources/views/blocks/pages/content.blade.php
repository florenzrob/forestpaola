{{-- image cover --}}
<div class="row mb-3">
    <div class="col">
        @if(!empty($page->image_cover))
            <figure>
                <img src="{{ asset('images/uploads/'.$page->image_cover) }}"
                    class="img-fluid img-thumbnail rounded float-left mr-5 mb-5 mt-3"
                    style="max-height:{{!empty($page->cover_height) ? $page->cover_height : 300}}px;" alt="{{$page->title}}">
            </figure>
        @endif

        {!!$page->content!!}
    </div>
</div>

{{-- video --}}
@if(!empty($page->video_link))
    @include('blocks.medias.video', ['link' => $page->video_link])
@endif
