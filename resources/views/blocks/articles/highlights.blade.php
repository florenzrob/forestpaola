<div class="row mt-3">
    <div class="col site-section-title">
        <h2>Prossimi appuntamenti</h2>
    </div>
</div>
<div class="row mb-5">
    @if(!empty($highlights))
        @foreach ($highlights as $article)
            @php($cover = isset($article->image_cover) ? 'uploads/' . $article->image_cover : 'placeholder.png')
            <div class="col-sm-12 col-md-6">
                <div class="mb-4 ">
                    <figure class="snip1477">
                        <img src="{{ asset('images/'. $cover) }}" class="crop img-fluid" alt="{{$article->title}}">
                        <div class="title">
                            <div>
                                <h2>{{$article->title}}</h2>
                                <h6>
                                    @if(!empty($article->category))
                                        {{$article->category->title}}
                                    @else
                                        &nbsp;
                                    @endif
                                </h6>
                            </div>
                        </div>
                        <figcaption>
                            <p>{!!$article->abstract!!}</p>
                        </figcaption>

                        <a class="nav-link" href="{{route('article', [/*'query' => $query,*/ 'articleSlug' => $article->slug])}}"></a>
                    </figure>
                </div>
            </div>
        @endforeach
    @endif
</div>
