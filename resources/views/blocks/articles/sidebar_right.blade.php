@if(!empty($contents))
    @foreach ($contents as $rs)
        @php($prefix = $rs->getTable() == 'articles' ? 'article' : 'page')
        <div class="row">
            <div class="col">
                <div class="card bg-white border-white mb-5">
                    <div class="card-body">
                        <h3 class="card-title">{{$rs->title}}</h3>
                        <img src="{{ asset('images/uploads/'.$rs->image_cover) }}" class="img-fluid mb-4" alt="{{$rs->title}}">
                        <p class="text-justify">
                            @if(!empty($rs->abstract))
                                {!!$rs->abstract!!}
                                <a href="{{route($prefix, ['articleSlug' => $rs->slug])}}"><i class="fas fa-share-square mr-2"></i> {{__('messages.read_more')}}</a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif