<div class="container py-5">

    <form id="unsubscribeForm" class="bg-white shadow p-3 mb-5"
        data-route="{{route('mailinglist.remove')}}"
        data-success="{{'updateUnsubscribeForm'}}"
        >
        <div class="row">
            <div class="col">
                <h3 class="text-center">Cancellati dalla mailing list</h3>
                <div class="text-center">Inserisci la tua email</div>
            </div>
        </div>

        {{--Seconda riga --}}
        <div class="row">
            <div class="col">
                <div class="form-group">
                    {!! Form::text('email', null, [
                        'id' => 'email',
                        'class' => 'form-control',
                        'placeholder' => __('messages.email'),

                        ])
                    !!}
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>
            </div>
       </div>

        {{--Terza riga --}}
        <div class="row">
            <div class="col">
                <div class="form-actions text-center my-5">
                    {!! Form::button(__('messages.submit'), [
                        'id' => 'unsubscribeNewsletter',
                        'data-formid' => 'unsubscribeForm',
                        'class' => 'btn bg-green text-uppercase px-5'])
                    !!}
                </div>

                {{-- Alerts --}}
                <div class="text-center alert form-response collapse"></div>

            </div>

        </div>

    {!! Form::close() !!}

</div>
