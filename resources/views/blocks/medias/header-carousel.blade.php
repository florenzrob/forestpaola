{{-- Carousel --}}

<div class="slide-one-item home-slider owl-carousel">
    @for($x=1; $x<5; $x++)
        <div class="site-blocks-cover sparoller" style="background:url({{ asset('images/hero/hero_bg_' . $x . '.jpg') }}) no-repeat center; background-size: cover;">
            {{--
            <div class="text">
                <h2>853 S Lucerne Blvd</h2>
                <p class="location"><span class="property-icon icon-room"></span> Los Angeles, CA 90005</p>
                <p class="mb-2"><strong>$2,250,500</strong></p>
                <p class="mb-0"><a href="#" class="text-uppercase small letter-spacing-1 font-weight-bold">More Details</a></p>
            </div>
            --}}
        </div>
    @endfor
</div>
