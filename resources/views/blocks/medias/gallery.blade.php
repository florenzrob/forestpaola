@switch($media->mediatype->type)
    @case('carousel')
        {{-- Header carousel --}}
        <div class="slide-one-item home-slider owl-carousel">
            @foreach($media->mediaitems as $item)
                <img src="{{asset('images/uploads/'.$item->path)}}" class="img-fluid">
                {{--
                                    <div class="site-blocks-cover sparoller" style="background:url({{asset('images/uploads/'.$item->path)}}) no-repeat center; background-size: cover;">
                </div>

                --}}
            @endforeach
        </div>

    @break

    @case('gallery')
        @php($galleryClass = strtolower($media->mediatype->type) == 'gallery' ? 'owl-gallery' : 'owl-sliding')
        <div class="{{$galleryClass}} owl-carousel owl-theme mb-5">
            @foreach($media->mediaitems as $item)
                <a href="{{asset('images/uploads/'.$item->path)}}" data-lightbox="gallery"><img class="item img-thumbnail" src="{{asset('images/uploads/'.$item->path)}}"  style="height:150px;"></a>
            @endforeach
        </div>

    @break

    @case('banner')
        @foreach($media->mediaitems as $item)
            <div class="p-0 banner" style="background-image:url({{asset('images/uploads/'.$item->path)}});"></div>
        @endforeach
    @break

    @case('video')
        @foreach($media->mediaitems as $item)
            <div class="row mb-3">
                <div class="col">
                    <div class="embed-responsive embed-responsive-16by9 my-5">
                        <iframe class="embed-responsive-item"
                            src="{{$item->path}}"
                            frameborder="0"
                            allow="autoplay; encrypted-media"
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
        @endforeach
    @break

@endswitch


