{{-- video --}}
@if(!empty($link))
    <div class="row mb-3">
        <div class="col">
            <div class="embed-responsive embed-responsive-16by9 my-5">
                <iframe class="embed-responsive-item" 
                    src="{{$link}}" 
                    frameborder="0" 
                    allow="autoplay; encrypted-media" 
                    allowfullscreen>
                </iframe>
            </div>
        </div>
    </div>
@endif