<nav class="navbar fixed-bottom footer text-white">
	<div class="col-10"><a href="{{route('login')}}" target="_blank" class="text-white"><i class="{{config('settings.admin.dashboard.icon')}}"></i></a> - ©{{\Carbon\Carbon::now()->format('Y')}} {{$settings->company_business_name}}</div>

    <div class="col-2 text-right">
		<small>v.n° {{app()::VERSION}}</small>
	</div>
</nav>
