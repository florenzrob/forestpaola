{{-- Facebook --}}
@if(!empty($settings->social_facebook))
    <a href="{{$settings->social_facebook}}" target="_blank" class="mr-3 {{$text_color}}" alt="Facebook"><i class="fab fa-facebook-square"></i></a>                
@endif

{{-- Instagram --}}
@if(!empty($settings->social_instagram))
    <a href="{{$settings->social_instagram}}" target="_blank" class="mr-3 {{$text_color}}" alt="Instagram"><i class="fab fa-instagram"></i></a>                
@endif

{{-- Twitter --}}
@if(!empty($settings->social_twitter))
    <a href="{{$settings->social_twitter}}" target="_blank" class="mr-3 {{$text_color}}" alt="Twitter"><i class="fab fa-twitter-square"></i></a>                
@endif

{{-- Linkedin --}}
@if(!empty($settings->social_linkedin))
    <a href="{{$settings->social_linkedin}}" target="_blank" class="mr-3 {{$text_color}}" alt="Linkedin"><i class="fab fa-linkedin"></i></a>                
@endif

{{-- Google page --}}
@if(!empty($settings->social_googlepage))
    <a href="{{$settings->social_googlepage}}" target="_blank" class="mr-3 {{$text_color}}" alt="Google Plus"><i class="fab fa-google-plus-square"></i></a>                
@endif
