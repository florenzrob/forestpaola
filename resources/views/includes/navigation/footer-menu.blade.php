{{-- Top Navigation --}}
<nav class="navbar navbar-expand-md ">
    <a href="/" class="navbar-brand">ForestPaola</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar">
        <i class="fas fa-bars"></i>
    </button>
    <div class="navbar-collapse collapse justify-content-stretch" id="mainNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown"><a class="nav-link" href="{{route('home')}}"><i class="fas fa-home"></i> Home</a></li>
            @if(!empty($pages))
                @foreach ($pages as $page)
                    <li class="nav-item dropdown">
                        @if($page->children != null)
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">{{$page->item->title}}</a>
                            @include('includes.navigation.submenu', ['subitems' => $page->children])
                        @else
                            <a class="nav-link" href="//codeply.com">{{$page->item->title}}</a>
                        @endif
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</nav>
