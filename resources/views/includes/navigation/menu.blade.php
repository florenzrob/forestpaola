@php($currentRoute = Route::current()->parameters())

@php($query = isset($currentRoute['query']) ? explode('/', $currentRoute['query'])[0] : null)

<ul class="site-menu navbar-nav">
    {{--<li class="nav-item dropdown active"><a class="nav-link" href="{{route('home')}}"><i class="fas fa-home"></i> Home</a></li>--}}
    @if(!empty($pages))
        @foreach ($pages as $page)
            <li class="nav-item navmenu dropdown">
                <span class="hover hover-3">
                @if($page->children != null)
                    <a class="nav-link navigation-menu dropdown-toggle" href="#" data-toggle="dropdown">{{$page->item->title}}</a>
                    @include('includes.navigation.submenu', ['parent' => $page->item->slug, 'subitems' => $page->children])
                @else

                    @php($active = $page->item->slug == $query ? 'active' : null)

                    <a class="nav-link {{$active}}" href="{{route('page', ['query' => $page->item->slug])}}">{{$page->item->title}}</a>
                @endif
                </span>
            </li>
        @endforeach
    @endif
</ul>
