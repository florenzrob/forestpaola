<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
        @if(!empty($breadcrumb))
            @php($query = null)
            @foreach ($breadcrumb as $key => $b)
                @if($b->slug == $page->slug)
                    @if(!empty($article))
                        <li class="breadcrumb-item"><a href="{{route('page', ['query' => $query . '/' . $b->slug])}}">{{$b->title}}</a></li>
                    @else
                        <li class="breadcrumb-item active">{{$page->title}}</li>
                    @endif
                @else
                    @php($query = $query != '' ? $query . '/' . $b->slug : $b->slug)
                    <li class="breadcrumb-item"><a href="{{route('page', ['query' => $query])}}">{{$b->title}}</a></li>
                @endif
            @endforeach

            {{-- Article--}}
            @if(!empty($article))
                <li class="breadcrumb-item active">{{$article->title}}</li>
            @endif
        @else
            @if(!empty($article))
                <li class="breadcrumb-item active">{{$article->title}}</li>
            @endif
        @endif
    </ol>
</nav>
