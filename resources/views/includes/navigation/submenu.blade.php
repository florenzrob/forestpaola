@if($subitems != null)
    <ul class="dropdown-menu">
        @foreach ($subitems as $subitem)
            <li>
                @if($subitem->children != null)
                    <a class="dropdown-item dropdown-toggle dropdown-toggle" href="#" data-toggle="dropdown">{{$subitem->item->title}}</a>
                    @include('includes.navigation.submenu', ['parent' => $parent, 'subitems' => $subitem->children])
                @else
                    <a class="dropdown-item" href="{{route('page', ['query' => $subitem->route])}}">{{$subitem->item->title}}</a>
                @endif
            </li>
        @endforeach
    </ul>
@endif