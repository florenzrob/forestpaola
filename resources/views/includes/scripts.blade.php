<script src="{{ mix('/js/global.min.js') }}"></script>

{{-- Jquery --}}
{{--<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>--}}
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>


{{-- DataTables --}}
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
{{--
<script type="text/javascript" src="{{ asset('js/plugins/pagination/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pagination/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/pagination/dataTables.rowReorder.min.js') }}"></script>
--}}

{{--
*************************************************************************************
*
* DATATABLES VA INCLUSO PRIMA DI JQUERY-UI E BOOTSTRAP ALTRIMENTI GENERA CONFLITTI
*
*************************************************************************************
--}}

{{-- Jquery-ui --}}
{{--<script type="text/javascript" src="{{ asset('js/jquery-ui-1.12.1.min.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

{{-- Bootstrap --}}
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

{{--
<script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js') }}" ></script>
--}}
{{--<script src="{{ asset('js/bootstrap/bs-custom-file-input.min.js') }}" ></script>--}}
{{--
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
--}}

{{-- Font Awesome --}}
{{--<script src="{{ asset('js/fontawesome/all.min.js') }}" ></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/js/all.min.js"></script>

{{-- Plugins --}}

{{-- Owl Carousel --}}
<script type="text/javascript" src="{{ asset('js/plugins/owlcarousel/owl.carousel.min.js') }}"></script>

{{-- VERSIONE 4 --}}
<script type="text/javascript" src="{{ asset('js/plugins/tinymce/tinymce_4.7.13/tinymce.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tinymce/tinymce_4.7.13/jquery.tinymce.min.js') }}"></script>

{{-- Confirm: https://craftpip.github.io/jquery-confirm/ --}}
<script type="text/javascript" src="{{ asset('js/plugins/jquery-confirm/jquery-confirm.js') }}"></script>

{{-- Loader: https://sdkcarlos.github.io/sites/holdon.html --}}
<script type="text/javascript" src="{{ asset('js/plugins/loader/HoldOn.min.js') }}"></script>

{{-- Validate --}}
<script type="text/javascript" src="{{ asset('js/plugins/validation/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/validation/localization/messages_' . app()->getLocale() . '.js') }}"></script>

{{-- Datepicker --}}
<script type="text/javascript" src="{{ asset('js/plugins/jquery-ui/datepicker/datepicker-it.js') }}"></script>

{{-- Lazy loading --}}
<script type="text/javascript" src="{{ asset('js/plugins/lazy-loading/jquery.lazy.min.js') }}"></script>

{{-- Lightbox --}}
{{--<script type="text/javascript" src="{{ asset('js/plugins/lightbox/lightbox.min.js') }}"></script>--}}

{{-- Hide/Show Password --}}
<script src="{{ asset('js/plugins/pwd/bootstrap-show-password.min.js') }}" ></script>

{{-- Bootstrap Multiselect --}}
<script type="text/javascript" src="{{ asset('js/plugins/bs-multiselect/bootstrap-multiselect.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/plugins/bs-multiselect/BsMultiSelect.min.js') }}"></script>--}}

{{-- DateTimepicker with momentJs --}}
<script type="text/javascript" src="{{ asset('js/plugins/moment/moment_locales.2.24.0.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('js/plugins/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>

{{-- Dropzone --}}
<script type="text/javascript" src="{{ asset('js/plugins/dropzone/dropzone.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/dropzone/dropzone-config.js') }}"></script>

{{-- Uploader --}}
<script type="text/javascript" src="{{ asset('js/plugins/uploader/jquery.dm-uploader.min.js') }}"></script>

{{-- Unisharp File Manager --}}
<script type="text/javascript" src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>

{{-- Cookie plugin --}}
<script type="text/javascript" src="{{ asset('js/plugins/cookies/jquery-eu-cookie-law-popup.js') }}"></script>


{{-- Custom --}}
<script src="{{ mix('/js/global.min.js') }}"></script>

{{-- TYPE MODULE!!--}}
{{--<script type="module" src="{{ asset('js/main.js') }}" ></script>--}}
<script type="text/javascript" src="{{ asset('js/main.js') }}" ></script>

{{-- Extra script inclusi solo in determinate pagine --}}
@yield('extra-script')

