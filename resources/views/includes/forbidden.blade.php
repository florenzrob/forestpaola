<div class="alert alert-danger text-center" role="alert">
    <h4><i class="fas fa-exclamation-circle"></i> Attenzione, non hai accesso a questa risorsa</h4>
</div>
<p><h2 class="py-4 text-center"><a href="{{route('dashboard')}}" class="dashboard-back btn btn-link"><i class="fal fa-chevron-circle-left"></i> Torna alla dashboard</a></h2></p>
