<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500">

{{-- Jquery ui --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
{{--<link rel="stylesheet" href="{{ asset('css/plugins/jquery-ui/jquery-ui.min.css') }}">--}}

{{-- Bootstrap --}}
{{--<link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}">--}}
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

{{-- Font Awesome --}}
{{--<link rel="stylesheet" href="{{ asset('css/fontawesome/all.min.css') }}">--}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/css/all.min.css">

{{-- Plugins --}}

{{-- Owl Carousel --}}
<link rel="stylesheet" href="{{ asset('css/plugins/owlcarousel/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/plugins/owlcarousel/assets/owl.theme.default.min.css') }}">

{{-- Confirm --}}
<link rel="stylesheet" href="{{ asset('css/plugins/jquery-confirm/jquery-confirm.css') }}">

{{-- Lightbox --}}
<link rel="stylesheet" href="{{ asset('css/plugins/lightbox/lightbox.min.css') }}">

{{-- Datatables --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.bootstrap4.min.css">
{{--
<link rel="stylesheet" href="{{ asset('css/plugins/pagination/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/plugins/pagination/rowReorder.dataTables.min.css') }}">
--}}

{{-- Loader --}}
<link rel="stylesheet" href="{{ asset('css/plugins/loader/HoldOn.min.css') }}">

{{-- Bootstrap Multiselect --}}
<link rel="stylesheet" href="{{ asset('css/plugins/bs-multiselect/bootstrap-multiselect.css') }}">
{{--<link rel="stylesheet" href="{{ asset('css/plugins/bs-multiselect/BsMultiSelect.min.css') }}">--}}

{{-- DateTimepicker --}}
<link rel="stylesheet" href="{{ asset('css/plugins/datetimepicker/jquery.datetimepicker.min.css') }}">

{{-- Dropzone --}}
<link rel="stylesheet" href="{{ asset('css/plugins/dropzone/dropzone.min.css') }}">

{{-- Uploader --}}
<link rel="stylesheet" href="{{ asset('css/plugins/uploader/jquery.dm-uploader.min.css') }}">

{{--Filemanager--}}
<link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">


{{-- Cookie plugin --}}
<link rel="stylesheet" href="{{ asset('css/plugins/cookies/jquery-eu-cookie-law-popup.css') }}">

{{-- Custom --}}
<link rel="stylesheet" href="{{ asset('css/main.css') }}">
<link rel="stylesheet" href="{{ asset('css/hoverlay.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">

{{-- Extra style inclusi solo in determinate pagine --}}
@yield('extra-style')
