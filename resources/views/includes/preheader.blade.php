
<nav class="navbar navbar-expand-lg fixed-top custom-header bg-white text-dark shadow">
    <div class="container-fluid">

        {{-- Navbar brand --}}
    <a class="navbar-brand mr-5" href="{{route('home')}}">@if(!empty($settings->company_logo))<img src="{{ asset('images/uploads/' . $settings->company_logo) }}" style="max-height:{{!empty($settings->company_logo_height) ? $settings->company_logo_height : 70}}px;" alt="{{$settings->company_business_name}}">@else{{$settings->company_business_name}}@endif</a>

        {{-- Collapse button --}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>

        {{-- Collapsible content --}}
        <div class="collapse navbar-collapse" id="topNav">

            @include('includes.navigation.menu')

            <ul class="navbar-nav ml-auto">
                <li class="nav-item mr-5 d-flex flex-nowrap"><i class="fas fa-phone"></i> {{!empty($settings->company_mobile1) ? $settings->company_mobile1 : null}}</li>
                <li class="nav-item mr-5 d-flex flex-nowrap">
                    @if(!empty($settings->company_email1))
                        <a href="mailto:{{$settings->company_email1}}" class="text-dark"><i class="fas fa-envelope"></i> {{$settings->company_email1}}</a>
                    @endif
                </li>
                <li class="nav-item mr-5 md-flex flex-nowrap">
                    @include('includes.navigation.socials', ['text_color' => 'text-dark'])
                </li>
            </ul>
            <br>



        </div>

    </div>
</nav>
{{--
<div class="fixed-top border-bottom pt-2 top-bar bg-green text-dark shadow-sm">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6 col-md-4"><i class="fas fa-phone"></i> {{!empty($settings->company_mobile1) ? $settings->company_mobile1 : null}}</div>
            <div class="col-6 col-md-4">
                @if(!empty($settings->company_email1))
                    <a href="mailto:{{$settings->company_email1}}" class="text-dark"><i class="fas fa-envelope"></i> {{$settings->company_email1}}</a>
                @endif
                </div>
            <div class="d-none d-md-block col-md-4 text-right">
                @include('includes.navigation.socials', ['text_color' => 'text-dark'])
            </div>
        </div>
    </div>
</div>
--}}
