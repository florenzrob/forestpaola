@if(!empty($urls))

    @foreach ($urls as $url)

        @if(strtolower($url['extension']) == 'pdf')
            @php($file = '<i class="fas fa-file-pdf fa-2x text-secondary "></i>')
        @else
            @php($file = '<img class="img-thumbnail" style="max-width:80px; height:auto" src="' . asset('images/uploads/' . $url['filename']) . '">')
        @endif

        <tr>
            <td>
                <a href="{{ asset('images/uploads/' . $url['filename'])}}" target="_blank">{!!$file!!}</a>
            </td>
            <td>
                <p class="mb-2">
                    <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
                </p>
                <div class="progress mb-2">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar"
                        style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    </div>
                </div>
                <hr class="mt-1 mb-1" />
            </td>
            <td>
                <a class="btn btn-danger uploader-delete-file"><i class="fas fa-trash-alt"></i></a> 
            </td>
        </tr>
    @endforeach
@else
    <tr class="empty">
        <td colspan="3" class="text-muted text-center">{{ __('admin.uploader.no_file_uploaded')}}</td>
    </tr>
@endif




{{-- File item template --}}
@include('admin.medias.uploader_template')

