 {{-- 
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.medias.manage_images')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

@if(!empty($alert))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => $alert])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.medias.manage_images') }}</h2>
        </div>
    </div>

    {{-- Dropzone --}}
    @include('admin.medias.dropzone', ['route' => route('dropzone.store'), 'fieldLabel' =>  __('admin.medias.add_images')])

    <div id="images_grid_container">
        @include('admin.medias.images.grid', ['delete' => true])
    </div>
</div>