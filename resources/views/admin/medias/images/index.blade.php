@extends('admin.dashboard')

@section('extra-script')
    @parent
    {{-- TYPE MODULE!!--}}
    <script type="module" src="{{ asset('js/admin/medias.js') }}" ></script>   
@endsection


@section('dashboard-content')

    <div id="images_container">
        @include('admin.medias.images.list')
    </div>

@endsection