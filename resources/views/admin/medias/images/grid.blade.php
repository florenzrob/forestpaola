@php ($canDelete = isset($delete) && $delete == true ? true : false)
@php ($canSelect = isset($select) && $select == true ? true : false)
@php ($images = isset($cl->mediaitems) ? $cl->mediaitems : null)
@php ($route = route('gallery.images.add'))

{{-- Alerts --}}
@if(isset($success))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => $success])
@endif

@if(isset($error))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => $error])
@endif

<div class="container-fluid mt-2">
    {{Form::open(array('id' => 'gridImages', 'data-route' => $route, 'class' => 'form-horizontal')) }}

        {{-- TODO - Link Lista FOLDER --}}
        @if(!empty($folders))
            <div class="card-body">
                <div class="row mb-3">
                    @php($count = 0)
                    @foreach($folders as $folder)
                        @if($count >= 6)
                            @php($count = 0)
                            </div>
                            <div class="row mb-3">
                        @endif
                        <div class="col">
                            <div class="card p-3">
                                <a
                                class="navigate-folder"
                                data-route="{{route('gallery.openfolder')}}"
                                data-folder="{{$folder}}"><i class="far fa-folder fa-5x"></i></a>
                                <br>
                                <a href="{{route('gallery.openfolder1', ['folder' => $folder])}}">-->{{$folder}}</a>
                            </div>
                        </div>
                        @php($count++)
                    @endforeach
                </div>
            </div>
        @endif

        @if(!empty($medias))
            <div class="scard-body">
                <div class="row mb-3">
                    {{--@php($count = 0)--}}
                    @foreach($medias as $media)
                        @php($showImage = true)

                        {{-- Nascondo le immagini già salvate --}}
                        @if(isset($cl) && !empty($cl->mediaitems))
                            @foreach($cl->mediaitems as $item)
                                @if($item->path == $media['filename'])
                                    {{-- Immagine già inserita --}}
                                    @php($showImage = false)
                                @endif
                            @endforeach
                        @endif

                        @php($cssClasses = '')
                        @php($btnClasses = 'text-muted')
                        @php($filename = '')

                        @if($showImage)
                            @if(!empty($images))
                                @foreach($images as $image)
                                    @if($cssClass = $image->path == $media['filename'])
                                        @php($cssClasses = 'border border-info border-selected')
                                        @php($btnClasses = 'text-info')
                                        @php($filename = $media['filename'])
                                    @endif
                                @endforeach
                            @endif

                            @php($uri = $media['dir'] . '/' . $media['filename'])
                            {{--
                            @if($count >= 6)
                                @php($count = 0)
                                </div>
                                <div class="row mb-3">
                            @endif
                            --}}
                            <div class="col-sm-12 col-md-4 col-lg-2">

                                @if($canDelete)
                                    {{-- Lista immagini --}}
                                    <div class="coverimage">
                                        <div class="img">
                                            <a href="#"
                                                data-route="{{route('image.delete')}}"
                                                data-filename="{{$media['filename']}}"
                                                data-container-div="images_grid_container"
                                                class="icon btn btn-danger shadow-sm delete-image"
                                                data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                                                data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                                                data-btn-confirm="{{ __('admin.yes')}}"
                                                data-btn-dismiss="{{ __('admin.no')}}"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="{{__('admin.uploader.delete_title')}}"><i class="{{config('settings.icons.delete')}}"></i>
                                            </a>
                                            <a href="{{ asset('images/' . $uri) }}" data-lightbox="{{$uri}}" target="_blank"><img id="image_{{$loop->index}}" class="{{$cssClasses}} img-fluid" src="{{ asset('images/' . $uri) }}" title="{{$media['filename']}}" alt="{{$media['filename']}}"></a>
                                        </div>
                                    </div>
                                    <div class="alert alert-secondary mt-1 p-1 rounded-0"><i class="fas fa-tag mr-1"></i> {{$media['filename']}}</div>
                                @elseif($canSelect)
                                    {{-- Modifica gallery --}}
                                    <a href="#"
                                        id="select_{{$loop->index}}"
                                        data-loop="{{$loop->index}}"
                                        data-route="{{route('image.select')}}"
                                        data-filename="{{$media['filename']}}"
                                        data-id="hidden_{{$loop->index}}"
                                        data-image-id="image_{{$loop->index}}"
                                        class="check-image"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="{{__('admin.select')}}">

                                        <div class="coverimage">
                                            <div class="img">
                                                {{--
                                                <div class="icon btn-group" role="group">
                                                    <div class="btn btn-light {{$btnClasses}} shadow-sm">
                                                        <i class="{{config('settings.icons.check')}}"></i>
                                                    </div>
                                                </div>
                                                --}}
                                                {!! Form::hidden('images[]', $filename, ['id' => 'hidden_' . $loop->index])!!}
                                                <img id="image_{{$loop->index}}" class="{{$cssClasses}} img-fluid" src="{{ asset('images/' . $uri) }}" title="{{$media['filename']}}" alt="{{$media['filename']}}">
                                            </div>
                                        </div>
                                        <div class="alert alert-secondary mt-1 p-1 rounded-0"><i class="fas fa-tag mr-1"></i> {{$media['filename']}}</div>
                                    </a>
                                @endif
                            </div>
                        @endif
                        {{--@php($count++)--}}
                    @endforeach
                </div>
            </div>
        @endif
    {{ Form::close() }}
</div>
