{{--
Dropozone parameters:
- $fieldLabel: label per il canvas
- $route: rotta di upload files
- $fileListContainer: OPZ. Div che contiene i file caricati 
- $debug: true/false. Abilita messaggi debug.
--}}
@php($fileListContainer = isset($fileListContainer) ? $fileListContainer : 'files')
{{-- default fileListContainer --}}
@php($debug = isset($debug) ? true : false) {{-- default false --}}

<fieldset class="mb-3">
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="drag-and-drop-zone dm-uploader p-4 text-center" data-route="{{$route}}">
                <h3 class="mb-2 mt-2 text-muted">{{__('admin.uploader.dnd')}}</h3>
                <div class="btn btn-primary btn-block mb-5">
                    <span>{{__('admin.uploader.choose_file')}}</span>
                    <input type="file" title="{{__('admin.uploader.click_to_add')}}" />
                </div>
            </div>
        </div>

        {{--File list --}}
        <div class="col-md-6 col-sm-12">
            <div class="card h-100">
                <div class="card-header">
                    Preview files
                </div>
                <div class="p-3">
                    <table class="table table table-sm uploader-list" 
                        id="{{$fileListContainer}}"
                        data-delete-route="{{ route('uploader.destroy')}}"
                        data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                        data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                        data-btn-confirm="{{ __('admin.yes')}}" 
                        data-btn-dismiss="{{ __('admin.no')}}"

                    >
                        @include('admin.medias.uploader_list')
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if($debug)
        <div class="row">
            <div class="col-12">
                <div class="card h-100">
                    <div class="card-header">
                        Debug Messages
                    </div>

                    <ul class="list-group list-group-flush" id="debug">
                        <li class="list-group-item text-muted empty">Loading plugin....</li>
                    </ul>
                </div>
            </div>
        </div>
    @endif


    {{-- Debug item template --}}
    <script type="text/html" id="debug-template">
        <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
    </script>

</fieldset>