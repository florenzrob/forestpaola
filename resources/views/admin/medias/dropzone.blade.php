{{--
Dropozone parameters:
- $fieldLabel: label per il canvas
- $route: rotta di upload files
- $fileContainer: OPZ. Div che contiene i file caricati 
- $multiple:  
--}}
@php($route = route('dropzone.store'))
@php($fileContainer = isset($fileContainer) ? $fileContainer : 'images_grid_container') {{-- default file-container --}}
@php($multiple = isset($multiple) ? true : false) {{-- default false --}}


<div class="form-row">
    <div class="form-group col-sm-12">
        <label for="dropzone-container">{{$fieldLabel}}</label>
        <div id="dropzone-container" 
            data-url="{{$route}}" data-reload-div="{{$fileContainer}}" class="dropzone dropzone_images">
            <div class="fallback">
                <input name="file" class="form-control-file" type="file" @if($multiple)multiple @endif />
            </div>

            <div class="dz-message">
                <i class="check-icon fas fa-download fa-5x"></i>
                <br>
                <span class="title1">{{__('messages.choose_file')}}</span> <span class="title2">{{__('messages.or')}} {{__('messages.drop_file')}}</span>
            </div>
        </div>
    </div>
</div>

    