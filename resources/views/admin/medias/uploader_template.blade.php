<script type="text/html" id="files-template">
    
    <tr>
        <td>
            <a href="{{ asset('images/placeholder.png') }}" target="_blank"><img class="mr-3 mb-2 preview-img" src="{{ asset('images/placeholder.png') }}" alt="Placeholder image" style="max-width:80px;"></a>
        </td>
        <td>
            <p class="mb-2">
                <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
            </p>
            <div class="progress mb-2">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar"
                    style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
            <hr class="mt-1 mb-1" />
        </td>
        <td>
            <a class="btn btn-danger uploader-delete-file"><i class="fas fa-trash-alt"></i></a> 
        </td>
    </tr>

</script>

{{--
<script type="text/html" id="files-template">
    
    <li class="media">
        <a href="#" class="uploader-delete-file"><img class="mr-3 mb-2 preview-img" src="{{ asset('images/placeholder.png') }}" alt="Placeholder image" style="max-width:100px;"></a>
        <div class="media-body mb-1">
            <p class="mb-2">
                <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
            </p>
            <div class="progress mb-2">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar"
                    style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
            <hr class="mt-1 mb-1" />
        </div>
    </li>

</script>
--}}