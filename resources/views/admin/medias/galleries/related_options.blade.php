@if(!empty($records))
    @if(!$records->isEmpty())
        @foreach($records as $record)
            @php($selectedRecord = isset($cl) && $cl->record_id == $record->id ? 'selected' : '')
            <option value="{{$record->id}}" {{$selectedRecord}}>{{$record->title}}</option>
        @endforeach
    @else
        <option selected>{{__('admin.medias.related_model_empty')}}</option>
    @endif
@else
    <option selected>{{__('admin.medias.related_model_choose')}}</option>
@endif