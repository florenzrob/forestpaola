 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.medias.manage_galleries')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.medias.manage_galleries') }}</h2>
            <a href="{{route('gallery.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($galleries))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => 'reorder', 'name' => 'admin.id'],
                        ['class' => '', 'name' =>  'admin.medias.type'],
                        ['class' => '', 'name' =>  'admin.positions.position'],
                        ['class' => '', 'name' =>  'admin.categories.categories'],
                        ['class' => '', 'name' =>  'admin.medias.related_model'],
                        ['class' => '', 'name' =>  'admin.name'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($galleries as $gallery)
                        @php($cl = $gallery['gallery'])
                        @php($record = $gallery['record'])
                    {{--dd($record)--}}
                        <tr>
                            <th class="px-3 align-middle text-center orderme" scope="row">#{{$cl->id}}</th>
                            <td nowrap class="px-3 align-middle">@if(!empty($cl->mediatype)){{ucfirst($cl->mediatype->type)}}@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle">@if(!empty($cl->mediaposition)){{ucfirst($cl->mediaposition->name)}}@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle">@if(!empty($cl->mediarelatedmodel)){{ucfirst($cl->mediarelatedmodel->modelname)}}@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle">@if(!empty($record)){{$record->title}}@else<div class="text-center">-</div>@endif</td>

                            <td nowrap class="px-3 align-middle w-100"><a href="{{route('gallery.edit', ['id' => $cl->id])}}" class="lead">{{$cl->name}}</a></td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('gallery.edit', ['id' => $cl->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $cl->name}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#"
                                            data-route="{{route('gallery.delete', ['id' => $cl->id])}}"
                                            data-id="{{$cl->id}}"
                                            class="btn btn-light shadow-sm delete-gallery"
                                            data-confirm-title="{{ __('admin.medias.delete_gallery')}}"
                                            data-confirm-message="{{ __('admin.medias.delete_gallery_message')}}"
                                            data-btn-confirm="{{ __('admin.yes')}}"
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.delete') . ' ' . $cl->name}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
