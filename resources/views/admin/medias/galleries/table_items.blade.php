@php
    $classForm = config('settings.forms.classForm');
@endphp

@if(!empty($cl->mediaitems))
        <div class="card-body table-responsive">
            <table class="table table-striped stable-hover stable-paginate mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => 'reorder', 'name' => 'admin.id'],
                        ['class' => '', 'name' =>  'admin.image'],
                        ['class' => '', 'name' =>  'admin.medias.link_type'],
                        ['class' => '', 'name' =>  'admin.link'],
                        ['class' => '', 'name' =>  'admin.target_link'],
                        ['class' => '', 'name' =>  'admin.title'],
                        ['class' => '', 'name' =>  'admin.subtitle'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($cl->mediaitems as $media)
                        @php($uri = $media->path)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$media->id}}</th>
                            <td nowrap class="px-3 align-middle">
                                <a href="{{ asset('images/uploads/' . $uri) }}" data-lightbox="{{$uri}}" target="_blank"><img id="image_{{$media->id}}" class="img-fluid" src="{{ asset('images/uploads/' . $uri) }}" title="{{$media->path}}" alt="{{$media->path}}" style="max-width:80px;"></a>
                                {!! Form::hidden('path_' . $media->id, $media->path, ['id' => 'path_' . $media->id])!!}
                                {!! Form::hidden('ids[]', $media->id, ['id' => 'id_' . $media->id])!!}
                            </td>
                            <td nowrap class="px-3 align-middle">
                                @if(!empty($enumsLinktype))
                                    <select name="link_type_{{$media->id}}" id="link_type_{{$media->id}}" class="custom-select">
                                        @foreach ($enumsLinktype as $enum)
                                            @php($selectedEnumLink = $media->link_type == $enum ? 'selected' : '')
                                            @php($enum = empty($enum) ? '---' : $enum)
                                            <option value="{{$enum}}" {{$selectedEnumLink}}>{{$enum}}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </td>
                            <td nowrap class="px-3 align-middle">
                                {!! Form::text('link_' . $media->id, $media->link, ['id' => 'link_' . $media->id, 'class' => $classForm, 'placeholder' => __('admin.link')])!!}
                            </td>
                            <td nowrap class="px-3 align-middle">
                                @if(!empty($enumsLinktarget))
                                    <select name="target_{{$media->id}}" id="target_{{$media->id}}" class="custom-select">
                                        @foreach ($enumsLinktarget as $enum)
                                            @php($selectedEnumTarget = $media->target == $enum ? 'selected' : '')
                                            @php($enum = empty($enum) ? '---' : $enum)
                                            <option value="{{$enum}}" {{$selectedEnumTarget}}>{{$enum}}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </td>
                            <td nowrap class="px-3 align-middle">
                                {!! Form::text('title_' . $media->id, $media->title, ['id' => 'title_' . $media->id, 'class' => $classForm, 'placeholder' => __('admin.title')])!!}
                            </td>
                            <td nowrap class="px-3 align-middle">
                                {!! Form::text('subtitle_' . $media->id, $media->subtitle, ['id' => 'subtitle_' . $media->id, 'class' => $classForm, 'placeholder' => __('admin.subtitle')])!!}
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="#" 
                                            data-route="{{route('gallery.delete.item', ['id' => $media->id])}}" 
                                            data-gallery-id="{{$cl->id}}" 
                                            class="btn btn-light shadow-sm delete-gallery-item" 
                                            data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                                            data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                                            data-btn-confirm="{{ __('admin.yes')}}" 
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            title="{{__('admin.delete') . ' ' . $media->path}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td nowrap class="px-3 align-middle"></td>
                            <td colspan="7" nowrap class="px-3 align-middle">
                                <button type="button" id="btn_{{$media->id}}" class="btn alert alert-success btn-block text-left" data-toggle="collapse" data-target="#collapsing_{{$media->id}}"><i class="{{config('settings.icons.edit')}} mr-2"></i> <i class="fas fa-angle-double-down mr-2"></i> {{__('admin.medias.edit_content')}}</button>
                                <div id="collapsing_{{$media->id}}" aria-labelledby="btn_{{$media->id}}" class="collapse @if(!empty($media->content))show @endif">
                                    {!! Form::textarea('content_' . $media->id, $media->content, ['id' => 'content_' . $media->id, 'class' => $classForm . ' text-editor', 'placeholder' => __('admin.content')])!!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif