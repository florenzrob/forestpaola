@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formGallery';
    $submitId = 'submitGallery';
    $resetId = 'resetGallery';
    $exitId = 'exitGallery';
    $exitRoute = route('galleries.list');

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'gallery.update' ? route($routeName, ['id' => $cl->id]) : route($routeName);

@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    {{-- TYPE MODULE!!--}}
    <script type="module" src="{{ asset('js/admin/medias.js') }}" ></script>
@endsection

@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => [
            ['route' => route('galleries.list'), 'label' => __('admin.medias.galleries_list')],
        ],
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header form-header-title">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">{{$cl->name}}</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'url' => $route, 'class' => 'form-horizontal', 'files' => true)) }}


                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        <label id="lbl_type_id" for="type_id">{{__('admin.medias.type')}}</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.medias.type')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>

                            {{-- Gallery type --}}
                            <select name="mediatype_id" id="mediatype_id" class="custom-select">
                                @foreach ($types as $tp)
                                    @php($selectedType = $cl->mediatype_id == $tp->id ? 'selected' : '')
                                    <option value="{{$tp->id}}" {{$selectedType}}>{{ucfirst($tp->type)}} @if(!empty($tp->description)) - {{$tp->description}}@endif</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        <label id="lbl_type_id" for="type_id">{{__('admin.positions.position')}}</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.positions.position')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>

                            {{-- Gallery position --}}
                            <select name="mediaposition_id" id="mediaposition_id" class="custom-select">
                                @foreach ($positions as $position)
                                    @php($selectedPosition = $cl->mediaposition_id == $position->id ? 'selected' : '')
                                    <option value="{{$position->id}}" {{$selectedPosition}}>{{ucfirst($position->name . ' - ' . $position->description)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        <label id="lbl_type_id" for="type_id">{{__('admin.medias.related_model')}}</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.medias.related_model')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>

                            {{-- related model --}}
                            <select name="mediarelatedmodel_id" id="mediarelatedmodel_id" class="custom-select"
                                data-route="{{route('gallery.getrecords')}}"
                            >
                                <option selected value="">----</option>
                                @foreach ($relateds as $related)
                                    @php($selectedModel = $cl->mediarelatedmodel_id == $related->id ? 'selected' : '')
                                    <option value="{{$related->id}}" {{$selectedModel}}>{{$related->description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        <label id="lbl_record_id" for="record_id">{{__('admin.medias.related_record')}}</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.medias.related_record')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>

                            {{-- related record --}}
                            <select name="record_id" id="record_id" class="custom-select">
                                <option>{{__('admin.medias.related_model_choose')}}</option>
                                @include('admin.medias.galleries.related_options')
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-6">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.name'),
                            'name' => 'name',
                            'value' => $cl->name,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.description'),
                            'name' => 'description',
                            'value' => $cl->description,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                <hr>

                <div id="table_items_container">
                    @include('admin.medias.galleries.table_items')
                </div>

                <hr>

                <button id="open_images"
                    type="button"
                    class="btn btn-block alert alert-success"
                    data-gallery-id="{{$cl->id}}"
                    data-route="{{route('gallery.openmodal')}}"
                    data-route-add-images="{{route('gallery.images.add')}}"
                    data-dialog-title="{{ __('admin.medias.choose_images')}}"
                    data-btn-confirm="{{ __('admin.add')}}"
                    data-btn-dismiss="{{ __('admin.close')}}">
                    {{__('admin.medias.choose_images')}}
                </button>

                {{--@include('admin.medias.images.grid', ['select' => true])--}}

                <hr>

                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            'exitId' => $exitId,
                            'exitCustomClass' => '',
                            'exitText' =>  __('admin.exit'),
                            'exitRoute' =>  $exitRoute,
                            'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
