<a id="{{$btnId}}" data-holder="{{$imgId}}" href="#" class="icon btn btn-danger shadow-sm delete-tmp-image" style="display:none">
    <i class="{{config('settings.icons.delete')}}"></i>
</a>
<img id="{{$imgId}}" class="img-fluid" src="" style="max-height:100px;" />
