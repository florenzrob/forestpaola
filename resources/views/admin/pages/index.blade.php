@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/pages.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="pages_container">
        @include('admin.pages.list')
    </div>

@endsection
