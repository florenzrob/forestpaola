@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formPage';
    $submitId = 'submitPage';
    $resetId = 'resetPage';
    $exitId = 'exitPage';
    $exitRoute = route('pages.list');
    $isHomeDefault = $page->id == null ? 0 : $page->is_home;
    $homeDefault = $page->id == null ? 0 : $page->on_home;
    $privateDefault = $page->id == null ? 0 : $page->is_private;
    $visibleDefault = $page->id == null ? 1 : $page->visible;
    $showSidebarDxDefault = $page->show_sidebar_dx == null ? 0 : $page->show_sidebar_dx;

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'page.update' ? route($routeName, ['id' => $page->id]) : route($routeName);
@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/pages.js') }}" ></script>
@endsection

@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => [
            ['route' => route('pages.list'), 'label' => __('admin.pages.list')],
        ],
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header form-header-title">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">{{$page->title}}</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'name' => $formId, 'url' => $route, 'class' => 'form-horizontal', 'files' => true)) }}

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('is_home', $isHomeDefault,  $isHomeDefault == 1 ? true : false, ['id' => 'is_home', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('is_home', __('admin.is_home'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('on_home', $homeDefault,  $homeDefault == 1 ? true : false, ['id' => 'on_home', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('on_home', __('admin.on_home'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('is_private', $privateDefault,  $privateDefault == 1 ? true : false, ['id' => 'is_private', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('is_private', __('admin.is_private'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('visible', $visibleDefault,  $visibleDefault == 1 ? true : false, ['id' => 'visible', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('visible', __('admin.visible'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('show_sidebar_dx', $showSidebarDxDefault,  $showSidebarDxDefault == 1 ? true : false, ['id' => 'show_sidebar_dx', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('show_sidebar_dx', __('admin.show_sidebar_dx'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>

                </div>

                {{--Cover image --}}
                <div class="form-row mb-3">
                    <div class="form-group col-sm-4 col-md-2">
                        <a id="upload_cover" data-input="image_cover" data-preview="image_holder" class="btn btn-outline-secondary"><i class="far fa-image"></i> {{__('admin.image_cover')}}</a>
                        {!! Form::hidden('image_cover', $page->image_cover, ['id' => 'image_cover', 'class' => $classForm, 'data-bnt-delete' => 'image_holder_btn', 'placeholder' => __('admin.image_cover')])!!}

                        <p class="mt-3 mr-5">
                            <label for="cover_height">{{__('admin.cover_height')}}</label>
                            <span class="text-danger">{{ $errors->first('cover_height') }}</span>
                            {!! Form::text('cover_height', $page->cover_height, ['id' => 'cover_height', 'class' => $classForm, 'placeholder' => __('admin.cover_height') . ' *'])!!}
                        </p>

                    </div>

                    <div class="form-group col-sm-8 col-md-10">
                        <div class="coverimage">
                            <div class="img">
                                @if(!$page->image_cover == null)
                                    <a href="#"
                                        data-route="{{route('page.cover.delete')}}"
                                        data-id="{{$page->id}}"
                                        data-filename="{{$page->image_cover}}"
                                        data-container-div="coverimage"
                                        data-input-id="image_cover"
                                        class="icon btn btn-danger shadow-sm delete-image"
                                        data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                                        data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                                        data-btn-confirm="{{ __('admin.yes')}}"
                                        data-btn-dismiss="{{ __('admin.no')}}"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="{{__('admin.uploader.delete_title')}}"><i class="{{config('settings.icons.delete')}}"></i></a>

                                        <a href="{{ asset('images/uploads/' . $page->image_cover) }}" data-lightbox="{{$page->image_cover}}" target="_blank">
                                            <img id="image_holder" class="img-fluid" src="{{ asset('images/uploads/' . $page->image_cover) }}" title="{{__('admin.image_cover')}}" alt="{{__('admin.image_cover')}}" style="max-height:100px;">
                                        </a>
                                @else
                                    @include('admin.medias.image_holder', ['btnId' => 'image_holder_btn', 'imgId' => 'image_holder'])
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                {{--
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @if(!$page->image_cover == null)
                            <div class="coverimage">
                                <div class="img">
                                    <a href="#"
                                        data-route="{{route('page.cover.delete')}}"
                                        data-id="{{$page->id}}"
                                        data-filename="{{$page->image_cover}}"
                                        data-container-div="coverimage"
                                        class="icon btn btn-danger shadow-sm delete-image"
                                        data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                                        data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                                        data-btn-confirm="{{ __('admin.yes')}}"
                                        data-btn-dismiss="{{ __('admin.no')}}"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="{{__('admin.uploader.delete_title')}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    <a href="{{ asset('images/uploads/' . $page->image_cover) }}" data-lightbox="{{$page->image_cover}}" target="_blank"><img class="img-fluid" src="{{ asset('images/uploads/' . $page->image_cover) }}" title="{{__('admin.image_cover')}}" alt="{{__('admin.image_cover')}}" style="max-height:100px;"></a>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-sm-12 col-md-8">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.uploader.choose_file'),
                            'toggleTitle' => __('admin.image_cover'),
                            'name' => 'image_cover',
                            'value' => $page->image_cover,
                            'classes' => 'form-control-file',
                            'type' => 'file',
                        ])
                    </div>
                </div>
                --}}

                <hr>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.parent')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>
                            <select name="parent_id" id="parent_id" class="custom-select">
                                <option value="">----</option>
                                @foreach ($pages as $parent)
                                    @php($selected = $page->parent_id == $parent->id ? 'selected' : '')
                                    <option value="{{$parent->id}}" {{$selected}}>{{$parent->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-6">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.title'),
                            'name' => 'title',
                            'value' => $page->title,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    {{--
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.slug'),
                            'name' => 'slug',
                            'value' => $page->slug,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    --}}
                    <div class="form-group col-sm-12 col-md-6">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.subtitle'),
                            'name' => 'subtitle',
                            'value' => $page->subtitle,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.pages.seo_title'),
                            'name' => 'seo_title',
                            'value' => $page->seo_title,
                            'classes' => $classForm,
                        ])
                    </div>
                    <div class="form-group col-sm-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.pages.seo_content'),
                            'name' => 'seo_content',
                            'value' => $page->seo_content,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-3">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.pages.page_categories')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>
                            <select name="categories[]" id="categories" class="{{$classForm}} multiselect custom-select" multiple="multiple">

                                @if(!empty($categories))
                                    @foreach ($categories as $category)
                                        @php($selected = '')
                                        @if(!empty($page->categories))
                                            @foreach ($page->categories as $pc)
                                                @if($pc->pivot->category_id == $category->id)
                                                    @php($selected = 'selected')
                                                @endif
                                            @endforeach
                                        @endif
                                        <option value="{{$category->id}}" {{$selected}}>{{$category->title}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.abstract'),
                            'name' => 'abstract',
                            'value' => $page->abstract,
                            'classes' => $classForm . ' text-editor-compact',
                            'type' => 'textarea',
                        ])
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.content'),
                            'name' => 'content',
                            'value' => $page->content,
                            'classes' => $classForm . ' text-editor',
                            'type' => 'textarea',
                        ])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.video_link'),
                            'name' => 'video_link',
                            'value' => $page->video_link,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                {{-- Dropzone --}}
                {{-- @include('admin.medias.dropzone', ['route' => route('dropzone.store'), 'fieldLabel' =>  __('admin.image_cover')]) --}}

                <hr>

                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            'exitId' => $exitId,
                            'exitCustomClass' => '',
                            'exitText' =>  __('admin.exit'),
                            'exitRoute' =>  $exitRoute,
                            'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
