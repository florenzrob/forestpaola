 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.pages.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.pages.title') }}</h2>
            <a href="{{route('page.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($pages))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1"
                data-ordering="true"
                data-searching="true"
                data-orderBy="0"
                data-orderDirection="asc"
                data-success="updatePagesList"
                data-route="{{route('page.reorder')}}">

                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.parent'],
                        ['class' => '', 'name' =>  'admin.title'],
                        ['class' => '', 'name' =>  'admin.categories.categories'],
                        //['class' => '', 'name' =>  'admin.icon'],
                        ['class' => '', 'name' =>  'admin.is_home'],
                        ['class' => '', 'name' =>  'admin.on_home'],
                        ['class' => '', 'name' =>  'admin.show_sidebar_dx'],
                        ['class' => '', 'name' =>  'admin.is_private'],
                        ['class' => '', 'name' =>  'admin.visible'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($pages as $page)
                    {{--dd($pages)--}}
                        <tr id="{{$page->id}}">
                            <th class="px-3 align-middle text-center orderme" scope="row">{{$page->sort}}</th>
                            <td nowrap class="px-3 align-middle">@if(!empty($page->parent))<span class="badge border border-warning p-2">{{$page->parent->title}}</span>@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle w-100"><a href="{{route('page.edit', ['id' => $page->id])}}" class="lead">{{$page->title}}</a></td>
                            <td nowrap class="px-3">
                                @if(!empty($page->categories))
                                    @foreach ($page->categories as $pc)
                                        <span class="badge border border-success p-2">{{$pc->title}}</span>
                                    @endforeach
                                @endif
                            </td>
                            {{--<td nowrap class="px-3 align-middle">{{$page->icon}}</td>--}}
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('is_home', $page->is_home,  $page->is_home == 1 ? true : false, ['id' => 'is_home' . $page->id, 'data-route' => route('page.update.is_home', ['id' => $page->id]), 'class' => 'page-ishome-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('on_home', $page->on_home,  $page->on_home == 1 ? true : false, ['id' => 'on_home' . $page->id, 'data-route' => route('page.update.on_home', ['id' => $page->id]), 'class' => 'page-onhome-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('show_sidebar_dx', $page->show_sidebar_dx,  $page->show_sidebar_dx == 1 ? true : false, ['id' => 'show_sidebar_dx' . $page->id, 'data-route' => route('page.update.show_sidebar_dx', ['id' => $page->id]), 'class' => 'page-sidebardx-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('is_private', $page->is_private,  $page->is_private == 1 ? true : false, ['id' => 'is_private_' . $page->id, 'data-route' => route('page.update.is_private', ['id' => $page->id]), 'class' => 'page-private-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('visible', $page->visible,  $page->visible == 1 ? true : false, ['id' => 'visible_' . $page->id, 'data-route' => route('page.update.visible', ['id' => $page->id]), 'class' => 'page-visible-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('page.edit', ['id' => $page->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $page->title}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#"
                                            data-route="{{route('page.delete', ['id' => $page->id])}}"
                                            data-id="{{$page->id}}"
                                            class="btn btn-light shadow-sm delete-item"
                                            data-confirm-title="{{ __('admin.pages.delete_title')}}"
                                            data-confirm-message="{{ __('admin.pages.delete_message', ['name' => $page->title])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}"
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.delete') . ' ' . $page->title}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
