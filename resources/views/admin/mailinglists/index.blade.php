@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/mailinglists.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="mailinglists_container">
        @include('admin.mailinglists.list')
    </div>

@endsection
