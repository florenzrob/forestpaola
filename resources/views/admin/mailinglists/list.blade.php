 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.mailinglists.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.mailinglists.title') }}</h2>
            <a href="{{route('mailinglist.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>

    @include('admin.mailinglists.includes.mailing_result')

    @if(!empty($mailinglists))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => 'reorder', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.mailinglists.subject'],
                        ['class' => '', 'name' =>  'admin.mailinglists.sent_groups'],
                        ['class' => '', 'name' =>  'admin.mailinglists.attachment'],
                        ['class' => '', 'name' =>  'admin.mailinglists.sent_date'],
                        ['class' => '', 'name' =>  'admin.mailinglists.sent'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($mailinglists as $mailinglist)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$mailinglist->id}}</th>
                            <td nowrap class="px-3 align-middle"><a href="{{route('mailinglist.edit', ['id' => $mailinglist->id])}}" class="lead">{{$mailinglist->subject}}</a></td>
                            <td class="px-3 align-middle w-100">
                                @if(!empty($mailinglist->groups))
                                    @foreach ($mailinglist->groups as $group)
                                        @php($comma = $loop->index < count($mailinglist->groups)-1 ? ', ' : '')
                                        {{$group->name}}{{$comma}}
                                    @endforeach
                                @endif
                            </td>
                            <td nowrap class="px-3 align-middle">@if(!empty($mailinglist->attachment))<a href="{{asset('images/uploads/files/'.$mailinglist->attachment)}}" target="_blank">{{$mailinglist->attachment}}</a>@endif</td>
                            <td nowrap class="px-3 align-middle">@if(!empty($mailinglist->sent_date)){{\Carbon\Carbon::parse($mailinglist->sent_date)->format('d-m-Y')}}@endif</td>
                            <td nowrap class="px-3 align-middle">@if($mailinglist->sent)<i class="fas fa-check text-success"></i>@endif</td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="#"
                                        @if(count($mailinglist->groups) < 1) disabled @endif
                                            data-route="{{route('mailinglist.send')}}"
                                            data-id="{{$mailinglist->id}}"
                                            class="btn btn-light mailing-send shadow-sm mr-2
                                            @if(count($mailinglist->groups) < 1) disabled @endif
                                            "
                                            data-confirm-title="{{ __('admin.mailinglists.mailing_send')}}"
                                            data-confirm-message="{{ __('admin.mailinglists.mailing_confirm_send')}} @if($mailinglist->sent)<p><span class='text-danger'>{{ __('admin.mailinglists.mailing_confirm_send_alert')}}</span></p>@endif"
                                            data-btn-confirm="{{ __('admin.send')}}"
                                            data-btn-dismiss="{{ __('admin.cancel')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.mailinglists.send')}}"><i class="fas fa-paper-plane"></i></a>

                                        <a href="{{route('mailinglist.edit', ['id' => $mailinglist->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $mailinglist->subject}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#"
                                            data-route="{{route('mailinglist.delete', ['id' => $mailinglist->id])}}"
                                            data-id="{{$mailinglist->id}}"
                                            class="btn btn-light shadow-sm delete-item"
                                            data-confirm-title="{{ __('admin.mailinglists.delete_title')}}"
                                            data-confirm-message="{{ __('admin.mailinglists.delete_message')}}"
                                            data-btn-confirm="{{ __('admin.yes')}}"
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.delete') . ' ' . $mailinglist->subject}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
