@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formMailinglist';
    $submitId = 'submitMailinglist';
    $resetId = 'resetMailinglist';
    $exitId = 'exitMailinglist';
    $exitRoute = route('mailinglists.list');
    $sentDefault = $mailinglist->id == null ? 0 : $mailinglist->sent;

    $sentDate = empty($mailinglist->sent_date) ? null : $mailinglist->sent_date;
    $formattedSentDate = $sentDate == null ? '' : \Carbon\Carbon::parse($sentDate)->format('d/m/Y');

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'mailinglist.update' ? route($routeName, ['id' => $mailinglist->id]) : route($routeName);
@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/mailinglists.js') }}" ></script>
@endsection

@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => [
            ['route' => route('mailinglists.list'), 'label' => __('admin.mailinglists.list')],
        ],
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header form-header-title">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">{{$mailinglist->subject}}</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'url' => $route, 'class' => 'form-horizontal', 'files' => true)) }}

            <div class="form-row">

                <div class="form-group col-sm-12 col-md-6">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.mlgroups.groups')}}">
                            <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                        </div>
                        <select name="groups[]" id="groups" class="{{$classForm}} multiselect custom-select" multiple="multiple">

                            @foreach ($groups as $group)
                                @php($selected = '')
                                @if(!empty($mailinglist->groups))
                                    @foreach ($mailinglist->groups as $sg)
                                        @if($sg->pivot->mlgroup_id == $group->id)
                                            @php($selected = 'selected')
                                        @endif
                                    @endforeach
                                @endif
                                <option value="{{$group->id}}" {{$selected}}>{{$group->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                    <div class="form-group col-sm-12">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.mailinglists.subject'),
                            'name' => 'subject',
                            'value' => $mailinglist->subject,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.content'),
                            'name' => 'content',
                            'value' => $mailinglist->content,
                            'classes' => $classForm . ' text-editor',
                            'type' => 'textarea',
                        ])
                    </div>
                </div>


                 {{--Allegati --}}
                    <div class="form-group col-sm-12">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => !empty($mailinglist->attachment) ? $mailinglist->attachment : __('admin.uploader.choose_file'),
                            'toggleTitle' => __('admin.mailinglists.attachment'),
                            'name' => 'attachment',
                            'value' => $mailinglist->attachment,
                            'classes' => 'custom-file-input',
                            'type' => 'file',
                        ])
                        <input type="hidden" id="hid_attachment" name="hid_attachment" value="{{$mailinglist->attachment}}">
                    </div>


                    @if(!empty($mailinglist->attachment))
                        <div id="lbl2_attachment" class="form-row">
                            <div class="form-group col">
                                <button class="btn btn-danger clear-attachment ml-2" data-input-id="attachment" data-toggle="tooltip" data-placement="top" title="{{__('admin.clear_file')}}"><i class="{{config('settings.icons.delete')}}"></i></button>
                                <a href="{{asset('images/uploads/files/'.$mailinglist->attachment)}}" target="_blank">{{$mailinglist->attachment}}</a>
                            </div>
                        </div>
                    @endif

                <hr>

                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            'exitId' => $exitId,
                            'exitCustomClass' => '',
                            'exitText' =>  __('admin.exit'),
                            'exitRoute' =>  $exitRoute,
                            'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
