
@if(isset($success))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => $message])
@endif

@if(isset($error))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => $message])
@endif

 {{--In caso di errori lli stampo qui --}}
@if(isset($errors) && !empty($errors))
    @foreach($errors as $k => $r)
        <div class="alert alert-danger">{!!$r!!}</div><br>
    @endforeach
@endif