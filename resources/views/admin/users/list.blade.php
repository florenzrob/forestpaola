 {{-- 
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.users.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.users.title') }}</h2>
            <a href="{{route('user.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($users))
        <div class="table-responsive">
            <table class="table table-sm table-striped table-hover mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.users.fullname'],
                        ['class' => '', 'name' =>  'admin.users.is_admin'],
                        ['class' => '', 'name' =>  'admin.active'],

                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$user->id}}</th>
                            <td nowrap class="px-3 align-middle w-100"><a href="{{route('user.edit', ['id' => $user->id])}}" class="lead">{{$user->name}} {{$user->surname}}</a></td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('is_admin', $user->is_admin,  $user->is_admin == 1 ? true : false, ['id' => 'is_admin_' . $user->id, 'data-route' => route('user.update.is_admin', ['id' => $user->id]), 'class' => 'user-isadmin-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('active', $user->active,  $user->active == 1 ? true : false, ['id' => 'active_' . $user->id, 'data-route' => route('user.update.active', ['id' => $user->id]), 'class' => 'user-active-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('user.edit', ['id' => $user->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $user->name . ' ' .  $user->surname}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#" 
                                            data-route="{{route('user.delete', ['id' => $user->id])}}" 
                                            data-id="{{$user->id}}" 
                                            class="btn btn-light shadow-sm delete-item" 
                                            data-confirm-title="{{ __('admin.users.delete_title')}}"
                                            data-confirm-message="{{ __('admin.users.delete_message', ['name' => $user->name . ' ' . $user->surname])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}" 
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            title="{{__('admin.delete') . ' ' . $user->name . ' ' . $user->surname}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>