@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/users.js') }}" ></script>
@endsection

@section('dashboard-content')

    <div id="users_container">
        @include('admin.users.list')
    </div>

@endsection
