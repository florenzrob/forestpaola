@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formUser';
    $submitId = 'submitUser';
    $resetId = 'resetUser';
    $exitId = 'exitUser';
    $exitRoute = route('users.list');

    $isAdminDefault = $user->id == null ? 0 : $user->is_admin;
    $activeDefault = $user->id == null ? 1 : $user->active;

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'user.update' ? route($routeName, ['id' => $user->id]) : route($routeName);
@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/users.js') }}" ></script>
@endsection

@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => [
            ['route' => route('users.list'), 'label' => __('admin.users.list')],
        ],
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header form-header-title">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">{{$user->name}} {{$user->surname}}</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'url' => $route, 'class' => 'form-horizontal')) }}

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('is_admin', $isAdminDefault,  $isAdminDefault == 1 ? true : false, ['id' => 'is_admin', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('is_admin', __('admin.users.is_admin'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('active', $activeDefault,  $activeDefault == 1 ? true : false, ['id' => 'active', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('visible', __('admin.active'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>

                    <div class="form-group col-sm-12 col-md-3">
                        @if(!empty($user->id))
                            <a href="{{route('user.password.edit', ['id' => $user->id])}}"><i class="fas fa-key"></i> {{ __('admin.users.edit_password') }}</a>
                        @endif
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @if(!$user->picture == null)
                            <div class="coverimage">
                                <div class="img">
                                    <a href="#"
                                        data-route="{{route('user.picture.delete')}}"
                                        data-id="{{$user->id}}"
                                        data-filename="{{$user->picture}}"
                                        data-container-div="coverimage"
                                        class="icon btn btn-danger shadow-sm delete-image"
                                        data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                                        data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                                        data-btn-confirm="{{ __('admin.yes')}}"
                                        data-btn-dismiss="{{ __('admin.no')}}"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="{{__('admin.uploader.delete_title')}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    <a href="{{ asset('images/users/' . $user->picture) }}" data-lightbox="{{$user->picture}}" target="_blank"><img class="img-fluid" src="{{ asset('images/users/' . $user->picture) }}" title="{{__('admin.users.image_cover')}}" alt="{{__('admin.users.image_cover')}}" style="max-height:100px;"></a>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-sm-12 col-md-8">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.uploader.choose_file'),
                            'toggleTitle' => __('admin.users.image_cover'),
                            'name' => 'picture',
                            'value' => $user->picture,
                            'classes' => 'form-control-file',
                            'type' => 'file',
                        ])
                    </div>
                </div>

                <hr>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-2">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.title'),
                            'name' => 'title',
                            'value' => $user->title,
                            'classes' => $classForm,
                        ])
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.name'),
                            'name' => 'name',
                            'value' => $user->name,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    <div class="form-group col-sm-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.surname'),
                            'name' => 'surname',
                            'value' => $user->surname,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>

                    <div class="form-group col-sm-2">
                        <label for="gender">{{__('admin.users.gender')}}</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.users.gender')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>
                            <select name="gender" id="gender" class="custom-select">
                                @foreach ($enums as $option)
                                    @php($selected = $option == $user->gender ? 'selected' : '')
                                    <option value="{{$option}}" {{$selected}}>{{ucfirst($option)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.username'),
                            'name' => 'username',
                            'value' => $user->username,
                            'classes' => $classForm,
                        ])
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.email'),
                            'name' => 'email',
                            'value' => $user->email,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    <div class="form-group col-sm-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.email2'),
                            'name' => 'email2',
                            'value' => $user->email2,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                @if(empty($user->id))
                    @include('auth.passwords.password', ['inline' => true, 'prepend' => true, 'toggletitle' => __('auth.password')])
                @endif

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.address'),
                            'name' => 'address',
                            'value' => $user->address,
                            'classes' => $classForm,
                        ])
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.province'),
                            'name' => 'province',
                            'value' => $user->province,
                            'classes' => $classForm,
                        ])
                    </div>
                    <div class="form-group col-sm-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.city'),
                            'name' => 'city',
                            'value' => $user->city,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.cap'),
                            'name' => 'cap',
                            'value' => $user->cap,
                            'classes' => $classForm,
                        ])
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.nation'),
                            'name' => 'nation',
                            'value' => $user->nation,
                            'classes' => $classForm,
                        ])
                    </div>
                    <div class="form-group col-sm-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.nationality'),
                            'name' => 'nationality',
                            'value' => $user->nationality,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-3">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.phone'),
                            'name' => 'phone',
                            'value' => $user->phone,
                            'classes' => $classForm,
                        ])
                    </div>

                    <div class="form-group col-sm-12 col-md-3">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.phone2'),
                            'name' => 'phone2',
                            'value' => $user->phone2,
                            'classes' => $classForm,
                        ])
                    </div>
                    <div class="form-group col-sm-3">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.mobile'),
                            'name' => 'mobile',
                            'value' => $user->mobile,
                            'classes' => $classForm,
                        ])
                    </div>
                    <div class="form-group col-sm-3">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.mobile2'),
                            'name' => 'mobile2',
                            'value' => $user->mobile2,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.pec'),
                            'name' => 'pec',
                            'value' => $user->pec,
                            'classes' => $classForm,
                        ])
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.cf'),
                            'name' => 'cf',
                            'value' => $user->cf,
                            'classes' => $classForm,
                        ])
                    </div>
                    <div class="form-group col-sm-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.piva'),
                            'name' => 'piva',
                            'value' => $user->piva,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.users.note'),
                            'name' => 'note',
                            'value' => $user->note,
                            'classes' => $classForm . ' text-editor',
                            'type' => 'textarea',
                        ])
                    </div>
                </div>

                <hr>

                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            'exitId' => $exitId,
                            'exitCustomClass' => '',
                            'exitText' =>  __('admin.exit'),
                            'exitRoute' =>  $exitRoute,
                            'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
