@extends('layouts.admin')

@section('extra-script')
    @parent
    {{-- TYPE MODULE!!--}}
    <script type="module" src="{{ asset('js/admin/admin.js') }}" ></script>
@endsection

@section('content')
    
    <div class="row">
        {{-- Menu dashboard --}}
        <div class="col-12 col-md-4 col-lg-3 bg-light text-dark p-2">
            @include('admin.includes.navigation')
        </div>

        {{-- Main content --}}
        <div class="col-12 col-md-8 col-lg-9 dashboard_container mb-5">
            @yield('dashboard-content', View::make('admin.includes.dashboard-content'))
        </div>
    </div>
    

@endsection