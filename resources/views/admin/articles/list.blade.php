 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.articles.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.articles.title') }}</h2>
            <a href="{{route('article.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($articles))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1"
                data-ordering="true"
                data-searching="true"
                data-orderBy="0"
                data-orderDirection="asc"
                data-success="updateArticlesList"
                data-route="{{route('article.reorder')}}">

                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.parent'],
                        ['class' => '', 'name' =>  'admin.categories.category'],
                        ['class' => '', 'name' =>  'admin.title'],
                        ['class' => '', 'name' =>  'admin.tags.tags'],
                        //['class' => '', 'name' =>  'admin.icon'],
                        ['class' => 'text-center', 'name' =>  ['admin.articles.published', 'admin.date_from']],
                        ['class' => 'text-center', 'name' =>  'admin.date_to'],
                        ['class' => '', 'name' =>  'admin.show_sidebar_dx'],
                        ['class' => '', 'name' =>  'admin.is_private'],
                        ['class' => '', 'name' =>  'admin.visible'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($articles as $article)
                        <tr id="{{$article->id}}">
                            <th class="px-3 align-middle text-center orderme" scope="row">{{$article->sort}}</th>
                            <td nowrap class="px-3 align-middle">@if(!empty($article->parent)){{$article->parent->title}}@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle">@if(!empty($article->category)){{$article->category->title}}@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle"><a href="{{route('article.edit', ['id' => $article->id])}}" class="lead">{{$article->title}}</a></td>
                            <td class="px-3 align-middle w-100">
                                @if(!empty($article->tags))
                                    @foreach ($article->tags as $tag)
                                        @php($comma = $loop->index < count($article->tags)-1 ? ', ' : '')
                                        {{$tag->tag}}{{$comma}}
                                    @endforeach
                                @endif
                            </td>
                            {{--<td nowrap class="px-3 align-middle">{{$article->icon}}</td>--}}

                            <td nowrap class="px-3 align-middle">@if(!empty($article->publish_from)){{\Carbon\Carbon::parse($article->publish_from)->format('d-m-Y')}}@endif</td>
                            <td nowrap class="px-3 align-middle">@if(!empty($article->publish_to)){{\Carbon\Carbon::parse($article->publish_to)->format('d-m-Y')}}@endif</td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('show_sidebar_dx', $article->show_sidebar_dx,  $article->show_sidebar_dx == 1 ? true : false, ['id' => 'show_sidebar_dx' . $article->id, 'data-route' => route('article.update.show_sidebar_dx', ['id' => $article->id]), 'class' => 'article-sidebardx-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('is_private', $article->is_private,  $article->is_private == 1 ? true : false, ['id' => 'is_private_' . $article->id, 'data-route' => route('article.update.is_private', ['id' => $article->id]), 'class' => 'article-private-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('visible', $article->visible,  $article->visible == 1 ? true : false, ['id' => 'visible_' . $article->id, 'data-route' => route('article.update.visible', ['id' => $article->id]), 'class' => 'article-visible-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('article.edit', ['id' => $article->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $article->title}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#"
                                            data-route="{{route('article.delete', ['id' => $article->id])}}"
                                            data-id="{{$article->id}}"
                                            class="btn btn-light shadow-sm delete-item"
                                            data-confirm-title="{{ __('admin.articles.delete_title')}}"
                                            data-confirm-message="{{ __('admin.articles.delete_message', ['name' => $article->title])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}"
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.delete') . ' ' . $article->title}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
