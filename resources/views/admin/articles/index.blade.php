@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/articles.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="articles_container">
        @include('admin.articles.list')
    </div>

@endsection
