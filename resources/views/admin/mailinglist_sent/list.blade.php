 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.mailinglistsent.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.mailinglists.sent_report') }}</h2>
        </div>
    </div>


    @if(!empty($mailingsent))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.mailinglists.subject'],
                        ['class' => '', 'name' =>  'admin.mailinglists.recipients'],
                        ['class' => '', 'name' =>  'admin.mailinglists.attachment'],
                        ['class' => '', 'name' =>  'admin.mailinglists.sent_date'],
                        ['class' => '', 'name' =>  'admin.mailinglists.sent'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields, 'noactions' => true])
                <tbody>
                    @foreach ($mailingsent as $sent)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$sent->id}}</th>
                            <td class="px-3 align-middle">{{$sent->mailinglist->subject}}</td>
                            <td class="px-3 align-middle">
                                @if(!empty($sent->recipients))
                                    @php($recipients = explode(',', $sent->recipients))
                                    @foreach ($recipients as $recipient)
                                        <span class="badge bg-light text-dark">{{$loop->index+1 . '] ' . $recipient}}</span>
                                    @endforeach
                                @endif
                            </td>
                            <td nowrap class="px-3 align-middle">@if(!empty($sent->mailinglist->attachment))<a href="{{asset('images/uploads/files/'.$sent->mailinglist->attachment)}}" target="_blank">{{$sent->mailinglist->attachment}}</a>@endif</td>
                            <td nowrap class="px-3 align-middle">@if(!empty($sent->data_sent)){{\Carbon\Carbon::parse($sent->data_sent)->format('d-m-Y H:i:s')}}@endif</td>
                            <td nowrap class="px-3 align-middle">@if($sent->sent)<i class="fas fa-check text-success"></i>@else<i class="fas fa-times-circle text-warning"></i> @endif</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
