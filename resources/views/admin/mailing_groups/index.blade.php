@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/mlgroups.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="mlgroups_container">
        @include('admin.mailing_groups.list')
    </div>

@endsection
