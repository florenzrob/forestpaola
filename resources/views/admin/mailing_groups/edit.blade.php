@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formGroup';
    $submitId = 'submitGroup';
    $resetId = 'resetGroup';
    $exitId = 'exitGroup';
    $exitRoute = route('mlgroups.list');

    $visibleDefault = $mlgroup->id == null ? 1 : $mlgroup->visible;

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'mlgroup.update' ? route($routeName, ['id' => $mlgroup->id]) : route($routeName);
@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/mlgroups.js') }}" ></script>
@endsection

@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => [
            ['route' => route('mlgroups.list'), 'label' => __('admin.mlgroups.list')],
        ],
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header form-header-title">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">{{$mlgroup->name}}</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'url' => $route, 'class' => 'form-horizontal', 'files' => true)) }}

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('visible', $visibleDefault,  $visibleDefault == 1 ? true : false, ['id' => 'visible', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('visible', __('admin.visible'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.name'),
                            'name' => 'name',
                            'value' => $mlgroup->name,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.description'),
                            'name' => 'description',
                            'value' => $mlgroup->description,
                            'classes' => $classForm,

                        ])
                    </div>
                </div>

                <hr>

                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            'exitId' => $exitId,
                            'exitCustomClass' => '',
                            'exitText' =>  __('admin.exit'),
                            'exitRoute' =>  $exitRoute,
                            'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
