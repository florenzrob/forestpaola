 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.mlgroups.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.mlgroups.title') }}</h2>
            <a href="{{route('mlgroup.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($mlgroups))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.name'],
                        ['class' => '', 'name' =>  'admin.description'],
                        ['class' => '', 'name' =>  'admin.visible'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($mlgroups as $mlgroup)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$mlgroup->id}}</th>
                            <td nowrap class="px-3 align-middle"><a href="{{route('mlgroup.edit', ['id' => $mlgroup->id])}}" class="lead">{{$mlgroup->name}}</a></td>
                            <td nowrap class="px-3 align-middle w-100">{{$mlgroup->description}}</a></td>

                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('visible', $mlgroup->visible,  $mlgroup->visible == 1 ? true : false, ['id' => 'visible' . $mlgroup->id, 'data-route' => route('mlgroup.update.visible', ['id' => $mlgroup->id]), 'class' => 'mlgroup-visible-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('mlgroup.edit', ['id' => $mlgroup->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $mlgroup->name}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#"
                                            data-route="{{route('mlgroup.delete', ['id' => $mlgroup->id])}}"
                                            data-id="{{$mlgroup->id}}"
                                            class="btn btn-light shadow-sm delete-item"
                                            data-confirm-title="{{ __('admin.mlgroups.delete_title')}}"
                                            data-confirm-message="{{ __('admin.mlgroups.delete_message', ['name' => $mlgroup->name])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}"
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.delete') . ' ' . $mlgroup->name}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
