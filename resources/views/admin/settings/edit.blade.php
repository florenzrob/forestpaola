@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formSettings';
    $submitId = 'submitSettings';
    $resetId = 'resetSettings';

    if($routeName == 'settings.update'):
        $routeName = !empty($settings->id) ? $routeName : 'settings.store';
    endif;

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'settings.update' ? route($routeName, ['id' => $settings->id]) : route($routeName);
@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    {{-- TYPE MODULE!!--}}
    <script type="module" src="{{ asset('js/admin/settings.js') }}" ></script>
@endsection

@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => null,
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header ">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">Settings</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'url' => $route, 'class' => 'form-horizontal')) }}

            <div class="form-row mb-3">
                <div class="form-group col-sm-4 col-md-2">
                    <a id="upload_logo" data-input="company_logo" data-preview="logo_holder" class="btn btn-outline-secondary"><i class="far fa-image"></i> {{__('admin.uploader.choose_logo')}}</a>
                    {!! Form::hidden('company_logo', $settings->company_logo, ['id' => 'company_logo', 'class' => $classForm, 'data-bnt-delete' => 'logo_holder_btn', 'placeholder' => __('admin.uploader.choose_logo')])!!}

                    <p class="mt-3 mr-5">
                    <label for="company_logo_height">{{__('admin.logo_height')}}</label>
                    <span class="text-danger">{{ $errors->first('company_logo_height') }}</span>
                    {!! Form::text('company_logo_height', $settings->company_logo_height, ['id' => 'company_logo_height', 'class' => $classForm, 'placeholder' => __('admin.logo_height') . ' *'])!!}
                    </p>
                </div>

                <div class="form-group col-sm-8 col-md-10">
                    <div class="coverimage">
                        <div class="img">
                            @if(!$settings->company_logo == null)
                                <a href="#"
                                    data-route="{{route('settings.logo.delete')}}"
                                    data-id="{{$settings->id}}"
                                    data-filename="{{$settings->company_logo}}"
                                    data-container-div="coverimage"
                                    data-input-id="company_logo"
                                    class="icon btn btn-danger shadow-sm delete-image"
                                    data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                                    data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                                    data-btn-confirm="{{ __('admin.yes')}}"
                                    data-btn-dismiss="{{ __('admin.no')}}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="{{__('admin.uploader.delete_title')}}"><i class="{{config('settings.icons.delete')}}"></i></a>

                                    <a href="{{ asset('images/uploads/' . $settings->company_logo) }}" data-lightbox="{{$settings->company_logo}}" target="_blank">
                                        <img id="logo_holder" class="img-fluid" src="{{ asset('images/uploads/' . $settings->company_logo) }}" title="{{__('admin.image_cover')}}" alt="{{__('admin.image_cover')}}" style="max-height:100px;">
                                    </a>
                            @else
                                @include('admin.medias.image_holder', ['btnId' => 'logo_holder_btn', 'imgId' => 'logo_holder'])
                            @endif

                        </div>
                    </div>
                </div>

            </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12">
                        <label for="company_business_name">{{__('admin.business_name')}}</label>
                        <span class="text-danger">{{ $errors->first('company_business_name') }}</span>
                        {!! Form::text('company_business_name', $settings->company_business_name, ['id' => 'company_business_name', 'class' => $classForm, 'placeholder' => __('admin.business_name') . ' *'])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="company_title">{{__('admin.title')}}</label>
                        <span class="text-danger">{{ $errors->first('company_title') }}</span>
                        {!! Form::text('company_title', $settings->company_title, ['id' => 'company_title', 'class' => $classForm, 'placeholder' => __('admin.title')])!!}
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="company_subtitle">{{__('admin.subtitle')}}</label>
                        <span class="text-danger">{{ $errors->first('company_subtitle') }}</span>
                        {!! Form::text('company_subtitle', $settings->company_subtitle, ['id' => 'company_subtitle', 'class' => $classForm, 'placeholder' => __('admin.subtitle')])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12">
                        <label for="company_slogan">{{__('admin.slogan')}}</label>
                        <span class="text-danger">{{ $errors->first('company_slogan') }}</span>
                        {!! Form::text('company_slogan', $settings->company_slogan, ['id' => 'company_slogan', 'class' => $classForm . ' text-editor', 'placeholder' => __('admin.slogan')])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12">
                        <label for="company_abstract">{{__('admin.abstract')}}</label>
                        <span class="text-danger">{{ $errors->first('company_abstract') }}</span>
                        {!! Form::textarea('company_abstract', $settings->company_abstract, ['id' => 'company_abstract', 'class' => $classForm . ' text-editor', 'placeholder' => __('admin.abstract'), 'rows' => 3])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12">
                        <label for="company_description">{{__('admin.description')}}</label>
                        <span class="text-danger">{{ $errors->first('company_description') }}</span>
                        {!! Form::textarea('company_description', $settings->company_description, ['id' => 'company_description', 'class' => $classForm . ' text-editor', 'placeholder' => __('admin.description'), 'rows' => 3])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12">
                        <label for="company_logo">{{__('admin.pages.seo_title')}}</label>
                        <span class="text-danger">{{ $errors->first('company_seo_title') }}</span>
                        {!! Form::text('company_seo_title', $settings->company_seo_title, ['id' => 'company_seo_title', 'class' => $classForm, 'placeholder' => __('admin.pages.seo_title')])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12">
                        <label for="company_seo_content">{{__('admin.pages.seo_content')}}</label>
                        <span class="text-danger">{{ $errors->first('company_seo_content') }}</span>
                        {!! Form::textarea('company_seo_content', $settings->company_seo_content, ['id' => 'company_seo_content', 'class' => $classForm . ' text-editor', 'placeholder' => __('admin.pages.seo_content'), 'rows' => 3])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_phone1">{{__('admin.telephone1')}}</label>
                        <span class="text-danger">{{ $errors->first('company_phone1') }}</span>
                        {!! Form::text('company_phone1', $settings->company_phone1, ['id' => 'company_phone1', 'class' => $classForm, 'placeholder' => __('admin.telephone1')])!!}
                    </div>
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_phone2">{{__('admin.telephone2')}}</label>
                        <span class="text-danger">{{ $errors->first('company_phone1') }}</span>
                        {!! Form::text('company_phone2', $settings->company_phone2, ['id' => 'company_phone2', 'class' => $classForm, 'placeholder' => __('admin.telephone2')])!!}
                    </div>
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_mobile1">{{__('admin.mobile1')}}</label>
                        <span class="text-danger">{{ $errors->first('company_mobile1') }}</span>
                        {!! Form::text('company_mobile1', $settings->company_mobile1, ['id' => 'company_mobile1', 'class' => $classForm, 'placeholder' => __('admin.mobile1')])!!}
                    </div>
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_mobile2">{{__('admin.mobile2')}}</label>
                        <span class="text-danger">{{ $errors->first('company_mobile2') }}</span>
                        {!! Form::text('company_mobile2', $settings->company_mobile2, ['id' => 'company_mobile2', 'class' => $classForm, 'placeholder' => __('admin.mobile2')])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_email1">{{__('admin.email1')}}</label>
                        <span class="text-danger">{{ $errors->first('company_email1') }}</span>
                        {!! Form::text('company_email1', $settings->company_email1, ['id' => 'company_email1', 'class' => $classForm, 'placeholder' => __('admin.email1')])!!}
                    </div>
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_email2">{{__('admin.email2')}}</label>
                        <span class="text-danger">{{ $errors->first('company_email2') }}</span>
                        {!! Form::text('company_email2', $settings->company_email2, ['id' => 'company_email2', 'class' => $classForm , 'placeholder' => __('admin.email2')])!!}
                    </div>
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_pec">{{__('admin.pec')}}</label>
                        <span class="text-danger">{{ $errors->first('company_pec') }}</span>
                        {!! Form::text('company_pec', $settings->company_pec, ['id' => 'company_pec', 'class' => $classForm, 'placeholder' => __('admin.pec')])!!}
                    </div>
                    <div class="form-group col-sm-6 col-md-3">
                        <label for="company_fax">{{__('admin.fax')}}</label>
                        <span class="text-danger">{{ $errors->first('company_fax') }}</span>
                        {!! Form::text('company_fax', $settings->company_fax, ['id' => 'company_fax', 'class' => $classForm, 'placeholder' => __('admin.fax')])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-10">
                        <label for="company_address">{{__('admin.address')}}</label>
                        <span class="text-danger">{{ $errors->first('address') }}</span>
                        {!! Form::text('company_address', $settings->company_address, ['id' => 'company_address', 'class' => $classForm, 'placeholder' => __('admin.address')])!!}
                    </div>

                    <div class="form-group col-sm-2">
                        <label for="company_address_number">{{__('admin.address_number')}}</label>
                        <span class="text-danger">{{ $errors->first('company_address_number') }}</span>
                        {!! Form::text('company_address_number', $settings->company_address_number, ['id' => 'company_address_number', 'class' => $classForm, 'placeholder' => __('admin.address_number')])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="company_city">{{__('admin.city')}}</label>
                        <span class="text-danger">{{ $errors->first('company_city') }}</span>
                        {!! Form::text('company_city', $settings->company_city, ['id' => 'company_city', 'class' => $classForm, 'placeholder' => __('admin.city')])!!}
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        <label for="company_postal_code">{{__('admin.postal_code')}}</label>
                        <span class="text-danger">{{ $errors->first('company_postal_code') }}</span>
                        {!! Form::text('company_postal_code', $settings->company_postal_code, ['id' => 'company_postal_code', 'class' => $classForm, 'placeholder' => __('admin.postal_code')])!!}
                    </div>

                    <div class="form-group col-sm-12 col-md-2">
                        <label for="company_province">{{__('admin.province')}}</label>
                        <span class="text-danger">{{ $errors->first('company_province') }}</span>
                        {!! Form::text('company_province', $settings->company_province, ['id' => 'company_province', 'class' => $classForm, 'placeholder' => __('admin.province')])!!}
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="company_cf">{{__('admin.cf')}}</label>
                        <span class="text-danger">{{ $errors->first('company_cf') }}</span>
                        {!! Form::text('company_cf', $settings->company_cf, ['id' => 'company_cf', 'class' => $classForm, 'placeholder' => __('admin.cf') . '*'])!!}
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="company_vat">{{__('admin.vat')}}</label>
                        <span class="text-danger">{{ $errors->first('company_vat') }}</span>
                        {!! Form::text('company_vat', $settings->company_vat, ['id' => 'company_vat', 'class' => $classForm, 'placeholder' => __('admin.vat')])!!}
                    </div>
                </div>

                <hr>

                <div class="form-row mb-3">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="social_fb">{{__('admin.socials.fb')}}</label>
                        <span class="text-danger">{{ $errors->first('social_facebook') }}</span>
                        {!! Form::text('social_facebook', $settings->social_facebook, ['id' => 'social_facebook', 'class' => $classForm, 'placeholder' => __('admin.socials.fb')])!!}
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="social_fb">{{__('admin.socials.in')}}</label>
                        <span class="text-danger">{{ $errors->first('social_instagram') }}</span>
                        {!! Form::text('social_instagram', $settings->social_instagram, ['id' => 'social_instagram', 'class' => $classForm, 'placeholder' => __('admin.socials.in')])!!}
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="social_tw">{{__('admin.socials.tw')}}</label>
                        <span class="text-danger">{{ $errors->first('social_twitter') }}</span>
                        {!! Form::text('social_twitter', $settings->social_twitter, ['id' => 'social_twitter', 'class' => $classForm, 'placeholder' => __('admin.socials.tw')])!!}
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="social_in">{{__('admin.socials.ln')}}</label>
                        <span class="text-danger">{{ $errors->first('social_linkedin') }}</span>
                        {!! Form::text('social_linkedin', $settings->social_linkedin, ['id' => 'social_linkedin', 'class' => $classForm , 'placeholder' => __('admin.socials.ln')])!!}
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label for="social_gp">{{__('admin.socials.gp')}}</label>
                        <span class="text-danger">{{ $errors->first('social_googlepage') }}</span>
                        {!! Form::text('social_googlepage', $settings->social_googlepage, ['id' => 'social_googlepage', 'class' => $classForm, 'placeholder' => __('admin.socials.gp')])!!}
                    </div>
                </div>


                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            //'exitId' => $exitId,
                            //'exitCustomClass' => '',
                            //'exitText' =>  __('admin.exit'),
                            //'exitRoute' =>  $exitRoute,
                            //'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
