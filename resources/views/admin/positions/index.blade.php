@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/positions.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="positions_container">
        @include('admin.positions.list')
    </div>

@endsection
