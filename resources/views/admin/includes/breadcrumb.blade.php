<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="fas fa-tachometer-alt"></i> {{ __('admin.dashboard') }}</a></li>
        @if(!empty($items))
            @foreach ($items as $item)
                <li class="breadcrumb-item"><a href="{{$item['route']}}">{{$item['label']}}</a></li>
            @endforeach
        @endif
        <li class="breadcrumb-item active">{{$active}}</li>
    </ol>
</nav>