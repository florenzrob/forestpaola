<div class="card">
    <div class="card-header">
        <h5><i class="{{config('settings.icons.dashboard')}} mr-3"></i>{{__('admin.dashboard')}}</h5>
    </div>
    <div class="card-body">
        {{__('admin.dashboard_title')}}
    </div>
</div>
