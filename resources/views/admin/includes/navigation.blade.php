
@php($currentRoute = Route::currentRouteName())

<h2 class="mb-2 px-3 py-1"><i class="{{config('settings.icons.dashboard')}} mr-3"></i>{{ __('admin.menu') }}</h2>

@if(!empty($adminNavigation))
    @foreach($adminNavigation as $section)
        @if($section->visible)
            <div class="card mb-3 mx-2">
                <div class="card-header">
                    <h5><i class="{{$section->icon}} mr-3"></i> {{$section->title}}</h5>
                </div>

                @if(!empty($section->items))
                    <div class="list-group">
                        @foreach($section->items as $item)
                            @if($item->visible)
                                <a href="{{$item->route}}" class="list-group-item list-group-item-action @if($currentRoute == $item->routename) active @endif"><i class="{{$item->icon}} mr-3"></i> {{$item->label}}</a>
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        @endif
    @endforeach
@endif
