@if(!empty($images))
    @foreach($images as $img)
        <div class="col-sm-12 col-md-2 mb-3">
            <div class="img">
                {{--!! Form::hidden($img->id, $img->path, ['id' => $img->id, 'data-original-filename' => $img->path]) !!--}}
                <img id="{{str_slug($img)}}_preview" src="{{asset($img)}}" class="h100 border-0">
                {{--Delete --}}
                <a id="{{str_slug($img)}}_delete_btn"
                    href="#"
                    class="deleteicon btn btn-danger shadow-sm delete-image-gallery"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="{{ __('admin.uploader.remove_image')}}"
                    {{--data-id="{{str_slug($img)}}"--}}
                    data-fieldname="{{str_slug($img)}}"
                    data-imagetype={{$imagetypeId}}
                    data-imagepath={{$img}}
                    >
                    <i class="{{config('settings.icons.delete')}}"></i>
                </a>

                {{--Reload --}}
                <a id="{{str_slug($img)}}_reload_btn"
                    href="#"
                    class="reloadicon btn btn-primary shadow-sm reload-image d-none"
                    data-fieldname="{{str_slug($img)}}"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="{{ __('admin.uploader.reload_image')}}"
                    >
                    <i class="{{config('settings.icons.reload')}}"></i>
                </a>
            </div>
        </div>
    @endforeach
@endif
