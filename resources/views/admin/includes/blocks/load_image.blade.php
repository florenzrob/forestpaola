@php($value = $model->{$fieldName} !== null ? $model->{$fieldName} : null)
@php($mediaHeight = isset($fieldHeight) && !empty($fieldHeight) ?  $model->{$fieldHeight} : null /*config('settings.images.cover_height')*/)

@php($maxHeight = isset($maxHeight) ? $maxHeight : '200px')

@php($filePath = null) {{--Path dell'immagine preview --}}
@php($displayDeleteButton = 'd-none') {{--Visualizzo bottone delete true/false --}}
@php($displayReloadButton = 'd-none') {{--Visualizzo bottone reload true/false --}}
@php($displayImage = 'd-none') {{--Visualizzo preview immagine true/false --}}

@if(!$value == null)
    @php($filePath = asset($value))
    @php($displayDeleteButton = null)
    @php($displayImage = null)
@endif



<div class="form-row mb-3">

    {{--load image --}}
    <div class="form-group col-sm-6 col-md-3">
        <label id="lbl_{{$fieldName}}" for="{{$fieldName}}">{{$placeholder}}</label>
        <br>
        <a data-fieldname="{{$fieldName}}" class="upload_image btn btn-outline-secondary">
            <i class="{{config('settings.icons.add_round')}}"></i> {{__('admin.medias.add_image')}}
        </a>
        {!! Form::hidden($fieldName, $value, ['id' => $fieldName, 'data-original-filename' => $value]) !!}

        {{--image height --}}
        @if(isset($fieldHeight) && !empty($fieldHeight))
            <p class="mt-4 mb-0 p-0">
            @include('admin.includes.forms.input_text', [
                'placeholder' => __('admin.cover_height'),
                'name' => $fieldHeight,
                'value' => $mediaHeight,
                'classes' => $fieldHeight . ' ' . $classForm,
            ])
            </p>
        @endif

    </div>

    <div class="form-group col-sm-12 col-md-9">
        <div class="img">

            {{--Delete --}}
            <a id="{{$fieldName}}_delete_btn"
                href="#"
                class="deleteicon btn btn-danger shadow-sm delete-image {{$displayDeleteButton}}"
                data-fieldname="{{$fieldName}}"
                data-toggle="tooltip"
                data-placement="top"
                title="{{ __('admin.uploader.remove_image')}}"

                @if(!$value == null)
                    data-id="{{$model->id}}"
                    data-confirm-title="{{ __('admin.uploader.delete_title')}}"
                    data-confirm-message="{{ __('admin.uploader.delete_message')}}"
                    data-btn-confirm="{{ __('admin.yes')}}"
                    data-btn-dismiss="{{ __('admin.no')}}"

                @endif
                >
                <i class="{{config('settings.icons.delete')}}"></i>
            </a>

            {{--Reload --}}
            <a id="{{$fieldName}}_reload_btn"
                href="#"
                class="reloadicon btn btn-primary shadow-sm reload-image {{$displayReloadButton}}"
                data-fieldname="{{$fieldName}}"
                data-toggle="tooltip"
                data-placement="top"
                title="{{ __('admin.uploader.reload_image')}}"
                >
                <i class="{{config('settings.icons.reload')}}"></i>
            </a>

            {{--Preview --}}
            <img id="{{$fieldName}}_preview" class="img-fluid {{$displayImage}}" src="{{$filePath}}" style="max-height:{{$maxHeight}};" />

        </div>
    </div>


</div>
