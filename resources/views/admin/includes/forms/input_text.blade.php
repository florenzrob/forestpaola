@php($inputIcon = isset($icon) ? $icon : 'fas fa-info-circle') {{-- default fas fa-info-circle --}}
@php($inputToggle = isset($prepend) ? false : true) {{-- default true --}}
@php($inputPrepend = !isset($prepend) ? true : false) {{-- default true --}}
@php($inputAppend = !isset($append) ? false : true) {{-- default false --}}
@php($appendIcon = isset($appendIcon) ? $appendIcon : 'fas fa-info-circle') {{-- default fas fa-info-circle --}}
@php($inputId = isset($id) ? $id : $name)
@php($inputToggleTitle = isset($toggleTitle) ? $toggleTitle : $placeholder)
@php($inputRequired = isset($required) ? true : false) {{-- default false --}}
@php($inputPlaceholder = !empty($placeholder) ? $placeholder : '')
@php($inputType = isset($type) ? $type : 'text') {{-- default text --}}

@if($inputRequired)
    @php($inputToggleTitle = $inputToggleTitle . ' *')
    @php($inputPlaceholder = $inputPlaceholder . ' *')
@endif


@if($inputType == 'file')
    <div class="input-group mb-2">
        @if($inputPrepend)
            <div class="input-group-prepend" @if($inputToggle)data-toggle="tooltip" data-placement="top" title="{{$inputToggleTitle}}"@endif>
                <div class="input-group-text"><i class="{{$inputIcon}}"></i></div>
            </div>
        @endif
        <div class="custom-file">
            {!! Form::file($name, ['id' => $inputId, 'class' => $classes])!!}
            <label id="lbl_{{$inputId}}" class="custom-file-label" for="{{$inputId}}">{{$inputPlaceholder}}</label>
        </div>
    </div>

@else


    <label id="lbl_{{$inputId}}" for="{{$inputId}}">{{$inputPlaceholder}}</label>
    <div class="input-group mb-2">
        @if($inputPrepend && $inputType != 'textarea')
            <div class="input-group-prepend" @if($inputToggle)data-toggle="tooltip" data-placement="top" title="{{$inputToggleTitle}}"@endif>
                <div class="input-group-text"><i class="{{$inputIcon}}"></i></div>
            </div>
        @endif

        @switch($inputType)
                    @case('text')
                {!! Form::text($name, $value, ['id' => $inputId, 'class' => $classes, 'placeholder' => $inputPlaceholder])!!}
                @break
            @case('textarea')
                {{-- Se è un text-editor mostro la label-- }}
                @if(strpos($classes, 'text-editor') !== false)
                    <label id="lbl_{{$inputId}}" for="{{$inputId}}">{{$inputPlaceholder}}</label>
                @endif
                ----}}
                {!! Form::textarea($name, $value, ['id' => $inputId, 'class' => $classes, 'rows' => 3, 'placeholder' => $inputPlaceholder])!!}

                {{-- Se è un text-editor, aggiungo div per la gestione dell'upload dei files --}}
                @if(strpos($classes, 'text-editor') !== false)
                    <div class="tinyuploadmanager" data-upload-url="{{route('filemanager-show')}}"></div>
                @endif

                @break
            @case('file')
                <div class="custom-file">
                    {!! Form::file($name, ['id' => $inputId, 'class' => $classes])!!}
                    <label class="custom-file-label" for="{{$name}}">{{$placeholder}}</label>
                </div>
                @break
        @endswitch
        @if($inputAppend)
            <div class="input-group-append" @if($inputToggle)data-toggle="tooltip" data-placement="top" title="{{$inputToggleTitle}}"@endif>
                <div class="input-group-text"><i class="{{$appendIcon}}"></i></div>
            </div>
        @endif

    </div>
    <span class="text-danger">{{ $errors->first($name) }}</span>
@endif
