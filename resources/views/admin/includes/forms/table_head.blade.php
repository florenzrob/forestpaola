<thead>
    <tr>
        @foreach($tableFields as $tableField)
            <th @if(isset($tableField['colspan']))colspan="{{$tableField['colspan']}}" @endif nowrap class="px-3 py-3 {{$tableField['class']}}">
                @if(is_array($tableField['name']))
                    @foreach ($tableField['name'] as $name)
                        {{__($name)}}        
                    @endforeach
                @else
                    {{__($tableField['name'])}}
                @endif
            </th>
        @endforeach
        
        @if(!isset($noactions))
            <th nowrap class="px-3 py-3 align-middle text-center">{{ __('admin.actions') }}</th>
        @endif
    </tr>
</thead>