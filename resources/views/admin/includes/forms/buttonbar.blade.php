<div class="form-group">
    <button
        id="{{$submitId}}"
        type="submit"
        class="btn btn-primary {{$submitCustomClass}}"
        data-formid="{{$formId}}">
        @if(!empty($submitIcon)){!!$submitIcon!!} @endif{{$submitText}}
    </button>
    <button
        id="{{$resetId}}"
        type="reset"
        class="btn btn-warning {{$resetCustomClass}}"
        data-formid="{{$formId}}">
        @if(!empty($resetIcon)){!!$resetIcon!!} @endif{{$resetText}}
    </button>

    @if(isset($exitId))
        <a
            id="{{$exitId}}"
            href="{{$exitRoute}}"
            class="btn btn-danger {{$exitCustomClass}}"
            >
            @if(!empty($exitIcon)){!!$exitIcon!!} @endif{{$exitText}}
        </a>
    @endif
</div>
