<div class="fixed-top border-bottom pt-2 top-bar bg-dark text-white py-2 shadow-sm">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-sm-6 pl-5">

                <a href="{{route('home')}}" target="_blank" class="btn btn-outline-light mr-5"><i class="fas fa-home"></i> Frontend</a>
                <i class="{{config('settings.admin.dashboard.icon')}}"></i> {{config('settings.admin.dashboard.title')}}

            </div>

            <div class="col-sm-6 pr-5 text-right">
                @if(Auth::user())
                    <button type="button" class="btn btn-sm btn-secondary dropdown-toggle mb-2"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @php($greeting = Auth::user()->gender == 'F' ? 'benvenuta' : 'benvenuto')
                        <small>{{ __('admin.users.' . $greeting)}} <strong>{{ Auth::user()->name . ' ' . Auth::user()->surname }}</strong></small>
                    </button>
                    <div class="dropdown-menu">
                        {{-- <a class="dropdown-item d-flex justify-content-between" href="#"><i class="fal fa-user"></i> Profilo</a> --}}
                        {{-- Need to be post --}}
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>


