@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formSubscriber';
    $submitId = 'submitSubscriber';
    $resetId = 'resetSubscriber';
    $exitId = 'exitSubscriber';
    $exitRoute = route('subscribers.list');

    $activeDefault = $subscriber->id == null ? 1 : $subscriber->active;

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'subscriber.update' ? route($routeName, ['id' => $subscriber->id]) : route($routeName);
@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/subscribers.js') }}" ></script>
@endsection
@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => [
            ['route' => route('subscribers.list'), 'label' => __('admin.subscribers.list')],
        ],
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header form-header-title">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">{{$subscriber->name . ' ' . $subscriber->surname}}</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'url' => $route, 'class' => 'form-horizontal', 'files' => true)) }}

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('active', $activeDefault,  $activeDefault == 1 ? true : false, ['id' => 'active', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('active', __('admin.active'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>

                </div>


                <div class="form-row">

                    <div class="form-group col-sm-12 col-md-6">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.mlgroups.title')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>
                            <select name="groups[]" id="groups" class="{{$classForm}} multiselect custom-select" multiple="multiple">

                                @foreach ($groups as $group)
                                    @php($selected = '')
                                    @if(!empty($subscriber->groups))
                                        @foreach ($subscriber->groups as $sg)
                                            @if($sg->pivot->mlgroup_id == $group->id)
                                                @php($selected = 'selected')
                                            @endif
                                        @endforeach
                                    @endif
                                    <option value="{{$group->id}}" {{$selected}}>{{$group->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.name'),
                            'name' => 'name',
                            'value' => $subscriber->name,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.surname'),
                            'name' => 'surname',
                            'value' => $subscriber->surname,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.email1'),
                            'name' => 'email',
                            'value' => $subscriber->email,
                            'classes' => $classForm,
                        ])
                    </div>
                </div>

                <hr>

                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            'exitId' => $exitId,
                            'exitCustomClass' => '',
                            'exitText' =>  __('admin.exit'),
                            'exitRoute' =>  $exitRoute,
                            'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
