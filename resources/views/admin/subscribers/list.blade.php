 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.subscribers.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.subscribers.title') }}</h2>
            <a href="{{route('subscriber.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($subscribers))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.name'],
                        ['class' => '', 'name' =>  'admin.surname'],
                        ['class' => '', 'name' =>  'admin.email1'],
                        ['class' => '', 'name' =>  'admin.mlgroups.groups'],
                        ['class' => '', 'name' =>  'admin.active'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($subscribers as $subscriber)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$subscriber->id}}</th>
                            <td nowrap class="px-3 align-middle"><a href="{{route('subscriber.edit', ['id' => $subscriber->id])}}" class="lead">{{$subscriber->name}}</a></td>
                            <td nowrap class="px-3 align-middle"><a href="{{route('subscriber.edit', ['id' => $subscriber->id])}}" class="lead">{{$subscriber->surname}}</a></td>
                            <td nowrap class="px-3 align-middle">{{$subscriber->email}}</a></td>

                            <td class="px-3 align-middle w-100">
                                @if(!empty($subscriber->groups))
                                    @foreach ($subscriber->groups as $group)
                                        @php($comma = $loop->index < count($subscriber->groups)-1 ? ', ' : '')
                                        {{$group->name}}{{$comma}}
                                    @endforeach
                                @endif
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('active', $subscriber->active,  $subscriber->active == 1 ? true : false, ['id' => 'active' . $subscriber->id, 'data-route' => route('subscriber.update.active', ['id' => $subscriber->id]), 'class' => 'subscriber-active-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('subscriber.edit', ['id' => $subscriber->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $subscriber->name . ' ' .$subscriber->surname}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#"
                                            data-route="{{route('subscriber.delete', ['id' => $subscriber->id])}}"
                                            data-id="{{$subscriber->id}}"
                                            class="btn btn-light shadow-sm delete-item"
                                            data-confirm-title="{{ __('admin.subscribers.delete_title')}}"
                                            data-confirm-message="{{ __('admin.subscribers.delete_message', ['name' => $subscriber->name . ' ' . $subscriber->surname])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}"
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.delete') . ' ' . $subscriber->name . ' ' . $subscriber->surname}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
