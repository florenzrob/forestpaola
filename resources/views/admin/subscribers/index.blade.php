@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/subscribers.js') }}" ></script>
@endsection

@section('dashboard-content')

    <div id="subscribers_container">
        @include('admin.subscribers.list')
    </div>

@endsection
