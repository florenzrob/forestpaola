 {{-- 
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.templates.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.templates.title') }}</h2>
            <a href="{{route('template.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($templates))
        <div class="table-responsive">
            <table class="table table-sm table-striped table-hover mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.title'],
                        ['class' => '', 'name' =>  'admin.templates.name'],
                        ['class' => '', 'name' =>  'admin.description'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($templates as $template)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$template->id}}</th>
                            <td nowrap class="px-3 align-middle"><a href="{{route('template.edit', ['id' => $template->id])}}" class="lead">{{$template->title}}</a></td>
                            <td nowrap class="px-3 align-middle">{{$template->template_name}}</td>
                            <td nowrap class="px-3 align-middle w-100">{{$template->description}}</td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('template.edit', ['id' => $template->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $template->title}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#" 
                                            data-route="{{route('template.delete', ['id' => $template->id])}}" 
                                            data-id="{{$template->id}}" 
                                            class="btn btn-light shadow-sm delete-item" 
                                            data-confirm-title="{{ __('admin.templates.delete_title')}}"
                                            data-confirm-message="{{ __('admin.templates.delete_message', ['name' => $template->title])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}" 
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            title="{{__('admin.delete') . ' ' . $template->title}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>