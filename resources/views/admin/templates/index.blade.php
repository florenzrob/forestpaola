@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/templates.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="templates_container">
        @include('admin.templates.list')
    </div>

@endsection
