 {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.categories.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.categories.title') }}</h2>
            <a href="{{route('category.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($categories))
        <div class="card-body table-responsive">
            <table class="table table-striped table-hover table-paginate mt-1"
                data-ordering="true"
                data-searching="true"
                data-orderBy="0"
                data-orderDirection="asc"
                data-success="updateCategoriesList"
                data-route="{{route('category.reorder')}}">

                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.parent'],
                        ['class' => '', 'name' =>  'admin.title'],
                        ['class' => '', 'name' =>  'admin.pages.pages'],
                        //['class' => '', 'name' =>  'admin.icon'],
                        ['class' => '', 'name' =>  'admin.show_sidebar_dx'],
                        ['class' => '', 'name' =>  'admin.is_private'],
                        ['class' => '', 'name' =>  'admin.visible'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($categories as $category)
                        <tr id="{{$category->id}}">
                            <th class="px-3 align-middle text-center orderme" scope="row">{{$category->sort}}</th>
                            <td nowrap class="px-3 align-middle">@if(!empty($category->parent))<span class="badge border border-warning p-2">{{$category->parent->title}}</span>@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle"><a href="{{route('category.edit', ['id' => $category->id])}}" class="lead">{{$category->title}}</a></td>
                            <td class="px-3 w-100">
                                @if(!empty($category->pages))
                                    @foreach ($category->pages as $cp)
                                        <span class="badge border border-success p-2">{{$cp->title}}</span>
                                    @endforeach
                                @endif
                            </td>
                            {{--<td nowrap class="px-3 align-middle">{{$category->icon}}</td>--}}
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('show_sidebar_dx', $category->show_sidebar_dx,  $category->show_sidebar_dx == 1 ? true : false, ['id' => 'show_sidebar_dx' . $category->id, 'data-route' => route('category.update.show_sidebar_dx', ['id' => $category->id]), 'class' => 'category-sidebardx-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('is_private', $category->is_private,  $category->is_private == 1 ? true : false, ['id' => 'is_private_' . $category->id, 'data-route' => route('category.update.is_private', ['id' => $category->id]), 'class' => 'category-private-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('visible', $category->visible,  $category->visible == 1 ? true : false, ['id' => 'visible_' . $category->id, 'data-route' => route('category.update.visible', ['id' => $category->id]), 'class' => 'category-visible-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('category.edit', ['id' => $category->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $category->title}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#"
                                            data-route="{{route('category.delete', ['id' => $category->id])}}"
                                            data-id="{{$category->id}}"
                                            class="btn btn-light shadow-sm delete-item"
                                            data-confirm-title="{{ __('admin.categories.delete_title')}}"
                                            data-confirm-message="{{ __('admin.categories.delete_message', ['name' => $category->title])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}"
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{__('admin.delete') . ' ' . $category->title}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
