@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/categories.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="categories_container">
        @include('admin.categories.list')
    </div>

@endsection
