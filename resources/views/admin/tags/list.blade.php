 {{-- 
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.tags.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.tags.title') }}</h2>
            <a href="{{route('tag.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($tags))
        <div class="table-responsive">
            <table class="table table-sm table-striped table-hover mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.tags.tag'],
                        ['class' => '', 'name' =>  'admin.tags.system'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($tags as $tag)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$tag->id}}</th>
                            <td nowrap class="px-3 align-middle w-100"><a href="{{route('tag.edit', ['id' => $tag->id])}}" class="lead">{{$tag->tag}}</a></td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('system', $tag->system,  $tag->system == 1 ? true : false, ['id' => 'system_' . $tag->id, 'data-route' => route('tag.update.system', ['id' => $tag->id]), 'class' => 'tag-system-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('tag.edit', ['id' => $tag->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $tag->tag}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#" 
                                            data-route="{{route('tag.delete', ['id' => $tag->id])}}" 
                                            data-id="{{$tag->id}}" 
                                            class="btn btn-light shadow-sm delete-item" 
                                            data-confirm-title="{{ __('admin.tags.delete_title')}}"
                                            data-confirm-message="{{ __('admin.tags.delete_message', ['name' => $tag->tag])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}" 
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            title="{{__('admin.delete') . ' ' . $tag->tag}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>