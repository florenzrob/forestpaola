@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/tags.js') }}" ></script>
@endsection


@section('dashboard-content')

    <div id="tags_container">
        @include('admin.tags.list')
    </div>

@endsection
