 {{-- 
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.menus.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.menus.title') }}</h2>
            <a href="{{route('menu.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($menus))
        <div class="table-responsive">
            <table class="table table-sm table-striped table-hover mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.parent'],
                        ['class' => '', 'name' =>  'admin.name'],
                        ['class' => '', 'name' =>  'admin.visible'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($menus as $menu)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$menu->id}}</th>
                            <td nowrap class="px-3 align-middle">@if(!empty($menu->parent)){{$menu->parent->name}}@else<div class="text-center">-</div>@endif</td>
                            <td nowrap class="px-3 align-middle w-100"><a href="{{route('menu.edit', ['id' => $menu->id])}}" class="lead">{{$menu->name}}</a></td>
                            <td nowrap class="px-3 align-middle">
                                <label class="custom-control border-switch border-switch-green">
                                    {!! Form::checkbox('visible', $menu->visible,  $menu->visible == 1 ? true : false, ['id' => 'visible' . $menu->id, 'data-route' => route('menu.update.visible', ['id' => $menu->id]), 'class' => 'menu-visible-switch border-switch-control-input'])!!}
                                    <span class="border-switch-control-indicator"></span>
                                </label>
                            </td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('menu.edit', ['id' => $menu->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $menu->name}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#" 
                                            data-route="{{route('menu.delete', ['id' => $menu->id])}}" 
                                            data-id="{{$menu->id}}" 
                                            class="btn btn-light shadow-sm delete-item" 
                                            data-confirm-title="{{ __('admin.menus.delete_title')}}"
                                            data-confirm-message="{{ __('admin.menus.delete_message', ['name' => $menu->name])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}" 
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            title="{{__('admin.delete') . ' ' . $menu->name}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>