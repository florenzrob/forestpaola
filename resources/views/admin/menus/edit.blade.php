@php
    //Variabili globali
    $classForm = config('settings.forms.classForm');
    $formId = 'formMenu';
    $submitId = 'submitMenu';
    $resetId = 'resetMenu';
    $exitId = 'exitMenu';
    $exitRoute = route('menus.list');

    $visibleDefault = $menu->id == null ? 1 : $menu->visible;

    $actionIcon = $breadcrumb_action == 'action_edit' ? config('settings.icons.edit') : config('settings.icons.add');
    $route = $routeName == 'menu.update' ? route($routeName, ['id' => $menu->id]) : route($routeName);

@endphp

@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/menus.js') }}" ></script>
@endsection

@section('dashboard-content')

    {{--
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'items' => [
            ['route' => route('menus.list'), 'label' => __('admin.menus.list')],
        ],
        'active' => __('admin.' . $breadcrumb_action)
    ])

    {{-- Alerts --}}
    @if(session('success'))
        @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
    @endif
    @if(session('error'))
        @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
    @endif

    {{-- Form --}}
    <div class="card bg-light mb-3">
        <div class="card-header form-header-title">
            <i class="{{$actionIcon}}"></i> {{ __('admin.' . $breadcrumb_action)}} <span class="title">{{$menu->name}}</span>
        </div>
        <div class="card-body">

            {{Form::open(array('id' => $formId, 'url' => $route, 'class' => 'form-horizontal')) }}

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-2">
                        <label class="custom-control border-switch border-switch-green">
                            {!! Form::checkbox('visible', $visibleDefault,  $visibleDefault == 1 ? true : false, ['id' => 'visible', 'class' => 'border-switch-control-input'])!!}
                            <span class="border-switch-control-indicator"></span>
                            {!! Form::label('visible', __('admin.visible'), ['class' => 'border-switch-control-description']) !!}
                        </label>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        <label id="lbl_parent_id" for="parent_id">{{__('admin.parent')}}</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.parent')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>
                            <select name="parent_id" id="parent_id" class="custom-select">
                                <option selected value="">----</option>
                                @foreach ($menus as $parent)
                                    @php($selected = $menu->parent_id == $parent->id ? 'selected' : '')
                                    <option value="{{$parent->id}}" {{$selected}}>{{$parent->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        @include('admin.includes.forms.input_text', [
                            'placeholder' => __('admin.name'),
                            'name' => 'name',
                            'value' => $menu->name,
                            'classes' => $classForm,
                            'required' => true,
                        ])
                    </div>

                    <div class="form-group col-sm-12 col-md-4">
                        <label id="lbl_parent_id" for="parent_id">{{__('admin.pages.pages_menu')}}</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="{{__('admin.pages.pages_menu')}}">
                                <div class="input-group-text"><i class="fas fa-info-circle"></i></div>
                            </div>
                            <select name="pages[]" id="pages" class="{{$classForm}} multiselect custom-select" multiple="multiple" style="overflow:visible;">

                                @foreach ($pages as $page)
                                    @php($selected = '')
                                    @if(!empty($menu->pages))
                                        @foreach ($menu->pages as $mp)
                                            @if($mp->pivot->page_id == $page->id)
                                                @php($selected = 'selected')
                                            @endif
                                        @endforeach
                                    @endif
                                    <option class="pr-5" value="{{$page->id}}" {{$selected}}>{{$page->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row text-center">
                    <div class="col button-section">
                        {{--
                        Include form buttons:
                        Parameters:
                            - formId:               id della form,
                            - submitId:             id bottone invio,
                            - submitCustomClass:    opz. Classi per il bottone invio,
                            - submitText:           testo bottone invio,
                            - submitIcon:           opz. Icona bottone invio,
                            - resetId:              id bottone reset,
                            - resetCustomClass:     opz. Classi per il bottone reset,
                            - resetText:            testo bottone reset,
                            - resetIcon:            opz. Icona bottone reset,
                        --}}
                        @include('admin.includes.forms.buttonbar', [
                            'formId' => $formId,
                            'submitId' => $submitId,
                            'submitCustomClass' => 'form-send',
                            'submitText' =>  __('admin.save'),
                            'submitIcon' => '<i class="' . config('settings.icons.save') .' mr-2"></i>',
                            'resetId' => $resetId,
                            'resetCustomClass' => '',
                            'resetText' =>  __('admin.undo'),
                            'resetIcon' => '<i class="' . config('settings.icons.undo') .' mr-2"></i>',
                            'exitId' => $exitId,
                            'exitCustomClass' => '',
                            'exitText' =>  __('admin.exit'),
                            'exitRoute' =>  $exitRoute,
                            'exitIcon' => '<i class="' . config('settings.icons.exit') .' mr-2"></i>',
                        ])
                    </div>
                </div>

            {{ Form::close() }}
        </div>
    </div>

@endsection
