@extends('admin.dashboard')

@section('extra-script')
    @parent
    <script type="text/javascript" src="{{ asset('js/admin/menus.js') }}" ></script>
@endsection

@section('dashboard-content')

    <div id="menus_container">
        @include('admin.menus.list')
    </div>

@endsection
