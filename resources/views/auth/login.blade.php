@extends('layouts.base')

@section('content')

    <div class="container my-5">
        <div class="row justify-content-sm-center">
            <div class="col-sm-8 col-md-6 col-lg-4 my-5">
                {{--
                Users: {{count($users)}}
                <ul>
                @foreach ($users as $user)
                    <li>{{$user->username}} - pwd: {{bcrypt($user->password)}}</li>
                @endforeach
                </ul>
                --}}

                @if (!Auth::check())
                    <div class="card">
                        <div class="card-header"><h3><i class="fas fa-key"></i> Login</h3></div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'login','id' => 'loginForm']) !!}
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" title="Username"><i class="fas fa-user-tie"></i></span>
                                        </div>
                                        {!! Form::text('username', null, [
                                            'class' => 'form-control',
                                            'placeholder' => __('auth.username') . ' *',
                                            ])
                                        !!}
                                        <br>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                </div>

                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" title="Password"><i class="fas fa-unlock-alt"></i></span>
                                        </div>
                                        {!! Form::password('password', [
                                            'data-toggle' => 'password',
                                            'class'=>'form-control' ,
                                            'placeholder' => __('auth.password') . ' *',
                                            ])
                                        !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('password') }}</span>

                                </div>

                                {{--
                                <div class="form-check {{ $errors->has('remember') ? 'has-error' : '' }}">
                                    {!! Form::checkbox('remember', 'value', false, [
                                        'class' => 'form-check-input',
                                        ])
                                    !!}
                                    <label for="'privacy">&nbsp; {{__('auth.remember') }}</label>
                                    <span class="text-danger">{{ $errors->first('remember') }}</span>
                                </div>
                                --}}
                                <div class="form-actions">
                                    {!! Form::submit(__('auth.login'), ['id' => 'btn_loginform', 'class' => 'btn btn-secondary btn-block py-3']) !!}
                                </div>

                            {!! Form::close() !!}

                            <div class="text-center pt-2">
                                <a class="btn btn-link text-secondary" href="{{ route('password.request') }}">{{__('auth.forgot_password') }}?</a>
                            </div>
                        </div>
                    </div>
                @else
                    Sei registrato
                @endif
            </div>
        </div>
    </div>
@endsection
