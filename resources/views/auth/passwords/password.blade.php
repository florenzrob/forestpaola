@php($inline = isset($inline) ? true : false) {{-- default false --}}
@php($prepend = isset($prepend) ? true : false) {{-- default false --}}
@php($toggle = isset($prepend) ? true : false) {{-- default false --}}
@php($icon = isset($icon) ? $icon : 'fas fa-info-circle') {{-- default fas fa-info-circle --}}
@php($toggleTitle = isset($toggleTitle) ? $toggleTitle : __('auth.password'))
@php($classForm = config('settings.forms.classForm'))

@php($toggleTitle = $toggleTitle . ' *')
@php($toggleTitle2 = __('auth.confirm_password') . ' *')

@if($inline)

    <div class="form-row">
        <div class="form-group col-sm-12 col-md-4">
            <div class="input-group mb-2">
                @if($prepend)
                    <div class="input-group-prepend" @if($toggle)data-toggle="tooltip" data-placement="top" title="{{$toggleTitle}}"@endif>
                        <div class="input-group-text"><i class="{{$icon}}"></i></div>
                    </div>
                @endif   
                {!! Form::text('password', null, ['id' => 'password', 'class' => $classForm, 'placeholder' => $toggleTitle])!!}
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group col-sm-12 col-md-4">
            <div class="input-group mb-2">
                @if($prepend)
                    <div class="input-group-prepend" @if($toggle)data-toggle="tooltip" data-placement="top" title="{{ $toggleTitle2 }}"@endif>
                        <div class="input-group-text"><i class="{{$icon}}"></i></div>
                    </div>
                @endif   
                {!! Form::text('password_confirmation', null, ['id' => 'password-confirm', 'class' => $classForm, 'placeholder' => $toggleTitle2])!!}
            </div>
        </div>
    </div>

@else

    <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('auth.new_password') }}</label>
        
            <div class="col-md-6">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        
        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('auth.confirm_password') }}</label>
        
            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>
@endif