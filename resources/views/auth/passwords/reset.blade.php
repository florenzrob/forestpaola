@extends('layouts.base')

@section('content')
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header"><h3><i class="fas fa-key"></i> {{ __('Reset Password') }}</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            {{ $errors->has('email') ? 'has-error' : '' }}
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" title="{{ __('E-Mail Address') }}"><i class="far fa-envelope"></i></span>
                                </div>
                                {!! Form::email('email', $email ?? old('email'), [
                                    'class' => 'form-control',
                                    'required autofocus',
                                    'placeholder' => __('E-Mail Address') . ' *',
                                    ]) 
                                !!}
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ $errors->has('password') ? 'has-error' : '' }}
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" title="Password"><i class="fas fa-unlock-alt"></i></span>
                                </div>
                                {!! Form::password('password', [
                                    'id' => 'password',
                                    'name' => 'password',
                                    'data-toggle' => 'password',
                                    'required',
                                    'class'=>'form-control' ,
                                    'placeholder' => __('auth.password') . ' *',
                                    ]) 
                                !!}
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                </div>
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ $errors->has('password-confirm') ? 'has-error' : '' }}
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" title="Password Confirm"><i class="fas fa-unlock-alt"></i></span>
                                </div>
                                {!! Form::password('password-confirm', [
                                    'id' => 'password-confirm',
                                    'name' => 'password_confirmation',
                                    'data-toggle' => 'password',
                                    'required',
                                    'class'=>'form-control' ,
                                    'placeholder' => __('Confirm Password') . ' *',
                                    ]) 
                                !!}
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                </div>
                                <span class="text-danger">{{ $errors->first('password-confirm') }}</span>
                            </div>
                        </div>

                        <div class="form-actions">
                            {!! Form::submit(__('Reset Password'), ['id' => 'btn_loginform', 'class' => 'btn btn-secondary btn-block py-3']) !!}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
