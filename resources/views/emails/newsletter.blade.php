<!doctype html>
<html lang="it">
	{{-- Head --}}
    @include('emails.includes.head')

	<body style="border:0; margin:0; padding:20px;">
        <table border="0" cellspacing="0" cellpadding="20" width="100%" style="margin:0 auto;">

            {{-- Logo --}}
			@include('emails.includes.logo')
			{{-- Body --}}
			<tr>
				<td colspan="2">{!! $fields['mailinglist']->content !!}</td>
			</tr>
            @if(!empty($fields['path']))
                <tr>
                    <td colspan="2" style="padding-top:10px">Se non vedi l'allegato clicca <a href="{{asset($fields['path'])}}" target="_blank">qui</a></td>
                </tr>
            @endif
            <tr>
                <td class="endbody"></td>
            </tr>
            {{-- Footer --}}
			@include('emails.includes.footer')

		</table>

        <div class="unsubscribe">Ricevi questa mail perchè sei iscritto alla mailinglist di {{ $fields['settings']->company_business_name }}.
            <br>Se vuoi cancellarti <a href="{{route('mailinglist.unsubscribe')}}" target="_blank">clicca qui</a></small>
        </div>

    </body>
</html>
