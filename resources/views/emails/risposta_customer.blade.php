<!doctype html>
<html lang="it">
	{{-- Head --}}
    @include('emails.includes.head')

	<body style="border:0; margin:0; padding:20px;">
        <table border="0" cellspacing="0" cellpadding="20" width="100%" style="margin:0 auto;">
			{{-- Logo --}}
			@include('emails.includes.logo')
			{{-- Body --}}
			<tr>
				<td colspan="2">
                    {{__('messages.saluto')}} {{ $fields['name'] }} {{ $fields['surname'] }},
                    <br>
					{!! __('messages.messaggio_risposta') !!}
					<p></p>
					{{__('messages.saluti')}},
					<br>
                    {{config('settings.mail.sender_title')}}
				</td>
			</tr>
	        {{-- Footer --}}
			@include('emails.includes.footer')
		</table>
	</body>
</html>
