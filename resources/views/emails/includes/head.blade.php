<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="copyright" content="Copyright {{ $fields['settings']->company_business_name }}" />
    <title>Informazioni da {{ $fields['settings']->company_business_name }}</title>
    <link rel="stylesheet" href="{{ asset('css/plugins/bootstrap.min.css') }}">
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: Catamaran, sans-serif;
            font-size: 16px;
        }

        a,
        a>img {
            border: none;
            text-decoration: none;
        }

        a
        {
            color: #444;
        }

        .logo {
            text-align: center;
            padding-bottom: 10px;
        }

        table,
        table td {
            border-collapse: collapse;
            font-family: Catamaran, sans-serif;
            color: #333;
        }

        p {
            font-family: Catamaran, sans-serif;
            color: #888;
        }

        .hr-separator {
            border: 1px solid #343a40;
        }

        thead {
            background-color: #6fba28;
        }

        thead tr, thead td {
            padding: 0;
            margin: 0;
        }

        tbody td {
            padding: 0 8px;
        }

        .endbody {
            padding-top: 10px;
        }

        .footer {
            background-color: #6fba28;
            font-size: 0.8rem;
            text-align: center;
        }

        .footer td {
            padding: 8px;
            color: #fff;
            line-height: 1.5rem;
        }

        .footer td a {
            color: #fff;
        }

        .unsubscribe {
            margin-top: 10px;
            color: #8e9eab;
            font-size: 0.7rem;
            text-align: center;
        }

        .unsubscribe a {
            margin-top: 10px;
            color: #222222;
            font-size: 0.7rem;
            text-align: center;
        }

    </style>

</head>
