<tfoot class="footer">
    <tr>
        <td>
            <strong>{{ $fields['settings']->company_business_name }}</strong>
            <br>P.IVA {{$fields['settings']->company_vat}} - {{$fields['settings']->company_address}} {{$fields['settings']->company_address_number}}, {{$fields['settings']->company_postal_code}} {{$fields['settings']->company_city}} ({{$fields['settings']->company_province}})
            <br>
            T: {{$fields['settings']->company_phone1}} - <a href="https://{{$fields['settings']->company_url}}" target="_blank">{{$fields['settings']->company_url}}</a></p>
        </td>
    </tr>
</tfoot>
