<!doctype html>
<html lang="it">
	{{-- Head --}}
	@include('emails.includes.head')

	<body style="border:0; margin:0; padding:20px;">
        <table border="0" cellspacing="0" cellpadding="20" width="100%" style="margin:0 auto;">
			{{-- Logo --}}
			@include('emails.includes.logo')
			{{-- Body --}}
			<tr>
				<td colspan="2">
					<p>E' arrivata una richiesta da parte di <b>{{ $fields['name'] }} {{ $fields['surname'] }}</b></p>
					<p>Dati della richiesta:</p>
                    <table border="0" cellspacing="0" cellpadding="5">
                        <tr>
                            <td>{{__('messages.name')}}:</td>
                            <td>{{ $fields['name'] }} {{ $fields['surname'] }}</td>
                        </tr>
                        @if($fields['email'] != '')
                            <tr>
                                <td>{{__('messages.email')}}:</td>
                                <td><a href="mailto:{{$fields['email']}}">{{$fields['email'] }}</td>
                            </tr>
                        @endif
                        @if($fields['phone'] != '')
                            <tr>
                                <td>{{__('messages.phone')}}:</td>
                                <td>{{ $fields['phone'] }}</td>
                            </tr>
                        @endif
                        <tr>
                            <td>{{__('messages.message')}}:</td>
                            <td>{!! $fields['message'] !!}</td>
                        </tr>
                    </table>
				</td>
			</tr>
	        {{-- Footer --}}
	        @include('emails.includes.footer')
	      </table>
	</body>
</html>
