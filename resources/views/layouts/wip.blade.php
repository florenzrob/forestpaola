<!doctype html>
<html lang="it">

<head>
    @include('includes.head') 
    @include('includes.styles')
</head>

<body class="h-100 wip">
    

        <div class="container-fluid h-100 bg-overlay">
            <div class="row justify-content-center align-items-center h-100 wip-text">
                <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3 text-center">
                    <h3 class="mb-5">www.forestpaola.it</h3>
                    <h1>COMING SOON</h1>
                </div>
            </div>
        </div>            
  

    
    @include('includes.scripts')
</body>

</html>