@extends('layouts.base')

@section('content')


    <div class="container-fluid py-2">

        <div class="container my-5">

            <div class="row justify-content-center">
                <div class="col-6">
                    @include('blocks.mailinglists.unsubscribe')
                </div>
            </div>


        </div>
    </div>

    <div class="container-fluid bg-light">
        <div class="row mx-5">
            <div class="col mx-5">
                {{-- Pagine in evidenza --}}
                @include('blocks.pages.highlights')
            </div>
        </div>
        <div class="row mx-5">
            <div class="col mx-5">
                {{-- Articoli in evidenza --}}
                @include('blocks.articles.highlights')
            </div>
        </div>
    </div>
@endsection
