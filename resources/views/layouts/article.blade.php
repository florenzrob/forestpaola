@extends('layouts.base')

@section('content')

    {{-- header gallery --}}
    @if(!empty($cl))
        @foreach($cl as $media)
            @if(strtolower($media->mediaposition->name) == 'header')
                @include('blocks.medias.gallery', ['media' => $media])
            @endif
        @endforeach
    @endif

    <div class="container-fluid py-3">
        <div class="container">
            <div class="row">
                <div class="col">
                    @include('includes.navigation.breadcrumb', ['page' => $page, 'article' => $article])
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-light py-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 @if($article->show_sidebar_dx) col-md-8 @endif">
                    @if(isset($article->category))
                        <h5 class="mt-3"><span class="badge border border-success p-2">{{$article->category->title}}</span></h5>
                    @endif

                    <h1>{{$article->title}}</h1>
                    <h3>{{$article->subtitle}}</h3>

                    {{-- header gallery --}}
                    @if(!empty($cl))
                        @foreach($cl as $media)
                            @if(strtolower($media->mediaposition->name) == 'header')
                                <div class="row mb-3">
                                    <div class="col">
                                        @include('blocks.medias.gallery', ['media' => $media])
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                    <div class="row mb-3">
                        <div class="col">
                            @if(!empty($article->image_cover))
                                <img src="{{ asset('images/uploads/'.$article->image_cover) }}"
                                class="mr-3 mb-3 img-fluid float-md-left {{--contentImg-250--}}"
                                style="max-height:{{!empty($article->cover_height) ? $article->cover_height : 300}}px;"
                                alt="{{$article->title}}" />
                            @endif

                            <span class="text-justify mb-5">{!!$article->content!!}</span>
                        </div>
                    </div>

                    {{-- video --}}
                    @if(!empty($article->video_link))
                        @include('blocks.medias.video', ['link' => $article->video_link])
                    @endif

                    {{-- footer gallery --}}
                    @if(!empty($cl))
                        @foreach($cl as $media)
                            @if(strtolower($media->mediaposition->name) == 'footer')
                                <div class="row mb-3">
                                    <div class="col">
                                        @include('blocks.medias.gallery', ['media' => $media])
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                </div>

                @if($article->show_sidebar_dx)
                    <div class="col-sm-12 col-md-3 offset-md-1 bg-white">
                        {{-- Categorie in evidenza --}}
                        @include('blocks.articles.sidebar_right')
                    </div>
                @endif
            </div>

            {{-- Elenco degli articoli figli --}}
            @if(count($article->children) > 0)
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <ul class="list-group list-group-flush">
                            @foreach($article->children as $child)
                                <li class="list-group-item p-0"><a class="nav-link" href="{{route('page', ['query' => $article->slug . '/' . $child->slug])}}"><i class="fas fa-angle-double-right mr-1"></i> {{$child->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif


             {{-- Elenco delle categorie --}}
             @if(isset($article->category))
                @if(count($article->category->pages) > 0)

                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <hr>
                            <p>{{__('admin.articles.related_pages')}}:</p>
                            <ul class="list-group list-group-flush">
                                @foreach($article->category->pages as $page)
                                    <li class="list-group-item p-0"><a class="nav-link" href="{{route('page', ['query' => $page->slug])}}"><i class="fas fa-angle-double-right mr-1"></i> {{$page->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection
