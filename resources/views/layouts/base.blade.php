<!doctype html>
<html lang="it">

<head>
    @include('includes.head')
    @include('includes.styles')
    @translations {{-- Package thepinecode/i18n: Includo il file delle traduzioni in javascript --}}
</head>

<body id="body" class="eupopup eupopup-bottomright"
    {{--Cookie policy --}}
    data-cookie-route="{{route('disclaimer.privacy')}}"
    data-cookie-title="{{__('cookies.title')}}"
    data-cookie-text="{{__('cookies.text')}}"
    data-cookie-btn-continue="{{__('cookies.continue')}}"
    data-cookie-btn-details="{{__('cookies.details')}}"
    >
    @include('includes.preheader')
    <div class="header-content">
        @include('includes.header')
    </div>
    <div class="body-content" style="margin-top:{{!empty($settings->company_logo_height) ? $settings->company_logo_height+15 : 60}}px;">
        @yield('content')
    </div>
    {{-- contatti --}}
    <div class="container-fluid mt-5 bg-dark">
        @include('forms.contatti')
    </div>

    @include('includes.footer')
    @include('includes.scripts')
</body>

</html>
