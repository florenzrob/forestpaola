@extends('layouts.base')

@section('content')

    {{-- header gallery --}}
    @if(!empty($cl))
        @foreach($cl as $media)
            @if(strtolower($media->mediaposition->name) == 'header')
                @include('blocks.medias.gallery', ['media' => $media])
            @endif
        @endforeach
    @endif

    <div class="container-fluid py-3">
        <div class="container">
            <div class="row">
                <div class="col">
                    @include('includes.navigation.breadcrumb', ['page' => $page])
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-light py-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 @if($page->show_sidebar_dx) col-md-8 @endif">
                    <h1>{{$page->title}}</h1>
                    <h3>{{$page->subtitle}}</h3>


                    @include('blocks.pages.content')

                    {{-- Footer gallery --}}
                    @if(!empty($cl))
                        @foreach($cl as $media)
                            @if(strtolower($media->mediaposition->name) == 'footer')
                                <div class="row mb-3">
                                    <div class="col">
                                        @include('blocks.medias.gallery', ['media' => $media])
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                </div>

                @if($page->show_sidebar_dx)
                    <div class="col-sm-12 col-md-3 offset-md-1 bg-white">
                        {{-- Categorie in evidenza --}}
                        @include('blocks.articles.sidebar_right')
                    </div>
                @endif
            </div>

            {{-- Elenco delle pagine figlie --}}
            @if(count($page->children) > 0)
                <div class="row mt-5">
                    <div class="col-sm-12 col-md-4">
                        <ul class="list-group list-group-flush">
                            @foreach($page->children as $child)
                                <li class="list-group-item p-0"><a class="nav-link" href="{{route('page', ['query' => $page->slug . '/' . $child->slug])}}"><i class="fas fa-angle-double-right mr-1"></i> {{$child->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <hr>
            {{-- Elenco delle categorie articoli collegate --}}
            @if(!empty($page->categories))
                @foreach($page->categories as $category)
                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <h3 class="border-bottom border-success">{{$category->title}}</h3>
                        </div>
                    </div>
                    <div class="row">
                        @if(count($category->articles) > 0)
                            @foreach($category->articles as $article)
                                <div class="col-sm-12 col-md-6">
                                    <figure class="snip1477">
                                        <img src="{{ asset('images/uploads/'.$article->image_cover) }}" class="crop img-fluid" alt="{{$article->title}}">
                                        <div class="title">
                                            <div>
                                                <h2>{{$article->title}}</h2>
                                            </div>
                                        </div>
                                        <figcaption>
                                            <p>{!!$article->abstract!!}</p>
                                        </figcaption>
                                        <a href="{{route('article', [/*'query' => $query,*/ 'articleSlug' => $article->slug])}}"></a>
                                    </figure>
                                </div>
                            @endforeach
                        @endif
                    </div>
                @endforeach
            @endif

        </div>
    </div>
@endsection
