@extends('layouts.base')

@section('content')

    {{--@include('blocks.medias.header-carousel')--}}

    {{-- header gallery --}}
    @if(!empty($cl))
        @foreach($cl as $media)
            @if(strtolower($media->mediaposition->name) == 'header')
                @include('blocks.medias.gallery', ['media' => $media])
            @endif
        @endforeach
    @endif

    <div class="container-fluid py-2">
        <div class="row">
            <div class="col px-5">
                @if(!empty($page))
                    @include('blocks.pages.content')
                @endif
            </div>
        </div>

        <div class="row">

            <div class="col-sm-12 col-md-10">
                <div class="row mx-5 bg-light">
                    <div class="col">
                        {{-- Pagine in evidenza --}}
                        @include('blocks.pages.highlights')
                    </div>
                </div>
                <div class="row mx-5 bg-light">
                    <div class="col">
                        {{-- Articoli in evidenza --}}
                        @include('blocks.articles.highlights')
                    </div>
                </div>

            </div>
            <div class="col-sm-12 col-md-2">
                <div class="card bg-light">
                    <div class="card-body">
                        <h5 class="card-title text-center">Iscriviti alla newsletter</h5>
                        <p class="card-text">@include('forms.newsletter')</p>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
