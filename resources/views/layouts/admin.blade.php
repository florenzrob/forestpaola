<!doctype html>
<html lang="it">

<head>
    @include('includes.head')
    @include('includes.styles')
    @translations {{-- Package thepinecode/i18n: Includo il file delle traduzioni in javascript --}}
</head>

<body>
    @include('admin.includes.preheader')
    <div class="header-content">
        @include('admin.includes.header')
    </div>

    <div class="container-fluid py-5">
        @yield('content')
    </div>

    @include('includes.footer')
    @include('includes.scripts')
</body>

</html>
