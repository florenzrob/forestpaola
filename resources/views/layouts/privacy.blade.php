@extends('layouts.base')

@section('content')

{{--@include('blocks.medias.header-carousel')--}}

<div class="container-fluid content-container py-2">

    <div class="container content-container my-5">

        <div class="row justify-content-center">
            <div class="col-10 text-justify">
                <p>La protezione dei tuoi Dati personali è una delle priorità di Paola Barducci, proprietaria del sito
                    <a href="https://www.forestpaola.it">www.forestpaola.it</a>.
                    Sul Sito applichiamo questa Politica sulla Privacy (“Politica sulla Privacy” o “Privacy Policy”) che
                    ci consente di informarti sull’uso e
                    sulla protezione dei tuoi Dati.</p>

                <p>Nell’ambito della Politica sulla Privacy, il termine “Dati personali” o “Dati” indica ogni tipo di
                    informazione che ti identifica
                    direttamente o indirettamente, con particolare attenzione a partire dal terminale che utilizzi,
                    ovvero, dal tuo computer, tablet,
                    smartphone o qualsiasi strumento con cui accedi ai servizi di Forestpaola.</p>

                <p>Utilizzando il nostro Sito, dichiari di aver letto la nostra Politica sulla Privacy. In caso di
                    modifica delle condizioni di trattamento dei
                    tuoi Dati, aggiorneremo la nostra Politica sulla Privacy e sarai informato tramite e-mail e/o
                    durante la tua prossima visita al Sito.</p>

                <p><b>Articolo 1. Titolare del Trattamento e Delegato della Protezione dei Dati</b></p>
                <p>il titolare del trattamento dei Dati Personali che ti riguardano secondo le modalità descritte in
                    questo documento è Paola Barducci,
                    socia dello Studio Forestale Associato ECOS con sede legale in Rio Santo 29 Frazione Susà 38057
                    Pergine Valsugana (TN) CF e
                    P.IVA 02084340229 email: <a href="mailto:p.barducci@libero.it">p.barducci@libero.it</a></p>

                <p><b>Articolo 2. Quali sono i Dati che raccogliamo e in quale momento?</b></p>
                <p>Quando utilizzi il nostro Sito e servizi, potremmo raccogliere i seguenti Dati su di te:
                    <ul>
                        <li>cognome, nome</li>
                        <li>indirizzo postale</li>
                        <li>indirizzo e-mail</li>
                        <li>Indirizzo IP</li>
                        <li></li>
                    </ul>
                </p>
                {{--
                    <p>Se ti colleghi al nostro Sito tramite l’autenticazione di Facebook, avremo anche accesso alle informazioni del tuo profilo pubblico e,
                        in base alle scelte che hai fatto tramite Facebook, all’indirizzo e-mail che hai inserito in Facebook.
                        Inoltre, altri Dati potrebbero essere raccolti automaticamente durante la navigazione sul Sito.
                        L’elenco delle categorie di Dati in questione è presentato nell’art. 10 relativo alla Cookie Policy.
                        --}}
                </p>
                <p>I Dati sopra riportati possono essere raccolti in momenti diversi. A titolo d’esempio, ma non
                    necessariamente esaustivo:
                    <ul>
                        <li>quando ti registri esplicitamente alla nostra newsletter</li>
                        <li>durante la navigazione nel Sito</li>
                        <li>quando ci contatti</li>
                        {{-- <li>quando esegui l’autenticazione tramite Facebook</li>
                        <li>li>quando effettui i tuoi ordini</li>
                        --}}
                    </ul>
                </p>
                <p><b>Articolo 3. A quali fini e su che basi sono utilizzati i tuoi Dati?</b></p>
                <p>
                    I tuoi Dati vengono elaborati per i seguenti fini:
                    <ul>
                        <li>inviarti le nostre offerte commerciali sulla base dell’interesse legittimo che abbiamo di
                            garantire che tu possa beneficiare dell’esperienza
                            più appropriata sul nostro Sito</li>
                        <li>personalizzare le comunicazioni che ti inviamo e le offerte che ti proponiamo, in base alla
                            tua navigazione sul Sito
                            {{--e ai tuoi precedenti acquisti, sulla base--}} e al tuo previo consenso</li>
                        <li>inviarti le offerte dei nostri partner sulla base del tuo previo consenso</li>
                        {{--
                <li>elaborare statistiche commerciali e analizzare i nostri strumenti di marketing (es. contabilizzare il numero di visualizzazione e attivazione delle nostre aree di vendita, stabilire statistiche di frequentazione delle pagine e degli elementi del Sito), sulla base dell’interesse legittimo che abbiamo di essere in grado di comprendere e migliorare le prestazioni del nostro Sito;
                – lottare contro la frode (ad esempio l’attuazione di misure di sicurezza), sulla base dell’interesse legittimo che abbiamo nel garantire
                la sicurezza delle transazioni effettuate sul nostro Sito, alle condizioni di cui all’articolo 8-2 di seguito.</li>
                    --}}
                    </ul>
                </p>
                <p><b>Articolo 4. Per quanto tempo conserviamo i tuoi Dati?</b></p>
                <p>Fatte salve le disposizioni specifiche per la lotta contro la frode, presentate nell’articolo 8 di
                    questa Politica sulla Privacy,
                    i tuoi Dati personali sono conservati nel nostro database attivo per un massimo di 5 anni dalla tua
                    ultima attività, ovvero a partire da:
                    <ul>
                        <li>la tua ultima visita al nostro Sito
                            {{--, a condizione che tu abbia effettuato l’accesso al tuo account e abbia visitato le nostre pagine;--}}
                        </li>
                        <li>l’apertura di un collegamento ipertestuale in una newsletter o altra e-mail commerciale che
                            ti inviamo (se hai acconsentito).</li>
                    </ul>
                </p>
                <p>
                    Qualche settimana prima di tale scadenza, potremmo contattarti per chiederti se desideri mantenere
                    {{--il tuo account--}} la tua iscrizione.
                    {{--
                 In caso contrario o in assenza di una tua risposta, chiuderemo il tuo account e cancelleremo i tuoi Dati dal nostro database attivo e non sarai
                 più in grado di accedere al tuo account con le tue vecchie credenziali e dovrai ricrearne uno nuovo.
                La cancellazione dei tuoi Dati dal nostro database attivo sarà seguita da un periodo di archiviazione provvisorio al fine di soddisfare i
                nostri obblighi legali, contabili e fiscali, nonché per poter gestire eventuali reclami, entro i limiti dei termini di prescrizione applicabili.
                Nel caso in cui i tuoi Dati debbano essere archiviati successivamente, saranno irreversibilmente resi anonimi.
                --}}
                    <br>
                    In ogni caso, e in conformità con l’articolo 7 di seguito, hai la possibilità in qualsiasi momento
                    di chiedere la cancellazione di tutti o parte dei tuoi
                    Dati, di opporti al loro trattamento o di richiederne la limitazione.
                </p>
                <p><b>Articolo 5. Destinatari dei tuoi Dati personali</b></p>
                <p>
                    I Dati sono trattati presso le sedi operative del Titolare ed in ogni altro luogo in cui le parti
                    coinvolte nel trattamento siano localizzate.
                    Per ulteriori informazioni, contatta il Titolare.<br>
                    I Dati Personali dell’Utente potrebbero essere trasferiti in un paese diverso da quello in cui
                    l’Utente si trova. Per ottenere ulteriori informazioni
                    sul luogo del trattamento l’Utente può fare riferimento alla sezione relativa ai dettagli sul
                    trattamento dei Dati Personali.<br>
                    L’Utente ha diritto a ottenere informazioni in merito alla base giuridica del trasferimento di Dati
                    al di fuori dell’Unione Europea o
                    ad un’organizzazione internazionale di diritto internazionale pubblico o costituita da due o più
                    paesi, come ad esempio l’ONU, nonché in
                    merito alle misure di sicurezza adottate dal Titolare per proteggere i Dati.</p>

                <p>
                    L’Utente può verificare se abbia luogo uno dei trasferimenti appena descritti esaminando la sezione
                    di questo documento relativa ai dettagli sul
                    trattamento di Dati Personali o chiedere informazioni al Titolare contattandolo agli estremi
                    riportati in apertura.</p>

                <p>
                    Possiamo anche condividere i tuoi Dati, per intero o in parte, con terzi laddove:
                    <ul>
                        <li>disponiamo del tuo previo consenso</li>
                        <li>siamo tenuti a farlo per legge, in base a una richiesta imperativa da parte di un’autorità
                            pubblica o nell’ambito di procedimenti legali</li>
                        <li>cediamo o trasferiamo per intero o in parte la nostra attività o beni, ad esempio
                            nell’ambito di una fusione o vendita.
                            Operazioni di questo tipo possono legittimamente implicare l’esecuzione di verifiche
                            preliminari da parte di professionisti e attraverso
                            la messa a disposizione di misure di sicurezza appropriate per garantire la riservatezza dei
                            Dati cui accedono.
                            In caso di effettivo completamento di una transazione per il trasferimento o la vendita dei
                            nostri beni contenenti i tuoi Dati,
                            implementeremo procedure informative per garantire l’esercizio dei diritti che hai sui tuoi
                            Dati.</li>
                    </ul>
                </p>

                <p><b>Articolo 6. Trasferimento dei tuoi Dati Personali fuori dell’Unione europea</b></p>


                <p>Può accadere che i tuoi Dati transitino o siano ospitati su server di proprietà o di proprietà dei
                    nostri partner, situati al di fuori dell’Unione
                    europea. Questi server possono essere localizzati in tutto il mondo, in Paesi le cui leggi
                    potrebbero fornire un diverso livello di protezione
                    rispetto al nostro. Tuttavia, ci impegniamo a prendere le misure necessarie per mantenere un
                    adeguato livello di privacy e sicurezza.
                    Ad esempio, potremmo richiedere ai nostri subappaltatori e partner di implementare misure atte a
                    garantire il livello di protezione richiesto
                    dalle normative applicabili sui Dati personali.</p>


                <p><b>Articolo 7. Quali sono i tuoi diritti e come esercitarli?</b></p>
                <p>
                    In conformità con le normative applicabili in materia di Dati personali, è possibile esercitare i
                    seguenti diritti con Forestpaola:<ul>
                        <li>accedere ai tuoi Dati, richiederne la correzione, cancellazione o portabilità</li>
                        <li>ritirare il consenso per il futuro, per tutti o parte dei trattamenti in questione</li>
                        <li>opporsi al trattamento dei tuoi Dati o richiederne la limitazione</li>
                    </ul>
                </p>

                <p>Puoi esercitare questi diritti o inviando un’e-mail a <a
                        href="mailto:p.barducci@libero.it">p.barducci@libero.it</a> o per posta al seguente
                    indirizzo:<br>
                    via Rio Santo 29<br>
                    Frazione Susà 38057 Pergine Valsugana (TN)<br>
                    fornendo un documento comprovante l’identità e specificando:<ul>
                        <li>cognome, nome, indirizzo e-mail</li>
                        <li>oggetto della richiesta</li>
                        <li>indirizzo a cui deve essere inviata la risposta</li>
                    </ul>
                </p>
                <p>Ti risponderemo entro un (1) mese dalla ricezione della tua richiesta da parte dei nostri
                    servizi.<br>
                    Hai anche il diritto di sporgere reclamo presso un’autorità di vigilanza.
                </p>

                <p><b>Articolo 8. Social Network</b></p>

                <p>
                    Come la maggior parte di voi, siamo presenti su molti social network come Facebook o Instagram. Puoi
                    trovarli sul nostro Sito in diverse occasioni:
                    <ul>
                        {{--<li>quando ti connetti al nostro Sito tramite il tuo account Facebook. Come indicato nell’articolo 2,
                    avremo quindi accesso alle informazioni del tuo profilo pubblico e, in base alle scelte che hai fatto tramite Facebook,
                    all’indirizzo e-mail che hai inserito su Facebook. Tuttavia, non pubblicheremo mai nulla sul tuo profilo Facebook;</li>--}}
                        <li>quando condividi le nostre iniziative sui social network</li>
                    </ul>
                </p>
                <p>Ciascuno dei social network ha la sua politica sulla privacy, che ti invitiamo a consultare.</p>

                <p><b>Articolo 9. Cookie Policy</b></p>

                <p>
                    Il termine “Cookie ” indica un file depositato e letto durante la consultazione di un sito web, la
                    lettura di un’e-mail, l’installazione o l’utilizzo
                    di software o un’applicazione mobile, indipendentemente dal tipo di terminale utilizzato.</p>
                <p>
                    I cookie impostati sul Sito sono di diversi tipi e servono a diversi scopi.</p>
                <p>
                    Puoi accettare o rifiutare l’installazione di questi cookie secondo i termini spiegati di seguito.
                    Nota che le impostazioni verranno prese in
                    considerazione entro 24 ore dalla richiesta. I tuoi cookie sono validi per 13 mesi.</p>
                <p>
                    È possibile in qualsiasi momento gestire i termini e le condizioni per il deposito e l’utilizzo dei
                    Dati raccolti tramite cookie.</p>

                <p>Come gestire i Cookies?</p>

                <p>
                    {{--Come si spiega alla pagina Trattamento dei Cookie,--}} I cookie sono di supporto al fine di
                    ottenere il massimo dal nostro Sito.<br>
                    Se si disattivano i cookie, è possibile che alcune sezioni del nostro Sito web non funzionino. Ad
                    esempio,
                    si possono avere difficoltà di registrazione o di visualizzazione delle pagine del sito.</p>
                <p>
                    Come abilitare o disabilitare i cookie sul proprio browser?</p>
                <p>
                    <dl>
                        <dt>Google Chrome</dt>
                        <dd>Clicca sull’icona della chiave inglese sulla barra del browser<br>
                            Seleziona “Impostazioni”<br>
                            Clicca su “Mostra impostazioni avanzate”<br>
                            Nella sezione “Privacy”, clicca sul pulsante “Impostazioni contenuti”<br>
                            Per abilitare i cookie, nella sezione “Cookie”, seleziona “Consenti il salvataggio dei dati
                            in locale”.
                            In questo modo verranno abilitati sia i cookie first-party che quelli di terzi.
                            Per abilitare solo i cookie first-party, invece, attiva la voce “Blocca cookie di terze
                            parti e dati dei siti”.<br>
                            Per disabilitare completamente i cookie, seleziona “Impedisci ai siti di impostare dati”<br>
                            Per maggiori informazioni sull’impostazione dei cookie su Chrome,
                            fare riferimento alla seguente pagina:
                            <a href="http://support.google.com/chrome/bin/answer.py?hl=en&answer=95647" target="_blank">http://support.google.com/chrome/bin/answer.py?hl=en&answer=95647</a></dd>

                        <dt>Microsoft Internet Explorer 6.0, 7.0, 8.0, 9.0, 10.0</dt>
                        <dd>
                            Cliccare su “Strumenti” nella parte alta della finestra del browser e clicca su “Opzioni
                            Internet”<br>
                            Spostati nella linguetta “Privacy”<br>
                            Per abilitare i cookie: Muovi il selettore su “Media” o inferiore<br>
                            Per disabilitare tutti i cookie: Muovi il selettore completamente in altro<br>
                            Per maggiori informazioni sull’impostazione dei cookie
                            su Internet Explorer, fare riferimento alla seguente pagina da
                            Microsoft: <a href="http://windows.microsoft.com/en-GB/windows-vista/Block-or-allow-cookies" target="_blank">http://windows.microsoft.com/en-GB/windows-vista/Block-or-allow-cookies</a></dd>
                        <dt>Mozilla Firefox</dt>
                        <dd>
                            Clicca su “Opzioni” dal menù del browser<br>
                            Seleziona il pannello “Privacy”<br>
                            Per abilitare i cookie: Seleziona la voce “Accetta i cookie dai siti”<br>
                            Per disabilitare i cookie: Deseleziona la voce “Accetta i cookie dai siti”<br>
                            Per maggiori informazioni sull’impostazione dei cookie su
                            Mozilla Firefox, fare riferimento alla seguente pagina da
                            Mozilla: <a href="http://support.mozilla.org/en-US/kb/Enabling%20and%20disabling%20cookies" target="_blank">http://support.mozilla.org/en-US/kb/Enabling%20and%20disabling%20cookies</a>
                        </dd>
                        <dt>Opera</dt>
                        <dd>
                            Clicca su “Impostazioni” dal menu del browser<br>
                            Seleziona “Impostazioni veloci”<br>
                            Per abilitare i cookies: seleziona “Attiva Cookie”<br>
                            Per disabilitare i cookie: deseleziona “Attiva Cookie”<br>
                            Per maggiori informazioni sull’impostazione dei cookie su Opera,
                            fare riferimento alla seguente pagine da Opera
                            Software:  <a href="http://www.opera.com/browser/tutorials/security/privacy/" target="_blank">http://www.opera.com/browser/tutorials/security/privacy/</a></dd>
                        <dt>Safari su </dt>
                        <dd>
                            Clicca su “Safari” dalla barra dei menu e seleziona “Preferenze”<br>
                            Clicca su “Sicurezza”<br>
                            Per abilitare i cookie: Nella sezione “Accetta i cookie” seleziona “Solo per il sito che sto
                            navigando”<br>
                            Per disabilitare i cookie: Nella sezione “Accetta i cookie” seleziona “Mai”<br>
                            Per maggiori informazioni sull’impostazione dei cookie su Opera, fare riferimento alla
                            seguente pagine
                            da Apple: <a href="http://docs.info.apple.com/article.html?path=Safari/3.0/en/9277.html" target="_blank">http://docs.info.apple.com/article.html?path=Safari/3.0/en/9277.html</a></dd>
                        <dt>Per tutti i browser</dt>
                        <dd>Fare riferimento all’aiuto online del browser che si sta usando.</dd>
                    </dl>
                </p>

            </div>
        </div>


    </div>
</div>

@endsection
