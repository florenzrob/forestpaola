<div class="container py-5">

    <form id="contactForm" class="bg-white shadow p-3 mb-5 rounded"
        data-route="{{route('contacts.send')}}"
        data-success="{{'updateContactsForm'}}"
        >
        @honeypot
        <div class="row">
            <div class="col">
                <h3>Contattami</h3>
            </div>
        </div>

        {{--Seconda riga --}}
        <div class="row">
            <div class="col-md-6">
                <div class="form-row">
                    <div class="col mb-4">
                        {!! Form::text('name', null, [
                            'id' => 'name',
                            'class' => 'form-control',
                            'placeholder' => __('messages.name') . ' *',
                            ])
                        !!}
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="col mb-4">
                        {!! Form::text('surname', null, [
                            'id' => 'surname',
                            'class' => 'form-control',
                            'placeholder' => __('messages.surname') . ' *',
                            ])
                        !!}
                        <span class="text-danger">{{ $errors->first('surname') }}</span>
                    </div>
                </div>


                <div class="form-row">
                    <div class="col mb-4">
                        {!! Form::text('phone', null, [
                            'id' => 'phone',
                            'class' => 'form-control',
                            'placeholder' => __('messages.phone')
                            ])
                        !!}
                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col mb-4">
                        {!! Form::text('email', null, [
                            'id' => 'email',
                            'class' => 'form-control',
                            'placeholder' => __('messages.email') . ' *',

                            ])
                        !!}
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                {!! Form::textarea('message', null, [
                    'id' => 'message',
                    'class' => 'form-control',
                    'rows' => 6,
                    'placeholder' => __('messages.request') . ' *',
                    ])
                !!}
                <span class="text-danger">{{ $errors->first('message') }}</span>

                {{-- Recaptcha --}}
                {{--
                <div class="d-flex justify-content-center mb-3">
                    <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
                    {{--!! NoCaptcha::renderJs() !!}
                    {!! NoCaptcha::display() !!-- }}
                </div>
                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                --}}
            </div>
        </div>

        {{--Terza riga --}}
        <div class="row">
            <div class="col">
                <div class="form-check">
                    {!! Form::checkbox('privacy', true, true, [
                        'id' => 'privacy',
                        'class' => 'form-check-input',
                        ])
                    !!}
                    <label for="privacy">&nbsp; {{__('messages.privacy') }} <u><a href="{{route('disclaimer.privacy')}}">{{__('messages.privacy_link') }}</a></u></label>
                </div>
                <span class="text-danger">{{ $errors->first('privacy') }}</span>
            </div>
        </div>

        {{--Quarta riga --}}
        <div class="row">
            <div class="col">
                <div class="form-check">
                    {!! Form::checkbox('newsletter', true, true, [
                        'id' => 'newsletter',
                        'class' => 'form-check-input',
                        ])
                    !!}
                    <label for="newsletter">Iscriviti alla newsletter</label>
                </div>

                <div class="alert alert-success form-response collapse"></div>

                @if ($errors->has('g-recaptcha-response'))
                    <span class="help-block">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                    </span>
                @endif


                <div class="form-actions text-center my-5">
                    {!! Form::button(__('messages.submit'), [
                        'id' => 'btnContact',
                        'data-formid' => 'contactForm',
                        'class' => 'btn btn-outline-dark rounded-pill text-uppercase px-5'])
                    !!}
                </div>
            </div>

        </div>

    {!! Form::close() !!}

</div>
