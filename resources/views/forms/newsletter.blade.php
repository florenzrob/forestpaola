<form id="newsletterForm" class="rounded" data-route="{{route('mailinglist.subscribe')}}" data-success="{{'updateNewsletterForm'}}">
    @honeypot
    <div class="form-row">
        <div class="col mb-4">
            {!! Form::text('name', null, [
                'id' => 'name',
                'class' => 'form-control',
                'placeholder' => __('messages.name') . ' *',
                ])
            !!}
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>
    </div>


    <div class="form-row">
        <div class="col mb-4">
            {!! Form::text('surname', null, ['id' => 'surname', 'class' => 'form-control', 'placeholder' => __('messages.surname') . ' *'])!!}
            <span class="text-danger">{{ $errors->first('surname') }}</span>
        </div>
    </div>

    <div class="form-row">
        <div class="col mb-4">
            {!! Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => __('messages.email') . ' *'])!!}
            <span class="text-danger">{{ $errors->first('email') }}</span>
        </div>
    </div>

    <div class="form-actions text-center my-5">
        {!! Form::button(__('messages.submit'), [
            'id' => 'btnNewsletter',
            'data-formid' => 'newsletterForm',
            'class' => 'btn btn-outline-dark rounded-pill text-uppercase px-5'])
        !!}
    </div>

    <div class="form-group">
        <div class="alert newsletter-response collapse text-center"></div>
    </div>
</form>
