const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix

    //Compilo e minimizzo i file js. L'ordine dei file è importante!
    .js([
        //'resources/js/app.js',
        'resources/js/plugins.js',
    ], 'public/js/global.min.js')


    //.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
