 {{-- 
    Include breadcrumb:
    Parameters:
        - items:    array di rotte/labels per la creazione del breadcrumb
        - active:   ultimo elemento del breadcrumb, quello attivo
    --}}
    @include('admin.includes.breadcrumb', [
        'active' => __('admin.positions.list')
    ])

{{-- Alerts --}}
@if(session('success'))
    @include('admin.includes.forms.alert', ['class' => 'alert-success', 'message' => session('success')])
@endif

@if(session('error'))
    @include('admin.includes.forms.alert', ['class' => 'alert-danger', 'message' => session('error')])
@endif

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h2>{{ __('admin.positions.title') }}</h2>
            <a href="{{route('position.create')}}" class="btn btn-link"><i class="{{config('settings.icons.add')}} fa-2x"></i></i></a>
        </div>
    </div>
    @if(!empty($positions))
        <div class="table-responsive">
            <table class="table table-sm table-striped table-hover mt-1">
                {{-- thead --}}
                @php
                    $tableFields = [
                        ['class' => '', 'name' =>  'admin.id'],
                        ['class' => '', 'name' =>  'admin.name'],
                        ['class' => '', 'name' =>  'admin.positions.position'],
                        ['class' => '', 'name' =>  'admin.description'],
                    ]
                @endphp
                @include('admin.includes.forms.table_head', ['tableFields' => $tableFields])
                <tbody>
                    @foreach ($positions as $position)
                        <tr>
                            <th class="px-3 align-middle text-center" scope="row">#{{$position->id}}</th>
                            <td nowrap class="px-3 align-middle"><a href="{{route('position.edit', ['id' => $position->id])}}" class="lead">{{$position->name}}</a></td>
                            <td nowrap class="px-3 align-middle">{{$position->position}}</td>
                            <td class="px-3 align-middle w-100">{{$position->description}}</td>
                            <td class="px-3 align-middle">
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group" role="group">
                                        <a href="{{route('position.edit', ['id' => $position->id])}}" class="btn btn-light shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="{{__('admin.edit') . ' ' . $position->position}}"><i class="{{config('settings.icons.edit')}}"></i></a>
                                        <a href="#" 
                                            data-route="{{route('position.delete', ['id' => $position->id])}}" 
                                            data-id="{{$position->id}}" 
                                            class="btn btn-light shadow-sm delete-item" 
                                            data-confirm-title="{{ __('admin.positions.delete_title')}}"
                                            data-confirm-message="{{ __('admin.positions.delete_message', ['name' => $position->position])}}"
                                            data-btn-confirm="{{ __('admin.yes')}}" 
                                            data-btn-dismiss="{{ __('admin.no')}}"
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            title="{{__('admin.delete') . ' ' . $position->position}}"><i class="{{config('settings.icons.delete')}}"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>