<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Settings
    |--------------------------------------------------------------------------
     */

    'forms' => [
        'classForm' => 'form-control',
    ],
    'icons' => [
        'dashboard' => 'fas fa-cogs',
        'add' => 'fas fa-plus-square',
        'edit' => 'fas fa-edit',
        'save' => 'fas fa-save',
        'delete' => 'fas fa-trash',
        'undo' => 'fas fa-undo',
        'exit' => 'fas fa-external-link-alt',
        'check' => 'fas fa-check-circle',
        'comments' => 'fas fa-comment-dots',
    ],

    /*
    |--------------------------------------------------------------------------
    | Email Settings
    |--------------------------------------------------------------------------
     */

    'mail' => [
        'sender_title' => 'Dott.ssa Paola Barducci',
    ],

    /*
    |--------------------------------------------------------------------------
    | Admin Settings
    |--------------------------------------------------------------------------
     */

    'admin' => [
        'dashboard' => [
            'title' => 'Pannello di controllo',
            'icon' => 'fab fa-canadian-maple-leaf',
        ],
    ],

];
