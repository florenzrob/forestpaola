$(function () {

    /*
          // Loading page
      	var loaderPage = function() {
      		$(".site-loader").fadeOut("slow");
      	};
      	loaderPage();

      /  */

    // Lazy loading images
    // http://jquery.eisbehr.de/lazy/
    /*
    $('img.lazy').lazy({
        combined: true,
        delay: 1000, // 1 seconds
        enableThrottle: true,
        throttle: 250
    })
    */


    //Hook per mostrare il nome di un file in un campo input-file
    $('.custom-file input').change(function (e) {
        if (e.target.files.length) {
            $(this).next('.custom-file-label').html(e.target.files[0].name);
        }
    });

    // Inizializzo i ToolTip di Bootstrap (nel caso ce ne siano):
    $('[data-toggle="tooltip"]').tooltip()

    // Funzioni per lo Scroll:
    $('.scroll').click(function (event) {
        event.preventDefault();
        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        },
            500
        )
    })



    //Inizializzo file input di bootstrap
    //bsCustomFileInput.init();

    //Clear file input
    $(document).on('click', '.clear-file', function (e) {
        event.preventDefault();
        var inputId = $(this).attr('data-input-id');
        $('#' + inputId).val(''); //Clear input
        $('#lbl_' + inputId).html(''); //Clear label
    })

    /*
     ***************************************************
     * MULTILEVEL MENU
     ***************************************************
     */
    // bootstrapthemesco/bootstrap-4-multi-dropdown-navbar

    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        var $el = $(this)
        $el.toggleClass('active-dropdown')
        var $parent = $(this).offsetParent('.dropdown-menu')
        if (
            !$(this)
                .next()
                .hasClass('show')
        ) {
            $(this)
                .parents('.dropdown-menu')
                .first()
                .find('.show')
                .removeClass('show')
        }
        var $subMenu = $(this).next('.dropdown-menu')
        $subMenu.toggleClass('show')

        $(this)
            .parent('li')
            .toggleClass('show')

        $(this)
            .parents('li.nav-item.dropdown.show')
            .on('hidden.bs.dropdown', function (e) {
                $('.dropdown-menu .show').removeClass('show')
                $el.removeClass('active-dropdown')
            })

        if (!$parent.parent().hasClass('navbar-nav')) {
            $el.next().css({
                top: $el[0].offsetTop,
                left: $parent.outerWidth() - 4
            })
        }

        return false
    })

    /*
     ***************************************************
     * OWL CAROUSEL
     ***************************************************
     */
    //Carousel
    $('.slide-one-item').owlCarousel({
        center: false,
        items: 1,
        lazyLoad: true,
        loop: true,
        stagePadding: 0,
        margin: 0,
        autoplay: true,
        pauseOnHover: false,
        nav: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: [
            '<i class="fas fa-angle-left"></i>',
            '<i class="fas fa-angle-right"></i>'
        ]
    })


    //Gallery
    $('.owl-gallery').owlCarousel({
        loop: true,
        margin: 10,
        //items:10,
        autoWidth: true,
        //autoHeight:true,
        nav: true,
        center: false,
        lazyLoad: true,
        stagePadding: 0,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        },

    })

    //Carousel
    $('.owl-sliding').owlCarousel({
        loop: true,
        margin: 10,
        //items:10,
        autoplay: true,
        autoWidth: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        },
        center: false,
        lazyLoad: true,
        stagePadding: 0,
        pauseOnHover: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
    })



    /*
     ***************************************************
     * CONTATTI
     ***************************************************
     */

    //Invio form
    $(document).on('click', '#btnContact', function (e) {

        //Valido form
        e.preventDefault();

        var formId = $(this).attr('data-formid'); //Id della form che si sta inviando

        //Campi da validare
        var rules = {
            name: {
                required: true,
                minlength: 2,
            },
            surname: {
                required: true,
                minlength: 2,
            },
            email: {
                required: true,
                email: true
            },
            privacy: {
                required: true,
            },

        };

        var messages = {
            name: {
                required: translator.trans('messages.validations.name_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            surname: {
                required: translator.trans('messages.validations.surname_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            email: {
                required: translator.trans('messages.validations.mail_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
                email: translator.trans('messages.validations.mail_format'),
            },
            privacy: {
                required: translator.trans('messages.validations.privacy_required'),
            },

        }

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })

    /*
     ***************************************************
     * ISCRIZIONE MAILING LIST
     ***************************************************
     */

    //Invio form
    $(document).on('click', '#btnNewsletter', function (e) {

        //Valido form
        e.preventDefault();
        var formId = $(this).attr('data-formid'); //Id della form che si sta inviando

        //Campi da validare
        var rules = {
            name: {
                required: true,
                minlength: 2,
            },
            surname: {
                required: true,
                minlength: 2,
            },
            email: {
                required: true,
                email: true
            },

        };

        var messages = {
            name: {
                required: translator.trans('messages.validations.name_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            surname: {
                required: translator.trans('messages.validations.surname_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            email: {
                required: translator.trans('messages.validations.mail_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
                email: translator.trans('messages.validations.mail_format'),
            },

        }

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })

    //Cancello da mailing list
    $(document).on('click', '#unsubscribeNewsletter', function (e) {

        //Valido form
        e.preventDefault();
        var formId = $(this).attr('data-formid'); //Id della form che si sta inviando

        //Campi da validare
        var rules = {
            email: {
                required: true,
                email: true
            },
        };

        var messages = {
            email: {
                required: translator.trans('messages.validations.mail_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
                email: translator.trans('messages.validations.mail_format'),
            },
        }

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }

    });


})
