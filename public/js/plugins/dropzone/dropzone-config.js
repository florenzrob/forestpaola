 /*
|--------------------------------------------------------------------------
|
| DROPZONE
|
|--------------------------------------------------------------------------
*/

 //selectorClass: classe dell'elemento usato per dropzone
 //params: variabili ulteriori da passare alla post
 Dropzone.autoDiscover = false;

 function loadDropzone(selectorClass, params) {

     //Tipi di file accettati
     var mimeTypes = [
         'image/jpeg',
         'image/png',
         'image/jpg',
         'image/tiff',
         'image/vnd.adobe.photoshop',
         'application/pdf',
         'application/rtf',
         'application/msword', // doc
         'application/vnd.openxmlformats-officedocument.wordprocessingml.document', //docx
         'application/docx', //docx
         'application/postscript',
         'application/x-compressed',
         'application/vnd.ms-excel', // xls
         'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', //xlsx
         'text/plain',
     ].join(',');

     $('.' + selectorClass).dropzone({
         url: $('.' + selectorClass).attr('data-url'),
         paramName: 'file', // The name that will be used to transfer the file
         maxFilesize: 10, // MB
         maxFiles: 1,
         createImageThumbnails: false,
         autoProcessQueue: true,
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         //acceptedFiles: 'image/jpeg, image/png, image/jpg, application/pdf',
         acceptedFiles: mimeTypes,
         addRemoveLinks: true,
         autoDiscover: false,
         //previewsContainer: $('#dz-preview').innerHTML,
         parallelUploads: 2,
         thumbnailHeight: 120,
         thumbnailWidth: 120,
         filesizeBase: 1000,
         uploadMultiple: false,
         timeout: 10000,

         /*
         thumbnail: function(file, dataUrl) {
             if (file.previewElement) {
                 file.previewElement.classList.remove("dz-file-preview");
                 var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                 for (var i = 0; i < images.length; i++) {
                     var thumbnailElement = images[i];
                     thumbnailElement.alt = file.name;
                     thumbnailElement.src = dataUrl;
                 }
                 setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
             }
         },
         */

         //to translate dropzone, you can provide these options:
         dictDefaultMessage: 'aggiungi', //The text used before any files are dropped.
         dictFileTooBig: 'file troppo grande', //If the filesize is too big. {{filesize}} and {{maxFilesize}} will be replaced with the respective configuration values.
         dictInvalidFileType: 'formato sbagliato', //If the file doesn't match the file type
         dictResponseError: 'errore', //If the server response was invalid. {{statusCode}} will be replaced with the servers status code.
         dictCancelUpload: 'Ferma il caricamento', //If addRemoveLinks is true, the text to be used for the cancel upload link.
         dictUploadCanceled: 'Upload cacellato', //The text that is displayed if an upload was manually canceled
         dictCancelUploadConfirmation: 'Vuoi fermarte il caricamento?', //If addRemoveLinks is true, the text to be used for confirmation when cancelling upload.
         dictRemoveFile: 'Elimina file', //If addRemoveLinks is true, the text to be used to remove a file.
         dictRemoveFileConfirmation: 'Vuoi cancellare il file?', //If this is not null, then the user will be prompted before removing a file.
         dictMaxFilesExceeded: 'Limite di file raggiunti', //Displayed if maxFiles is st and exceeded. The string {{maxFiles}} will be replaced by the configuration value.
         //dictFileSizeUnits: null, //Allows you to translate the different units. Starting with tb for terabytes and going down to b for bytes.


         init: function () {

            this.on('addedfile', function(file) { 
               // alert("Added file."); 
            }),

             //Passo parametri opzionali sull'upload
             this.on('sending', function (file, xhr, formData) {
                //console.log(file);
                 /*
                 var id = $('.' + selectorClass).attr('data-id'); //Id del record su cui si allegano file
                 var ismodal = $('.' + selectorClass).attr('data-ismodal'); //True: sto lavorando su form di verifica già esistente, false: nuova verifica
                 var hiddenfield = $('.' + selectorClass).attr('data-hiddenfield'); //Se presente, è il campo hidden in cui scrivo i nomi dei file per salvarli nel db
                 var strAllegati = '';

                 formData.append('id', id);
                 formData.append('ismodal', ismodal);

                 //Todo: non fa, va aggiunto staticamente
                 /*
                                  for (var key in params) {
                                      var value = params[key];
                                      //console.log('key:' + key + ' - value: ' + value);
                                      formData.append(key, value);
                                  }

                                  //console.log('formdata: ' + formData.values());
                                  for (var value of formData.values()) {
                                      console.log(value);
                                  }
                                  for (var key of formData.keys()) {
                                      console.log(key);
                                  }
                 */

                 //Inserisco i nomi dei file caricati in un campo hidden

                 //hiddenfield NOT false, undefined, 0, null or empty
                 if (typeof hiddenfield != 'undefined' && hiddenfield) {
                     strAllegati = $('#' + hiddenfield).val();
                     strAllegati = strAllegati.length == 0 ? file.name : strAllegati + ',' + file.name;

                     $('#' + hiddenfield).val(strAllegati);
                 }

             });

             //Pulisce canvas dopo l'upload
             this.on("queuecomplete", function () {
                 this.removeAllFiles();
             });
         },

         success: function (file, response) {

             var reloadDiv = $('.' + selectorClass).attr('data-reload-div'); //Div in cui mostrare i risultati
                /*
             console.log(response);
             console.log(response.path);
             console.log(response.id);
             console.log(response.ismodal);
             console.log(reloadDiv);
            */

             $('#' + reloadDiv).html(response.view);
         },
         error: function (file, response) {
             console.log('error');
             console.log(response);
             return false;
         },

     });
 }
