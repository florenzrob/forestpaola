$(function () {

    //Elimino template
    $(document).on('click', '.delete-item', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateTemplatesList',
            'params': '',
        }

        uf.confirmBox(confirmParams,postParams,uf, pf);

    });


    //Validazione form
    $(document).on('click', '.form-send', function (e) {

        e.preventDefault();

        var formId = $(this).attr('data-formid');

        //Campi da validare
        var rules = {
            title: {
                required: true,
                minlength: 2,
            },
            template_name: {
                required: true,
                minlength: 2,
            },

        };

        var messages = {
            title: {
                required: translator.trans('messages.validations.title_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            title: {
                required: translator.trans('messages.validations.name_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },

        };

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })

})
