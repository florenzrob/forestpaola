$(function () {

    //Init datatable
    uf.dataTable('table-paginate', uf, pf);

    //Multiselect
    uf.bsmultiselect('multiselect');

    //Attivo/disattivo active
    $(document).on('click', '.subscriber-active-switch', function (e) {

        e.preventDefault();

        var active = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_active = active == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_active': is_active,
            },
            'onSuccess': 'updateSubscribersList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });



    //Elimino subscriber
    $(document).on('click', '.delete-item', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateSubscribersList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Validazione form
    $(document).on('click', '.form-send', function (e) {

        e.preventDefault();

        var formId = $(this).attr('data-formid');

        //Campi da validare
        var rules = {
            name: {
                required: true,
                minlength: 2,
            },
            surname: {
                required: true,
                minlength: 2,
            },
            email: {
                required: true,
                email: true
            },

        };

        var messages = {
            name: {
                required: translator.trans('messages.validations.name_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            surname: {
                required: translator.trans('messages.validations.surname_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            email: {
                required: translator.trans('messages.validations.mail_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
                email: translator.trans('messages.validations.mail_format'),
            },
        }

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })
})
