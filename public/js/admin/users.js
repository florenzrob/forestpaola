$(function () {

    //Init datatable
    uf.dataTable('table-paginate', uf, pf);


    //Attivo/disattivo is_admin
    $(document).on('click', '.user-isadmin-switch', function (e) {

        e.preventDefault();

        var isAdmin = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_admin = isAdmin == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_admin': is_admin,
            },
            'onSuccess': 'updateUsersList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });


    //Attivo/disattivo active
    $(document).on('click', '.user-active-switch', function (e) {

        e.preventDefault();

        var active = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_active = active == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'active': is_active,
            },
            'onSuccess': 'updateUsersList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });


    //Elimino utente
    $(document).on('click', '.delete-item', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateUsersList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Elimino immagine profilo
    $(document).on('click', '.delete-image', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');
        var filename = $(this).attr('data-filename');
        var containerDiv = $(this).attr('data-container-div');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
                'filename': filename,
                'containerDiv': containerDiv,
            },
            'onSuccess': 'updateCoverContainer',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Validazione form
    $(document).on('click', '.form-send', function (e) {

        e.preventDefault();

        var formId = $(this).attr('data-formid');

        //Campi da validare
        var rules = {
            name: {
                required: true,
                minlength: 2,
            },
            surname: {
                required: true,
                minlength: 2,
            },
            email: {
                required: true,
                email: true
            },
            username: {
                required: true,
                minlength: 2,
            },
            password: {
                required: true,
                minlength: 8,
            },
            'password_confirmation': {
                required: true,
                minlength: 8,
            },

        };

        var messages = {
            name: {
                required: translator.trans('messages.validations.name_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            surname: {
                required: translator.trans('messages.validations.surname_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            email: {
                required: translator.trans('messages.validations.mail_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
                email: translator.trans('messages.validations.mail_format'),
            },
            username: {
                required: translator.trans('messages.validations.username_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            password: {
                required: translator.trans('messages.validations.password_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },
            'password_confirmation': {
                required: translator.trans('messages.validations.password_confirm_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },

        }

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })
})
