
class postSuccessFunctions {


    //Chiamata dinamica della funzione di successo sull'ajaxpost
    executeSuccess(successFunction, response, uf, ac, te) {
        //console.log(successFunction);
        this[successFunction](response, uf, ac, te);
    }

    //Users
    updateUsersList(response, uf) {
        $('#users_container').html(response.view);
    }

    //Templates
    updateTemplatesList(response, uf) {
        $('#templates_container').html(response.view);
    }

    //Positions
    updatePositionsList(response, uf) {
        $('#positions_container').html(response.view);
    }

    //Menus
    updateMenusList(response, uf) {
        $('#menus_container').html(response.view);
    }

    //Pages
    updatePagesList(response, uf) {
        $('#pages_container').html(response.view);
        uf.dataTable('table-paginate', uf, this);
    }

    //Categories
    updateCategoriesList(response, uf) {
        $('#categories_container').html(response.view);
        uf.dataTable('table-paginate', uf, this);

    }

    //Articles
    updateArticlesList(response, uf) {
        //console.log(response);
        $('#articles_container').html(response.view);
        uf.dataTable('table-paginate', uf, this);

    }

    //Tags
    updateTagsList(response, uf) {
        $('#tags_container').html(response.view);
    }

    //Mailinglist
    updateSubscribersList(response, uf) {
        $('#subscribers_container').html(response.view);
    }

    updateMailingGroupsList(response, uf) {
        $('#mlgroups_container').html(response.view);
    }


    updateMailinglistList(response, uf) {
        $('#mailinglists_container').html(response.view);
    }


    //Contatti
    updateContactsForm(response, uf) {
        //console.log(response);
        $('.form-response').removeClass('collapse').html(response.responseText);
    }

    //Iscrizione mailing list
    updateNewsletterForm(response, uf) {
        //console.log(response);
        $('.newsletter-response').removeClass('collapse').addClass('alert-' + response.class).html(response.responseText);

        if(response.class == 'success'){
            $('#' + response.formid)[0].reset();
        }
    }


    //Cancellazione da mailing list
    updateUnsubscribeForm(response, uf) {
        //console.log(response);
        $('.form-response').removeClass('collapse').addClass('alert-' + response.class).html(response.responseText);

        if(response.class == 'success'){
            $('#' + response.formid)[0].reset();
        }
    }


    /*
     ******************************************************************************************************************************************************
     * MEDIA
     ******************************************************************************************************************************************************
     */

    //Images - Reload lista immagini dopo eliminazione
    updateImagesList(response, uf) {
        $('#' + response.containerDiv).html(response.view);
    }

    //Gallery - Carico record nella select di creazione/modifica gallery
    getRelatedRecords(response, uf) {
        $('#record_id').html(response.view);
    }

    //Gallery - Ricarico elenco files dopo aggiunta da modale
    updateTableItemsContainer(response, uf, ac, te) {

        //Relolad editor
        //te.mceInit();

        $('#table_items_container').html(response.view);
    }

    //Galleries - Ricarico elenco dopo eliminazione gallery
    updateGalleryList(response, uf) {
        $('#galleries_container').html(response.view);
    }


    /*
     ******************************************************************************************************************************************************
     * UPLOADER
     ******************************************************************************************************************************************************
     */
    /*
     updateUploadList(response, uf) {
         console.log(response.urls);
         $('.' + response.divtoreload).html(response.view);
     }
     */

    updateCoverContainer(response, uf) {
        $('.' + response.divtoreload + ' .img').html(response.replaceHtml);
        $('#' + response.inputId).val('');
    }

    /*
     ******************************************************************************************************************************************************
     * DATATABLE
     ******************************************************************************************************************************************************
     */

    datatableUpdateSort(response, uf) {
        console.log(response);
    }

}


export default postSuccessFunctions // ES6
