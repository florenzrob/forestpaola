class autocompleteFunctions {

    test() {
        alert('autocomplete: ci sono');
    }

    /*
    |=================================================================================================================================
    |
    | SELECT2
    |
    |=================================================================================================================================
    */

    /*
    Parameters
    uf = utilitiesFunctions
    pf = postSuccessFunctions
    ac = autocompleteFunctions
    item = classe dell'elemento da trasformare in select2
    postParams = Parametri per la chiamata ajax
    ac = (opz) autocompleteFunctions
    */
    loadSuggestionS2(uf, pf, ac, item, params) {

        var itemId = params.itemId; //Id del campo che stiamo usando
        var placeholder = $('#' + itemId).attr('data-placeholder');
        var rotta = params.rotta;
        var type = params.type;

        //console.log(rotta);

        $('.' + item)
            .select2({
                language: 'it',
                minimumInputLength: 3,
                allowClear: true,
                //width: '100%',
                containerCssClass: 'custom-select',
                //dropdownCssClass: '',
                //adaptContainerCssClass: '',
                //adaptDropdownCssClass: '',
                //containerCss: '',
                placeholder: placeholder,
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: rotta,
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            keywords: params.term
                        };
                        // Query parameters will be ?keywords=[term]
                        return query;
                    },
                    processResults: function (response) {
                        //console.log(type);
                        switch (type) {
                            //Elenco clienti
                            case 'customers':
                                var select2Data = $.map(response.customers, function (obj) {
                                    var text = obj.title != null ? obj.title + ' ' + obj.surname + ' ' + obj.name : obj.surname + ' ' + obj.name;
                                    text = obj.cf != null ? text + ' - CF: ' + obj.cf : text;

                                    obj.id = obj.id;
                                    obj.text = text
                                    obj.tipo = type;

                                    return obj;
                                });
                                break;

                                //Elenco operatori
                            case 'operators':

                                var select2Data = $.map(response.operators, function (obj) {
                                    var text = obj.title != null ? obj.title + ' ' + obj.name + ' ' + obj.surname : obj.name + ' ' + obj.surname;

                                    obj.id = obj.id;
                                    obj.text = text
                                    obj.tipo = type;

                                    return obj;
                                });
                                break;
                        }


                        return {
                            results: select2Data
                            /*
                    pagination: {
                        more: data.result.more
                    }
                    */
                        };
                    }
                },
                //placeholder: 'Search for a repository',
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                templateResult: uf.formatRepo,
                templateSelection: uf.formatRepoSelection
            })

            .on('select2:select', function (e) {
                var data = e.params.data; //Dati del record

                //Setto i campi hidden in base alla form che sta usando la select 2
                switch (params.type) {
                    //Form crea nuovo piano. Cerco cliente
                    case "customers":
                        var hiddenfield = $('#' + itemId).attr('data-hidden-field'); //Campo nascosto che memorizza l'id dell'elemento selezionato
                        var campi = [{
                            field_id: hiddenfield,
                            value: data.id //Customer id
                        }, ];

                        //console.log(campi);
                        ac.settaCampi(campi, data.id);
                        break;

                        //Form crea nuovo piano. Cerco operatore
                    case "operators":
                        var hiddenfield = $('#' + itemId).attr('data-hidden-field'); //Campo nascosto che memorizza l'id dell'elemento selezionato
                        var campi = [{
                            field_id: hiddenfield,
                            value: data.id
                        }, ];

                        //console.log(campi);
                        ac.settaCampi(campi);
                        break;

                }
            })

            .maximizeSelect2Height(); //Plugin per settare altezza select2
    }

    formatRepo(repo) {
        /*
        console.log('repo:' + repo);

        if (repo.tipo == 'products-availability') {
            var value = repo.famiglia + " " + repo.nome;
            var id = repo.famiglia + " " + repo.nome;
        } else {
            var value = repo.nome + " " + repo.cognome + " (" + repo.email + ")";
            var id = repo.id;
        }
      

        markup = '<li id="' + id + '" value="' + value + '">' + value + "</li>";
        return markup;
        */
    }

    formatRepoSelection(repo) {
        //console.log('repo:' + repo);
        return repo.value || repo.text;

    }



    /*
    Funzione che setta alcuni campi quando si seleziona una scelta dell'autocomplete
    Params: (array) campi: 
        - id_campo, 
        - valore_campo
    */
    settaCampi(campi, customerId) {
        //console.log(campi);
        $.each(campi, function (index, data) {
            $('#' + data['field_id']).val(data['value']);
        });

        //Creazione piano. Aggiorno i tipi di reminders in base al cliente selezionato
        
        if (customerId > 0) {
            var rotta = $('#customer').attr('data-route-reminders');

            var postParams = {
                'type': 'POST',
                'rotta': rotta,
                'data': {
                    'customerId': customerId,
                },
                'onSuccess': 'filterRemindersList',
                'params': '',
            }

            //console.log(postParams);

            uf.ajaxPost(postParams);
        }
    }

}

export default autocompleteFunctions // ES6