$(function () {

    //Init standalone file-manager
    var route_prefix = "/filemanager";

    $('#upload_logo').filemanager('image', {
        prefix: route_prefix
    });


    //Carico immagine
    $(document).on('change', '#company_logo', function (e) {

        e.preventDefault();
        var img = $(this).val();
        if (img !== '') {
            var btn = $(this).attr('data-bnt-delete');
            $('#' + btn).show();
        }
    });

    //Elimino immagine temporanea
    $(document).on('click', '.delete-tmp-image', function (e) {

        e.preventDefault();
        var holder = $(this).attr('data-holder');
        $('#' + holder).attr('src', '');
        $(this).hide();
        $('#company_logo').val('');

    });

    //Elimino logo
    $(document).on('click', '.delete-image', function (e) {

        e.preventDefault();

        uf.deleteCoverImage(this, uf, pf);
    });




})
