$(function () {

    //Init datatable
    uf.dataTable('table-paginate', uf, pf);

    //Multiselect
    uf.bsmultiselect('multiselect');

    //Elimino newsletter
    $(document).on('click', '.delete-item', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateMailinglistList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });


     //Clear file input
     $(document).on('click', '.clear-attachment', function (e) {
        $('[data-toggle="tooltip"]').tooltip('hide');
        event.preventDefault();
        var inputId = $(this).attr('data-input-id');

        //alert(inputId);
        $('#' + inputId).val(''); //Clear input
        $('#lbl_' + inputId).html(''); //Clear label
        $('#lbl2_' + inputId).html(''); //Clear label

    })


    //Invio mailinglist
    $(document).on('click', '.mailing-send', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateMailinglistList',
            'params': '',
        }

        //console.log(postParams);

        uf.confirmBox(confirmParams, postParams, uf, pf);


    })

    //Validazione form
    $(document).on('click', '.form-send', function (e) {

        e.preventDefault();

        var formId = $(this).attr('data-formid');

        //Campi da validare
        var rules = {
            subject: {
                required: true,
                minlength: 2,
            },

        };

        var messages = {
            subject: {
                required: translator.trans('messages.validations.subject_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },

        };

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })

})
