$(function() {

    //Multiselect
    uf.bsmultiselect('multiselect');


    //Attivo/disattivo visible
    $(document).on('click', '.menu-visible-switch', function(e) {

        e.preventDefault();

        var visible = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_visible = visible == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_visible': is_visible,
            },
            'onSuccess': 'updateMenusList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });


    //Elimino menu
    $(document).on('click', '.delete-item', function(e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateMenusList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Validazione form
    $(document).on('click', '.form-send', function (e) {

        e.preventDefault();

        var formId = $(this).attr('data-formid');

        //Campi da validare
        var rules = {
            name: {
                required: true,
                minlength: 2,
            },

        };

        var messages = {
            name: {
                required: translator.trans('messages.validations.name_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },

        };

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })

})
