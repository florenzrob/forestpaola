$(function () {

    //Init dropzone
    loadDropzone('dropzone_images');

    //Init datatable
    uf.dataTable('table-paginate');

    //Elimino immagine cover
    $(document).on('click', '.delete-image', function (e) {

        e.preventDefault();

        var rotta = $(this).attr('data-route');
        var filename = $(this).attr('data-filename');
        var containerDiv = $(this).attr('data-container-div');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'filename': filename,
                'containerDiv': containerDiv,
            },
            'onSuccess': 'updateImagesList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Seleziono immagine da aggiungere alla gallery
    $(document).on('click', '.check-image', function (e) {

        e.preventDefault();


        var id = $(this).attr('data-id');
        var loop = $(this).attr('data-loop');
        var imageId = $(this).attr('data-image-id');
        var filename = $(this).attr('data-filename');

        var cssClasses = 'border border-info border-selected';

        if ($(this).hasClass('text-info')) {
            //Deseleziono
            $('#' + id).val('');
            $('#' + imageId).removeClass(cssClasses);
            $(this).addClass('text-muted');
            $(this).removeClass('text-info');
            $('#edit_' + loop).addClass('text-muted');
            $('#edit_' + loop).removeClass('text-info');

        } else {
            //Seleziono
            $('#' + id).val(filename);
            $('#' + imageId).addClass(cssClasses);
            $(this).addClass('text-info');
            $(this).removeClass('text-muted');
            $('#edit_' + loop).addClass('text-info');
            $('#edit_' + loop).removeClass('text-muted');

        }
    });



    //Carico contenuti nella select page/article/category durante creazione/modifica di una gallery
    $(document).on('change', '#mediarelatedmodel_id', function (e) {

        e.preventDefault();

        var modelId = $(this).val();
        //var modelName = $(this).attr('data-model');
        var rotta = $(this).attr('data-route');

        //console.log(modelName);

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                //'modelName': modelName,
                'modelId': modelId,
            },
            'onSuccess': 'getRelatedRecords',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);


    });


    //Apro modale per aggiungere immagini alla gallery
    $(document).on('click', '#open_images', function (e) {

        e.preventDefault();

        var rotta = $(this).attr('data-route');
        var galleryId = $(this).attr('data-gallery-id');
        var rotta_add = $(this).attr('data-route-add-images');

        var dialogParams = {
            'title': $(this).attr('data-dialog-title'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'galleryId': galleryId,
            },
            //'onSuccess': 'updateImagesList',
            'params': {
                'images_route': rotta_add,
                'galleryId': galleryId,

            }
        }

        //console.log(galleryId);
        uf.dialogGalleryBox(dialogParams, postParams, uf, pf, ac, te);

    });


     //Aggiorno contenuto modale quando navigo nelle cartelle
     $(document).on('click', '#navigate-folder', function (e) {

        e.preventDefault();

        var rotta = $(this).attr('data-route');
        var folder = $(this).attr('data-folder');

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'folder': folder,
            },
            'onSuccess': 'updateImagesGrid',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);

    });



    //Elimino immagine da una gallery
    $(document).on('click', '.delete-gallery-item', function (e) {

        e.preventDefault();

        var rotta = $(this).attr('data-route');
        //var itemId = $(this).attr('data-id');
        var galleryId = $(this).attr('data-gallery-id');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                //'itemId': itemId,
                'galleryId': galleryId,
            },
            'onSuccess': 'updateTableItemsContainer',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Elimino gallery
    $(document).on('click', '.delete-gallery', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateGalleryList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });


})
