$(function () {

    //Init datatable
    uf.dataTable('table-paginate', uf, pf);

    //Abilito datetime range
    uf.dateRange('publish_from', 'publish_to');

    //Multiselect
    uf.bsmultiselect('multiselect');

    //Init standalone file-manager
    var route_prefix = "/filemanager";
    $('#upload_cover').filemanager('image', {
        prefix: route_prefix
    });

    //Carico immagine
    $(document).on('change', '#image_cover', function (e) {

        e.preventDefault();
        var img = $(this).val();
        if (img !== '') {
            var btn = $(this).attr('data-bnt-delete');
            $('#' + btn).show();
        }
    });

    //Elimino immagine temporanea
    $(document).on('click', '.delete-tmp-image', function (e) {

        e.preventDefault();
        var holder = $(this).attr('data-holder');
        $('#' + holder).attr('src', '');
        $(this).hide();
        $('#image_cover').val('');
    });


    //Abilito/Disabilito publish_to
    $(document).on('change', '#publish_to', function (e) {

        var disabled = $(this).val() == false ? true : false;

        $('#date_to').prop('disabled', disabled);
        if (disabled) {
            $('#date_to').val('');
        }
    });

    //Attivo/disattivo visible
    $(document).on('click', '.article-visible-switch', function (e) {

        e.preventDefault();

        var visible = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_visible = visible == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_visible': is_visible,
            },
            'onSuccess': 'updateArticlesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });


    //Attivo/disattivo is_private
    $(document).on('click', '.article-private-switch', function (e) {

        e.preventDefault();

        var isprivate = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_private = isprivate == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_private': is_private,
            },
            'onSuccess': 'updateArticlesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });

    //Attivo/disattivo show_sidebar_dx
    $(document).on('click', '.article-sidebardx-switch', function (e) {

        e.preventDefault();

        var showsidebar = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var show_sidebar_dx = showsidebar == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'show_sidebar_dx': show_sidebar_dx,
            },
            'onSuccess': 'updateArticlesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });


    //Elimino articolo
    $(document).on('click', '.delete-item', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updateArticlesList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Elimino immagine cover
    $(document).on('click', '.delete-image', function (e) {

        e.preventDefault();

        uf.deleteCoverImage(this, uf, pf);
    });


    //Validazione form
    $(document).on('click', '.form-send', function (e) {

        e.preventDefault();

        var formId = $(this).attr('data-formid');

        //Campi da validare
        var rules = {
            title: {
                required: true,
                minlength: 2,
            },

        };

        var messages = {
            title: {
                required: translator.trans('messages.validations.title_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },

        };

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })

})
