$(function () {

    //Multiselect
    uf.bsmultiselect('multiselect');

    //Init datatable
    uf.dataTable('table-paginate', uf, pf);

    //Init standalone file-manager
    var route_prefix = "/filemanager";
    $('#upload_cover').filemanager('image', {
        prefix: route_prefix
    });

    //Carico immagine
    $(document).on('change', '#image_cover', function (e) {

        e.preventDefault();
        var img = $(this).val();
        if (img !== '') {
            var btn = $(this).attr('data-bnt-delete');
            $('#' + btn).show();
        }
    });

    //Elimino immagine temporanea
    $(document).on('click', '.delete-tmp-image', function (e) {

        e.preventDefault();
        var holder = $(this).attr('data-holder');
        $('#' + holder).attr('src', '');
        $(this).hide();
        $('#image_cover').val('');
    });


    //Attivo/disattivo is_home
    $(document).on('click', '.page-ishome-switch', function (e) {

        e.preventDefault();

        var ishome = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_home = ishome == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_home': is_home,
            },
            'onSuccess': 'updatePagesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });

    //Attivo/disattivo on_home
    $(document).on('click', '.page-onhome-switch', function (e) {

        e.preventDefault();

        var onhome = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var on_home = onhome == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'on_home': on_home,
            },
            'onSuccess': 'updatePagesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });

    //Attivo/disattivo visible
    $(document).on('click', '.page-visible-switch', function (e) {

        e.preventDefault();

        var visible = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_visible = visible == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_visible': is_visible,
            },
            'onSuccess': 'updatePagesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });


    //Attivo/disattivo is_private
    $(document).on('click', '.page-private-switch', function (e) {

        e.preventDefault();

        var isprivate = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var is_private = isprivate == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'is_private': is_private,
            },
            'onSuccess': 'updatePagesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });


    //Attivo/disattivo show_sidebar_dx
    $(document).on('click', '.page-sidebardx-switch', function (e) {

        e.preventDefault();

        var showsidebar = $(this).is(':checked');
        var rotta = $(this).attr('data-route');

        var show_sidebar_dx = showsidebar == true ? 1 : 0;

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'show_sidebar_dx': show_sidebar_dx,
            },
            'onSuccess': 'updatePagesList',
            'params': '',
        }

        uf.ajaxPost(uf, pf, postParams);
    });



    //Elimino pagina
    $(document).on('click', '.delete-item', function (e) {

        e.preventDefault();

        var id = $(this).attr('data-id');
        var rotta = $(this).attr('data-route');

        var confirmParams = {
            'title': $(this).attr('data-confirm-title'),
            'message': $(this).attr('data-confirm-message'),
            'btnConfirm': $(this).attr('data-btn-confirm'),
            'btnDismiss': $(this).attr('data-btn-dismiss'),
        }

        var postParams = {
            'type': 'POST',
            'uri': rotta,
            'data': {
                'id': id,
            },
            'onSuccess': 'updatePagesList',
            'params': '',
        }

        uf.confirmBox(confirmParams, postParams, uf, pf);

    });

    //Elimino immagine cover
    $(document).on('click', '.delete-image', function (e) {

        e.preventDefault();

        uf.deleteCoverImage(this, uf, pf);
    });


    //Validazione form
    $(document).on('click', '.form-send', function (e) {

        e.preventDefault();

        var formId = $(this).attr('data-formid');


        //Campi da validare
        var rules = {
            title: {
                required: true,
                minlength: 2,
            },

        };

        var messages = {
            title: {
                required: translator.trans('messages.validations.title_required'),
                minlength: jQuery.validator.format(translator.trans('messages.validations.min_lenght', {
                    number: '{0}'
                })),
            },

        };

        var success = ff.validateForm(formId, rules, messages);

        if (success) { //Validazione
            ff.sendForm(formId, uf, pf);
        }
    })
})
