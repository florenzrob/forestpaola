<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();


/*
 *****************************************************************
 *                                                               *
 *                             PUBLIC                            *
 *                                                               *
 *****************************************************************
 */

Route::get('/', 'MainController@index')->name('home');
//Route::get('/', 'MainController@wip')->name('home');

Route::get('/login', 'Auth\LoginController@index')->name('login');

//Show single page
Route::get('/page/{query}', 'PageController@getPage')->where('query', '.+')->name('page');

//Show single article
//Route::get('/article/{query}/{articleSlug}', 'ArticleController@show')->where('query', '.+')->where('articleSlug', '.+')->name('article');
Route::get('/article/{articleSlug}', 'ArticleController@show')->where('articleSlug', '.+')->name('article');

//Privacy
Route::get('/disclaimer/privacy', 'MainController@privacy')->name('disclaimer.privacy');

//Send contacts
Route::post('/ajax/contact/', 'MailController@send')->name('contacts.send');

//Subscribe
Route::post('/mailinglist/subscribe', 'SubscriberController@subscribe')->name('mailinglist.subscribe');

//Unsubscribe
Route::get('/mailinglist/unsubscribe', 'MainController@unsubscribe')->name('mailinglist.unsubscribe');

//Remove from mailinglist
Route::post('/ajax/mailinglist/remove', 'MailinglistController@unsubscribe')->name('mailinglist.remove');

/*
 *****************************************************************
 *                                                               *
 *                             ADMIN                             *
 *                                                               *
 *****************************************************************
 */

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    /*
     **********************************************************
     * PHPINFO
     **********************************************************
     */
    //Route::get('test/phpinfo', 'HomeController@phpVersion')->name('test.phpinfo');

    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    /*
     *****************************************************************
     * USERS
     *****************************************************************
     */

    //List
    Route::get('/users/list', 'UserController@index')->name('users.list');

    //New
    Route::get('/users/new', 'UserController@create')->name('user.create');

    //Store
    Route::post('/users/store', 'UserController@store')->name('user.store');

    //Edit
    Route::get('/users/edit/{id}', 'UserController@edit')->name('user.edit');

    //Update
    Route::post('/users/update/{id}', 'UserController@update')->name('user.update');

    //Delete
    Route::post('/ajax/users/delete/{id}', 'UserController@destroy')->name('user.delete');

    //Change password
    Route::get('/users/password/edit/{id}', 'UserController@editPassword')->name('user.password.edit');

    //Update password
    Route::post('/users/password/update/{id}', 'UserController@updatePassword')->name('user.update.password');

    //Update is_admin status
    Route::post('/ajax/users/update/is_admin/{id}', 'UserController@updateIsAdmin')->name('user.update.is_admin');

    //Update active status
    Route::post('/ajax/users/update/active/{id}', 'UserController@updateActive')->name('user.update.active');

    //Delete profile picture
    Route::post('/ajax/users/picture/delete', 'UserController@deletepicture')->name('user.picture.delete');

    /*
     *****************************************************************
     * SETTINGS
     *****************************************************************
     */

    //Store
    Route::post('/settings/store', 'SettingsController@store')->name('settings.store');

    //Edit
    Route::get('/settings/edit', 'SettingsController@edit')->name('settings.edit');

    //Update
    Route::post('/settings/update', 'SettingsController@update')->name('settings.update');

    //Delete logo
    Route::post('/ajax/settings/logo/delete', 'SettingsController@deletelogo')->name('settings.logo.delete');



    /*
     *****************************************************************
     * TEMPLATES
     *****************************************************************
     */

    //List
    Route::get('/templates/list', 'TemplateController@index')->name('templates.list');

    //New
    Route::get('/templates/new', 'TemplateController@create')->name('template.create');

    //Store
    Route::post('/templates/store', 'TemplateController@store')->name('template.store');

    //Edit
    Route::get('/templates/edit/{id}', 'TemplateController@edit')->name('template.edit');

    //Update
    Route::post('/templates/update/{id}', 'TemplateController@update')->name('template.update');

    //Delete
    Route::post('/ajax/templates/delete/{id}', 'TemplateController@destroy')->name('template.delete');

    /*
     *****************************************************************
     * POSITIONS
     *****************************************************************
     */

    //List
    Route::get('/positions/list', 'PositionController@index')->name('positions.list');

    //New
    Route::get('/positions/new', 'PositionController@create')->name('position.create');

    //Store
    Route::post('/positions/store', 'PositionController@store')->name('position.store');

    //Edit
    Route::get('/positions/edit/{id}', 'PositionController@edit')->name('position.edit');

    //Update
    Route::post('/positions/update/{id}', 'PositionController@update')->name('position.update');

    //Delete
    Route::post('/ajax/positions/delete/{id}', 'PositionController@destroy')->name('position.delete');

    //Update active status
    Route::post('/ajax/positions/update/active/{id}', 'PositionController@updateActive')->name('position.update.active');

    /*
     *****************************************************************
     * MENUS
     *****************************************************************
     */

    //List
    Route::get('/menus/list', 'MenuController@index')->name('menus.list');

    //New
    Route::get('/menus/new', 'MenuController@create')->name('menu.create');

    //Store
    Route::post('/menus/store', 'MenuController@store')->name('menu.store');

    //Edit
    Route::get('/menus/edit/{id}', 'MenuController@edit')->name('menu.edit');

    //Update
    Route::post('/menus/update/{id}', 'MenuController@update')->name('menu.update');

    //Delete
    Route::post('/ajax/menus/delete/{id}', 'MenuController@destroy')->name('menu.delete');

    //Update visible status
    Route::post('/ajax/menus/update/visible/{id}', 'MenuController@updateVisible')->name('menu.update.visible');

    /*
     *****************************************************************
     * PAGES
     *****************************************************************
     */

    //List
    Route::get('/pages/list', 'PageController@index')->name('pages.list');

    //New
    Route::get('/pages/new', 'PageController@create')->name('page.create');

    //Store
    Route::post('/pages/store', 'PageController@store')->name('page.store');

    //Edit
    Route::get('/pages/edit/{id}', 'PageController@edit')->name('page.edit');

    //Update
    Route::post('/pages/update/{id}', 'PageController@update')->name('page.update');

    //Delete
    Route::post('/ajax/pages/delete/{id}', 'PageController@destroy')->name('page.delete');

    //Update is_home status
    Route::post('/ajax/pages/update/is_home/{id}', 'PageController@updateIsHome')->name('page.update.is_home');

    //Update on_home status
    Route::post('/ajax/pages/update/on_home/{id}', 'PageController@updateOnHome')->name('page.update.on_home');

    //Update visible status
    Route::post('/ajax/pages/update/visible/{id}', 'PageController@updateVisible')->name('page.update.visible');

    //Update is_private status
    Route::post('/ajax/pages/update/is_private/{id}', 'PageController@updateIsPrivate')->name('page.update.is_private');

    //Update show_sidebar_dx status
    Route::post('/ajax/pages/update/show_sidebar_dx/{id}', 'PageController@updateShowSidebarDx')->name('page.update.show_sidebar_dx');

    //Delete cover
    Route::post('/ajax/pages/cover/delete', 'PageController@deleteCover')->name('page.cover.delete');

    //Reorder
    Route::post('/ajax/pages/reorder', 'PageController@reorder')->name('page.reorder');


    /*
     *****************************************************************
     * CATEGORIES
     *****************************************************************
     */

    //List
    Route::get('/categories/list', 'CategoryController@index')->name('categories.list');

    //New
    Route::get('/categories/new', 'CategoryController@create')->name('category.create');

    //Store
    Route::post('/categories/store', 'CategoryController@store')->name('category.store');

    //Edit
    Route::get('/categories/edit/{id}', 'CategoryController@edit')->name('category.edit');

    //Update
    Route::post('/categories/update/{id}', 'CategoryController@update')->name('category.update');

    //Delete
    Route::post('/ajax/categories/delete/{id}', 'CategoryController@destroy')->name('category.delete');

    //Update visible status
    Route::post('/ajax/categories/update/visible/{id}', 'CategoryController@updateVisible')->name('category.update.visible');

    //Update is_private status
    Route::post('/ajax/categories/update/is_private/{id}', 'CategoryController@updateIsPrivate')->name('category.update.is_private');

    //Update show_sidebar_dx status
    Route::post('/ajax/categories/update/show_sidebar_dx/{id}', 'CategoryController@updateShowSidebarDx')->name('category.update.show_sidebar_dx');

    //Delete cover
    Route::post('/ajax/categories/cover/delete', 'CategoryController@deleteCover')->name('category.cover.delete');

    //Reorder
    Route::post('/ajax/categories/reorder', 'CategoryController@reorder')->name('category.reorder');

    /*
     *****************************************************************
     * ARTICLES
     *****************************************************************
     */

    //List
    Route::get('/articles/list', 'ArticleController@index')->name('articles.list');

    //New
    Route::get('/articles/new', 'ArticleController@create')->name('article.create');

    //Store
    Route::post('/articles/store', 'ArticleController@store')->name('article.store');

    //Edit
    Route::get('/articles/edit/{id}', 'ArticleController@edit')->name('article.edit');

    //Update
    Route::post('/articles/update/{id}', 'ArticleController@update')->name('article.update');

    //Delete
    Route::post('/ajax/articles/delete/{id}', 'ArticleController@destroy')->name('article.delete');

    //Update visible status
    Route::post('/ajax/articles/update/visible/{id}', 'ArticleController@updateVisible')->name('article.update.visible');

    //Update is_private status
    Route::post('/ajax/articles/update/is_private/{id}', 'ArticleController@updateIsPrivate')->name('article.update.is_private');

    //Update show_sidebar_dx status
    Route::post('/ajax/articles/update/show_sidebar_dx/{id}', 'ArticleController@updateShowSidebarDx')->name('article.update.show_sidebar_dx');

    //Delete cover
    Route::post('/ajax/articles/cover/delete', 'ArticleController@deleteCover')->name('article.cover.delete');

    //Reorder
    Route::post('/ajax/articles/reorder', 'ArticleController@reorder')->name('article.reorder');

    /*
     *****************************************************************
     * TAGS
     *****************************************************************
     */

    //List
    Route::get('/tags/list', 'TagController@index')->name('tags.list');

    //New
    Route::get('/tags/new', 'TagController@create')->name('tag.create');

    //Store
    Route::post('/tags/store', 'TagController@store')->name('tag.store');

    //Edit
    Route::get('/tags/edit/{id}', 'TagController@edit')->name('tag.edit');

    //Update
    Route::post('/tags/update/{id}', 'TagController@update')->name('tag.update');

    //Delete
    Route::post('/ajax/tags/delete/{id}', 'TagController@destroy')->name('tag.delete');

    //Update system status
    Route::post('/ajax/tags/update/system/{id}', 'TagController@updateSystem')->name('tag.update.system');


    /*
     *****************************************************************
     * MAILING LIST GROUPS
     *****************************************************************
     */

    //List
    Route::get('/mlgroups/list', 'MlgroupController@index')->name('mlgroups.list');

    //New
    Route::get('/mlgroups/new', 'MlgroupController@create')->name('mlgroup.create');

    //Store
    Route::post('/mlgroups/store', 'MlgroupController@store')->name('mlgroup.store');

    //Edit
    Route::get('/mlgroups/edit/{id}', 'MlgroupController@edit')->name('mlgroup.edit');

    //Update
    Route::post('/mlgroups/update/{id}', 'MlgroupController@update')->name('mlgroup.update');

    //Delete
    Route::post('/ajax/mlgroups/delete/{id}', 'MlgroupController@destroy')->name('mlgroup.delete');

    //Update visible status
    Route::post('/ajax/mlgroups/update/visible/{id}', 'MlgroupController@updateActive')->name('mlgroup.update.visible');

    /*
     *****************************************************************
     * MAILING LIST SUBSCRIBERS
     *****************************************************************
     */

    //List
    Route::get('/subscribers/list', 'SubscriberController@index')->name('subscribers.list');

    //New
    Route::get('/subscribers/new', 'SubscriberController@create')->name('subscriber.create');

    //Store
    Route::post('/subscribers/store', 'SubscriberController@store')->name('subscriber.store');

    //Edit
    Route::get('/subscribers/edit/{id}', 'SubscriberController@edit')->name('subscriber.edit');

    //Update
    Route::post('/subscribers/update/{id}', 'SubscriberController@update')->name('subscriber.update');

    //Delete
    Route::post('/ajax/subscribers/delete/{id}', 'SubscriberController@destroy')->name('subscriber.delete');

    //Update active status
    Route::post('/ajax/subscribers/update/active/{id}', 'SubscriberController@updateActive')->name('subscriber.update.active');

    /*
     *****************************************************************
     * MAILING LIST
     *****************************************************************
     */

    //List
    Route::get('/mailinglist/list', 'MailinglistController@index')->name('mailinglists.list');

    //New
    Route::get('/mailinglist/new', 'MailinglistController@create')->name('mailinglist.create');

    //Store
    Route::post('/mailinglist/store', 'MailinglistController@store')->name('mailinglist.store');

    //Edit
    Route::get('/mailinglist/edit/{id}', 'MailinglistController@edit')->name('mailinglist.edit');

    //Update
    Route::post('/mailinglist/update/{id}', 'MailinglistController@update')->name('mailinglist.update');

    //Delete
    Route::post('/ajax/mailinglist/delete/{id}', 'MailinglistController@destroy')->name('mailinglist.delete');

    //Send
    Route::post('/ajax/mailinglist/send', 'MailinglistController@send')->name('mailinglist.send');
    //Route::get('/ajax/mailinglist/send1/{id}', 'MailinglistController@send')->name('mailinglist.send1');

    //Esiti invii mailinglist
    Route::get('/mailinglist/sent/list', 'MailinglistSentController@index')->name('mailinglists.sent.list');

    //Mostra mail
    Route::get('mailinglist/test/showmail', 'MailinglistController@previewEmail')->name('mailinglist.test.showmail');



    /*
     ********************************************************************************************************************************************************
     *                                                                                                                                                      *
     *                                                                                                                                                      *
     *                                                                                                                                                      *
     * GESTIONE MEDIA                                                                                                                                       *
     *                                                                                                                                                      *
     *                                                                                                                                                      *
     *                                                                                                                                                      *
     ********************************************************************************************************************************************************
     */

    /*
     *****************************************************************
     * IMAGES
     *****************************************************************
     */

    //List
    Route::get('/media/images/list', 'MediaController@getImages')->name('images.list');

    //Check
    Route::post('/ajax/media/images/select/', 'MediaController@selectImage')->name('image.select');

    //Delete
    Route::post('/ajax/media/images/delete/', 'MediaController@destroyImage')->name('image.delete');

    /*
     *****************************************************************
     * GALLERIES
     *****************************************************************
     */

    //List
    Route::get('/media/galleries/list', 'MediaController@index')->name('galleries.list');

    //New
    Route::get('/media/galleries/new', 'MediaController@create')->name('gallery.create');

    //Store
    Route::post('/media/galleries/store', 'MediaController@store')->name('gallery.store');

    //Edit
    Route::get('/media/galleries/edit/{id}', 'MediaController@edit')->name('gallery.edit');

    //Update
    Route::post('/media/galleries/update/{id}', 'MediaController@update')->name('gallery.update');

    //Delete gallery
    Route::post('/ajax/media/galleries/delete/', 'MediaController@destroyGallery')->name('gallery.delete');

    //Carica select record
    Route::post('/ajax/media/galleries/getrecords/', 'MediaController@getRecords')->name('gallery.getrecords');

    //Apre modale per selezione immagini
    Route::post('/ajax/media/galleries/openmodal/', 'MediaController@openImagesModal')->name('gallery.openmodal');

    //Aggiorna griglia immagini navigando tra le cartelle
    Route::post('/ajax/media/galleries/openfolder/', 'MediaController@navigateFoler')->name('gallery.openfolder');

    //Aggiorna griglia immagini navigando tra le cartelle no ajax
    Route::get('/media/galleries/openfolder1/', 'MediaController@navigateFoler')->name('gallery.openfolder1');


    //Aggiungo immagini selezionate
    Route::post('/ajax/media/galleries/images/add/', 'MediaController@addImages')->name('gallery.images.add');

    //Delete gallery item
    Route::post('/ajax/media/galleries/delete/{id}', 'MediaController@destroyGalleryItem')->name('gallery.delete.item');

    /*
     *****************************************************************
     * DROPZONE
     *****************************************************************
     */

    Route::get('/dropzone/multifileupload', 'MediaController@index')->name('dropzone.show');
    Route::post('/dropzone/multifileupload', 'MediaController@storeFiles')->name('dropzone.store');

    /*
     *****************************************************************
     * UPLOADER
     *****************************************************************
     */
    Route::post('/ajax/uploader/store', 'UploadController@store')->name('uploader.store');
    Route::post('/ajax/uploader/destroy', 'UploadController@destroy')->name('uploader.delete');

    /*
     *****************************************************************
     * TINYMCE UPLOAD
     *****************************************************************
     */

    Route::get('filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show')->name('filemanager-show');
    Route::post('filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload')->name('filemanager-upload');

    /*
    *****************************************************************
    * REMOVE IMAGE
    *****************************************************************
    */

     /*
     *****************************************************************
     * TEST CRON - CHIAMABILI DA BACKEND
     *****************************************************************
     */
    //Test funzionamento cron
    Route::get('cron/test', 'MailinglistController@testCron')->name('test.cron');

    //Invio mailinglist
    Route::get('cron/sendingmailinglist', 'MailinglistController@queueMailinglist')->name('test.cron.sendingmailinglist');

});
