<?php

use Illuminate\Database\Seeder;

class ArticlesPositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('article_position')->insert([
            [
                //1 
                'article_id' => 5,
                'position_id' => 2,
            ],
        ]
        );
    }
}
