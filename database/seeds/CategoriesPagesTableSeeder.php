<?php

use Illuminate\Database\Seeder;

class CategoriesPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('category_page')->insert([
            [
                //1
                'category_id' => 1,
                'page_id' => 4,
            ],
            [
                //2
                'category_id' => 2,
                'page_id' => 5,
            ],

        ]
        );
    }
}
