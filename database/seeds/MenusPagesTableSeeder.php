<?php

use Illuminate\Database\Seeder;

class MenusPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('menu_page')->insert([
            [
                //1
                'menu_id' => 1,
                'page_id' => 1,
                'sort' => 1,
                'visible' => 1,
            ],
            [
                //2
                'menu_id' => 1,
                'page_id' => 2,
                'sort' => 2,
                'visible' => 1,
            ],
            [
                //3
                'menu_id' => 1,
                'page_id' => 3,
                'sort' => 3,
                'visible' => 1,
            ],
            [
                //4
                'menu_id' => 1,
                'page_id' => 4,
                'sort' => 4,
                'visible' => 1,
            ],
            [
                //5
                'menu_id' => 1,
                'page_id' => 5,
                'sort' => 5,
                'visible' => 1,
            ],
            [
                //6
                'menu_id' => 2,
                'page_id' => 1,
                'sort' => 1,
                'visible' => 1,
            ],
            [
                //7
                'menu_id' => 2,
                'page_id' => 2,
                'sort' => 2,
                'visible' => 1,
            ],
            [
                //8
                'menu_id' => 2,
                'page_id' => 3,
                'sort' => 3,
                'visible' => 1,
            ],
            [
                //9
                'menu_id' => 2,
                'page_id' => 4,
                'sort' => 4,
                'visible' => 1,
            ],
            [
                //10
                'menu_id' => 2,
                'page_id' => 5,
                'sort' => 5,
                'visible' => 1,
            ],

        ]
        );
    }
}
