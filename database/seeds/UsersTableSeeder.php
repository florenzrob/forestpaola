<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            [
                //1
                'name' => 'R',
                'surname' => 'M',
                'username' => 'admin',
                'gender' => 'M',
                'email' => 'admin@forestpaola.it',
                'password' => bcrypt('admin'),
                'active' => true,
                'is_admin' => true,
            ],
            [
                //2
                'name' => 'Paola',
                'surname' => 'Barducci',
                'username' => 'mochena19',
                'gender' => 'F',
                'email' => 'infopb@forestpaola.it',
                'password' => bcrypt('mochena19'),
                'active' => true,
                'is_admin' => true,
            ],
        ]
        );
    }
}
