<?php

use Illuminate\Database\Seeder;

class TemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('templates')->insert([
            [
                //1 Template home
                'title' => 'Template home',
                'description' => 'Template per la pagina home',
                'template_name' => 'home',
            ],
            [
                //2 Template pagine
                'title' => 'Template Pagine',
                'template_name' => 'pages',
                'description' => 'Template per le pagine interne',
            ],
            [
                //3 Template contatti
                'title' => 'Template Contatti',
                'template_name' => 'contacts',
                'description' => 'Template per le pagine contatti',
            ],
            [
                //4 Template workshop
                'title' => 'Template Workshop',
                'template_name' => 'workshop',
                'description' => 'Template per le pagine workshop',
            ],

        ]
        );
    }
}
