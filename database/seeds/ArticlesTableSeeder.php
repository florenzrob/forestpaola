<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('articles')->insert([
            [
                //1
                'parent_id' => null,
                'category_id' => 1,
                'title' => 'I flinstones',
                'slug' => 'i-flinstones',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => 'foto1.jpg',
                'video_link' => null,
                'publish_from' => null,
                'publish_to' => null,
                'is_private' => 0,
                'sort' => 1,
                'visible' => 1,
            ],
            [
                //2
                'parent_id' => null,
                'category_id' => 1,
                'title' => 'Alla scoperta degli indiani',
                'slug' => 'alla-scoperta-degli-indiani',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => 'foto2.jpg',
                'video_link' => null,
                'publish_from' => null,
                'publish_to' => null,
                'is_private' => 0,
                'sort' => 2,
                'visible' => 1,
            ],
            [
                //3
                'parent_id' => null,
                'category_id' => 2,
                'title' => 'Colline nebbiose',
                'slug' => 'colline-nebbiose',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => 'foto3.jpg',
                'video_link' => null,
                'publish_from' => null,
                'publish_to' => null,
                'is_private' => 0,
                'sort' => 3,
                'visible' => 1,
            ],
            [
                //4
                'parent_id' => null,
                'category_id' => 1,
                'title' => 'Weekend al rifugio',
                'slug' => 'weekend-al-rifugio',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => 'foto4.jpg',
                'video_link' => null,
                'publish_from' => null,
                'publish_to' => null,
                'is_private' => 0,
                'sort' => 4,
                'visible' => 1,
            ],
            [
                //5
                'parent_id' => null,
                'category_id' => null,
                'title' => 'Chi sono',
                'slug' => 'chi-sono',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => 'paola.jpg',
                'video_link' => null,
                'publish_from' => null,
                'publish_to' => null,
                'is_private' => 0,
                'sort' => null,
                'visible' => 1,
            ]

        ]
        );
    }
}
