<?php

use Illuminate\Database\Seeder;

class ArticlesTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('article_tag')->insert([
            [
                //1 
                'article_id' => 1,
                'tag_id' => 1,
            ],
            [
                //2 
                'article_id' => 2,
                'tag_id' => 1,
            ],
            [
                //3 
                'article_id' => 3,
                'tag_id' => 1,
            ],
            [
                //4 
                'article_id' => 4,
                'tag_id' => 1,
            ],

        ]
        );
    }
}
