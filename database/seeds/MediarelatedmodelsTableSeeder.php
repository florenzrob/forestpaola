<?php

use Illuminate\Database\Seeder;

class MediarelatedmodelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('mediarelatedmodels')->insert([
            [
                //1
                'modelname' => 'Article',
                'description' => 'Tabella articoli',
            ],
            [
                //2
                'modelname' => 'Category',
                'description' => 'Tabella categorie',
            ],
            [
                //3
                'modelname' => 'Page',
                'description' => 'Tabella pagine',
            ],
        ]
        );
    }
}
