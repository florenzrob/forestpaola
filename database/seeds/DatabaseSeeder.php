<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /*
        IMPORTANT!!!!!
        IMPORTANT!!!!!
        IMPORTANT!!!!!
         ************************* RUN composer dump-autoload prima del seed se si aggiungono classi *************************

         ****************************************** Inserirli nell'ordine corretto ******************************************
        composer dump-autoload
        php artisan migrate
        php artisan db:seed
        php artisan db:seed --class=UsersTableSeeder per lanciare un singolo seed
         */

        $this->call([
            UsersTableSeeder::class,
            TemplatesTableSeeder::class,
            PagesTableSeeder::class,
            MenusTableSeeder::class,
            MenusPagesTableSeeder::class,
            TagsTableSeeder::class,
            CategoriesTableSeeder::class,
            ArticlesTableSeeder::class,
            ArticlesTagsTableSeeder::class,
            PositionsTableSeeder::class,
            ArticlesPositionsTableSeeder::class,
            CategoriesPagesTableSeeder::class,

            MediapositionsTableSeeder::class,
            MediarelatedmodelsTableSeeder::class,
            MediatypesTableSeeder::class,

        ]);
    }
}
