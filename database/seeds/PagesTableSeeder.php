<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pages')->insert([
            [
                //1 Chi sono
                'parent_id' => null,
                'template_id' => 1,
                'title' => 'Chi sono',
                'slug' => 'chi-sono',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 2,
            ],
            [
                //2 Attività
                'parent_id' => null,
                'template_id' => 1,
                'title' => 'Attività',
                'slug' => 'attivita',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 3,
            ],
            [
                //3 Contatti
                'parent_id' => null,
                'template_id' => 2,
                'title' => 'Contatti',
                'slug' => 'contatti',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 4,
            ],
            [
                //4 Attività -> Passeggiate e trekking
                'parent_id' => 2,
                'template_id' => 1,
                'title' => 'Passeggiate e Trekking',
                'slug' => 'passeggiate-e-trekking',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 1,
            ],
            [
                //5 Attività -> Laboratori
                'parent_id' => 2,
                'template_id' => 1,
                'title' => 'Laboratori',
                'slug' => 'laboratori',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 2,
            ],
            [
                //6 Attività -> Attività estive per bambini e ragazzi
                'parent_id' => 2,
                'template_id' => 1,
                'title' => 'Attività estive per bambini e ragazzi',
                'slug' => 'attivita-estive-per-bambini-e-ragazzi',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 3,
            ],
            [
                //7 Attività -> Gruppi scolastici
                'parent_id' => 2,
                'template_id' => 1,
                'title' => 'Gruppi scolastici',
                'slug' => 'gruppi-scolastici',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 4,
            ],
            [
                //8 Attività -> Collaborazioni
                'parent_id' => 2,
                'template_id' => 1,
                'title' => 'Collaborazioni',
                'slug' => 'collaborazioni',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 5,
            ],
            [
                //9 Attività -> Calendario Attività
                'parent_id' => 2,
                'template_id' => 3,
                'title' => 'Calendario Attività',
                'slug' => 'calendario-attivita',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 0,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 6,
            ],
            [
                //10 Home
                'parent_id' => null,
                'template_id' => 1,
                'title' => 'Home',
                'slug' => 'home',
                'subtitle' => null,
                'seo_title' => null,
                'seo_content' => null,
                'abstract' => 'Riassunto del contenuto.... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula...',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'is_home' => 1,
                'is_private' => 0,
                'on_home' => 0,
                'visible' => 1,
                'sort' => 0,
            ],

        ]
        );
    }
}
