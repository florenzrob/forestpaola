<?php

use Illuminate\Database\Seeder;

class MediapositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('mediapositions')->insert([
            [
                //1
                'name' => 'header',
                'description' => 'All\'inizio della pagina',
            ],
            [
                //2
                'name' => 'footer',
                'description' => 'In fondo alla pagina',
            ],

        ]
        );
    }
}
