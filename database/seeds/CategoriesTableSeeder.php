<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            [
                //1
                'parent_id' => null,
                'title' => 'Escursioni',
                'slug' => 'escursioni',
                'subtitle' => null,
                'abstract' => 'Riassunto del contenuto.... Qui raccologo le escursioni',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => null,
                'video_link' => null,
                'is_private' => 0,
                'sort' => 1,
                'visible' => 1,
            ],
            [
                //2
                'parent_id' => null,
                'title' => 'Attività',
                'slug' => 'attivita',
                'subtitle' => null,
                'abstract' => 'Riassunto del contenuto.... Qui raccolgo le attività',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => null,
                'video_link' => null,
                'is_private' => 0,
                'sort' => 2,
                'visible' => 1,
            ],
            [
                //3
                'parent_id' => null,
                'title' => 'Di tutto un po',
                'slug' => 'di-tutto-un-po',
                'subtitle' => null,
                'abstract' => 'Riassunto del contenuto.... Qui raccolgo un po\' di tutto',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel odio quis dui venenatis laoreet vitae vel ligula. Duis faucibus massa quis dolor consectetur, non facilisis nulla porta. Sed imperdiet faucibus vestibulum. Etiam dui turpis, vehicula ut sollicitudin vitae, volutpat et mauris. Pellentesque a nunc ut ligula euismod elementum. Nullam tincidunt elementum auctor. Mauris leo justo, tempor vitae lorem elementum, dignissim suscipit nisi. Morbi dictum, diam sit amet pharetra vestibulum, leo magna lacinia nibh, vel rhoncus erat odio vel ligula. Nam nec hendrerit orci. Donec viverra, quam eu venenatis pulvinar, felis dolor ullamcorper mauris, eget euismod magna arcu quis dui. Vivamus vitae rutrum erat. Pellentesque ultricies sapien sem, ut porttitor metus lacinia quis. Vivamus non mi neque. Etiam sollicitudin iaculis sem. Nunc faucibus neque eu quam dictum egestas.',
                'image_cover' => null,
                'video_link' => null,
                'is_private' => 0,
                'sort' => 3,
                'visible' => 1,
            ],
           
        ]
        );
    }
}
