<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('menus')->insert([
            [
                //1 Top Menu
                'parent_id' => null,
                'name' => 'Top Menu',
                'sort' => 1,
                'visible' => 1,
            ],
            [
                //1 Footer Menu
                'parent_id' => null,
                'name' => 'Footer Menu',
                'sort' => 2,
                'visible' => 1,
            ],
        ]
        );
    }
}
