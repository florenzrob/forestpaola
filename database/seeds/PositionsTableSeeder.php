<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('positions')->insert([
            [
                //1 
                'name' => 'sidebar sinistra',
                'position' => 'sidebar_left',
                'description' => 'Contenuti per la sidebar di sinistra',
            ],
            [
                //2 
                'name' => 'sidebar destra',
                'position' => 'sidebar_right',
                'description' => 'Contenuti per la sidebar di destra',
            ],

        ]
        );
    }
}
