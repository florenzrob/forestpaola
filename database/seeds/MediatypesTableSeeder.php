<?php

use Illuminate\Database\Seeder;

class MediatypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('mediatypes')->insert([
            [
                //1
                'type' => 'carousel',
                'description' => 'Gallery a scorrimento',
            ],
            [
                //2
                'type' => 'gallery',
                'description' => 'Gallery classica',
            ],
            [
                //3
                'type' => 'banner',
                'description' => 'Singola immagine',
            ],
            [
                //4
                'type' => 'video',
                'description' => null,
            ],

        ]
        );
    }
}
