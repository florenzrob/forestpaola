<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailinglistMlgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailinglist_mlgroup', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mailinglist_id')->unsigned()->index();
            $table->foreign('mailinglist_id')->references('id')->on('mailinglists')->onDelete('cascade');

            $table->integer('mlgroup_id')->unsigned()->index();
            $table->foreign('mlgroup_id')->references('id')->on('mlgroups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailinglist_mlgroup', function (Blueprint $table) {
            //
        });
    }
}
