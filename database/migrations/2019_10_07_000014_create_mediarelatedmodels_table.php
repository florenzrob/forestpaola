<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediarelatedmodelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mediarelatedmodels')) {
            Schema::create('mediarelatedmodels', function (Blueprint $table) {
                $table->increments('id');
                $table->string('modelname'); //Nome modello della tabella db collegata alla gallery: Page, Article, Category
                $table->string('description')->nullable();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mediarelatedmodels');
    }
}
