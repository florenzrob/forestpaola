<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mediaitems')) {
            Schema::create('mediaitems', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('mediacollection_id')->unsigned()->nullable();
                $table->string('path');
                $table->string('link')->nullable();
                $table->enum('link_type', ['', 'url','mail']);
                $table->enum('target', ['','_blank']);
                $table->string('title')->nullable();
                $table->string('subtitle')->nullable();
                $table->text('content')->nullable();
                $table->integer('sort')->unsigned()->nullable();
                $table->boolean('visible')->unsigned()->nullable()->default('1');

                $table->foreign('mediacollection_id')->references('id')->on('mediacollections');
    
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mediaitems');
    }
}
