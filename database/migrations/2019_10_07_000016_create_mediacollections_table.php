<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediacollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('mediacollections')) {
            Schema::create('mediacollections', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('mediatype_id')->unsigned()->nullable();
                $table->integer('mediaposition_id')->unsigned()->nullable();
                $table->integer('mediarelatedmodel_id')->unsigned()->nullable();
                $table->integer('record_id')->unsigned()->nullable();
                $table->string('name'); //Nome della raccolta: gallery home, gallery header ...
                $table->text('description')->nullable();

                $table->foreign('mediatype_id')->references('id')->on('mediatypes');
                $table->foreign('mediaposition_id')->references('id')->on('mediapositions');
                $table->foreign('mediarelatedmodel_id')->references('id')->on('mediarelatedmodels');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mediacollections');
    }
}
