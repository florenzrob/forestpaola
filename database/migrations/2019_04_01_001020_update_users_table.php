<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //$table->increments('id');
            $table->string('title')->nullable(); //Es: Dr.
            //$table->string('name');
            $table->string('surname')->nullable();
            $table->enum('gender', ['M', 'F', 'NN']);
            //$table->string('username');
            //$table->string('password');
            $table->string('address')->nullable();
            $table->string('province', 3)->nullable();
            $table->string('city')->nullable();
            $table->integer('cap')->nullable();
            $table->string('nation')->nullable();
            $table->string('nationality')->nullable();
            $table->string('phone', 32)->nullable();
            $table->string('phone2', 32)->nullable();
            $table->string('mobile', 32)->nullable();
            $table->string('mobile2', 32)->nullable();
            //$table->string('email')->unique();
            $table->string('email2')->nullable();
            $table->string('pec')->nullable();
            $table->string('cf', 16)->nullable();
            $table->string('piva', 11)->nullable();
            $table->string('picture')->nullable();
            $table->text('note')->nullable();
            //$table->timestamp('email_verified_at')->nullable();
            $table->boolean('is_admin')->unsigned()->nullable()->default('0');
            $table->boolean('active')->unsigned()->nullable()->default('1');
            //$table->rememberToken();
            //$table->timestamps();
        });

        //Reorder columns
        //Settare in config/database.php 'strict' => false durante la migration dei campi timestamp, altrimenti si ottiene errore su default value
        DB::statement('ALTER TABLE users MODIFY COLUMN title VARCHAR(255) AFTER id');
        DB::statement('ALTER TABLE users MODIFY COLUMN surname VARCHAR(255) AFTER name');
        DB::statement('ALTER TABLE users MODIFY COLUMN gender ENUM("M", "F", "NN") AFTER surname');
        DB::statement('ALTER TABLE users MODIFY COLUMN email2 VARCHAR(255) AFTER email');
        DB::statement('ALTER TABLE users MODIFY COLUMN email_verified_at TIMESTAMP AFTER active');
        DB::statement('ALTER TABLE users MODIFY COLUMN remember_token VARCHAR(100) AFTER email_verified_at');
        DB::statement('ALTER TABLE users MODIFY COLUMN created_at TIMESTAMP AFTER remember_token');
        DB::statement('ALTER TABLE users MODIFY COLUMN updated_at TIMESTAMP AFTER created_at');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('title');
            $table->dropColumn('surname');
            $table->dropColumn('address');
            $table->dropColumn('province');
            $table->dropColumn('city');
            $table->dropColumn('cap');
            $table->dropColumn('phone');
            $table->dropColumn('phone2');
            $table->dropColumn('mobile');
            $table->dropColumn('mobile2');
            $table->dropColumn('email2');
            $table->dropColumn('pec');
            $table->dropColumn('cf');
            $table->dropColumn('piva');
            $table->dropColumn('picture');
            $table->dropColumn('note');
            $table->dropColumn('is_admin');
            $table->dropColumn('active');
        });
    }
}
