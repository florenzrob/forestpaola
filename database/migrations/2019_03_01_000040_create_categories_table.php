<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('slug');
            $table->string('subtitle')->nullable();
            $table->text('abstract')->nullable();
            $table->text('content')->nullable();
            $table->string('image_cover')->nullable();
            $table->integer('cover_height')->nullable()->unsigned()->default('300');
            $table->string('video_link')->nullable();
            $table->boolean('is_private')->unsigned()->nullable()->default('0'); //Eventuali categorie visibili solo agli utenti loggati
            $table->boolean('show_sidebar_dx')->unsigned()->nullable()->default('1'); //Mostra sidebar dx
            $table->integer('sort')->unsigned()->nullable();
            $table->boolean('visible')->unsigned()->nullable()->default('1');
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
