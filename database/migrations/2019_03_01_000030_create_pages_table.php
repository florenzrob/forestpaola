<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->unsigned();
            $table->integer('template_id')->nullable()->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->string('subtitle')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('seo_content')->nullable();
            $table->text('abstract')->nullable();
            $table->text('content')->nullable();
            $table->string('icon')->nullable();
            $table->string('image_cover')->nullable();
            $table->integer('cover_height')->nullable()->unsigned()->default('300');
            $table->string('video_link')->nullable();
            $table->boolean('is_home')->unsigned()->nullable()->default('0');
            $table->boolean('is_private')->unsigned()->nullable()->default('0'); //Eventuali pagine visibili solo agli utenti loggati
            $table->boolean('on_home')->unsigned()->nullable()->default('0'); //Eventuali pagine visibili nella home
            $table->boolean('show_sidebar_dx')->unsigned()->nullable()->default('1'); //Mostra sidebar dx
            $table->integer('sort')->unsigned()->nullable();
            $table->boolean('visible')->unsigned()->nullable()->default('1');
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('pages');
            $table->foreign('template_id')->references('id')->on('templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
