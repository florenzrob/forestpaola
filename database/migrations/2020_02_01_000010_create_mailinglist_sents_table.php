<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailinglistSentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailinglist_sents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mailinglist_id')->unsigned()->index();
            $table->text('recipients')->nullable();
            $table->dateTime('data_sent')->nullable(); //Quando deve essere spedita
            $table->boolean('sent')->unsigned()->nullable()->default('0');
            $table->timestamps();

            $table->foreign('mailinglist_id')->references('id')->on('mailinglists')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailinglist_sents', function (Blueprint $table) {
            //
        });
    }
}
