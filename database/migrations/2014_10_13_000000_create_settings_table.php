<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * SETTINGS
 * Tabella per la gestione dei settings globali dell'applicazione
 */
class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_business_name')->nullable(); //Ragione sociale
            $table->string('company_title')->nullable();
            $table->string('company_subtitle')->nullable();
            $table->string('company_slogan')->nullable();
            $table->string('company_logo')->nullable();
            $table->integer('company_logo_height')->nullable()->unsigned()->default('100');
            $table->text('company_abstract')->nullable();
            $table->text('company_description')->nullable();
            $table->string('company_seo_title')->nullable();
            $table->text('company_seo_content')->nullable();
            $table->string('company_phone1')->nullable();
            $table->string('company_phone2')->nullable();
            $table->string('company_mobile1')->nullable();
            $table->string('company_mobile2')->nullable();
            $table->string('company_fax')->nullable();
            $table->string('company_email1')->nullable();
            $table->string('company_email2')->nullable();
            $table->string('company_pec')->nullable();
            $table->string('company_url')->nullable();
            $table->string('company_vat', 11)->nullable(); //Partita iva
            $table->string('company_cf', 16)->nullable(); //Codice fiscale
            $table->string('company_address')->nullable();
            $table->string('company_address_number')->nullable(); //Numero civico
            $table->string('company_postal_code', 5)->nullable();
            $table->string('company_city')->nullable();
            $table->string('company_province', 3)->nullable();
            $table->string('company_nation')->nullable();

            $table->string('social_facebook')->nullable(); //Facebook
            $table->string('social_instagram')->nullable(); //Google page
            $table->string('social_twitter')->nullable(); //Twitter
            $table->string('social_linkedin')->nullable(); //Linkedin
            $table->string('social_googlepage')->nullable(); //Google page

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
