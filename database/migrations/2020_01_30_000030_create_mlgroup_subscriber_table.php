<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMlgroupSubscriberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mlgroup_subscriber', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mlgroup_id')->unsigned()->index();
            $table->foreign('mlgroup_id')->references('id')->on('mlgroups')->onDelete('cascade');

            $table->integer('subscriber_id')->unsigned()->index();
            $table->foreign('subscriber_id')->references('id')->on('subscribers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mlgroup_subscriber', function (Blueprint $table) {
            //
        });
    }
}
