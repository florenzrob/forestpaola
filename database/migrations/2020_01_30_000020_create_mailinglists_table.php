<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailinglistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailinglists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->text('content')->nullable();
            $table->text('attachment')->nullable();
            $table->dateTime('sent_date')->nullable(); //Da quando l'articolo è visibile
            $table->boolean('sent')->unsigned()->nullable()->default('0'); //Eventuali articoli visibili solo agli utenti loggati
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailinglists');
    }
}
